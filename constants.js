module.exports = {
  LOGIN_TYPE: {
    NORMAL: 'normal',
    POLY_AUTH: 'polyAuth',
  },
  ERRORS: {
    ASSERT_CALLING_USER: {
      USER_ID_NOT_MATCH: 4100,
      THIRD_PARTY_UNBIND: 4101,
    },
  },
};
