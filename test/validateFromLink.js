'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');
const errors = require('restify-errors');

const AccessToken = require('august-access-token');

const acs = require('../acs.js');
const config = require('../config/index.js');
const okCode = '123456';

const validatePath = '/validateFromLink';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${validatePath}`, () => {
  const identifierType = 'email';
  const identifier = `${identifierType}:${faker.internet.email()}`;
  const code = okCode;
  const brand = 'august';
  const path = validatePath;
  const fakeAccessToken = new AccessToken();

  let verificationStub;
  let response;

  describe('valid request', () => {
    let data;

    before(async () => {
      data = {
        code,
        token: fakeAccessToken.toJWT(),
        brand,
        identifier,
      };
      verificationStub = stubValidateVerificationCode();

      try {
        response = await axios.post(path, data);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    after(() => {
      verificationStub.restore();
    });
  });

  describe('valid request without token', () => {
    let data;

    before(async () => {
      data = {
        code,
        brand,
        identifier,
      };
      verificationStub = stubValidateVerificationCode();

      try {
        response = await axios.post(path, data);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 409', () => {
      assert.strictEqual(response.status, 400);
    });

    after(() => {
      verificationStub.restore();
    });
  });

  describe('valid request with without code', () => {
    let data;

    before(async () => {
      data = {
        token: fakeAccessToken.toJWT(),
        brand,
        identifier,
      };
      verificationStub = stubValidateVerificationCode();

      try {
        response = await axios.post(path, data);
      } catch (err) {
        response = err.response;
      }
    });

    after(() => {
      verificationStub.restore();
    });

    it('should return a 400', () => {
      assert.strictEqual(response.status, 400);
    });
  });

  describe('valid request with wrong code', () => {
    let data;

    before(async () => {
      data = {
        code: '678910',
        token: fakeAccessToken.toJWT(),
        brand,
        identifier,
      };
      verificationStub = stubValidateVerificationCode();

      try {
        response = await axios.post(path, data);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 409', () => {
      assert.strictEqual(response.status, 409);
    });

    after(() => {
      verificationStub.restore();
    });
  });

  describe('valid request with expired token', () => {
    let data;

    before(async () => {
      const expiredAccessToken = new AccessToken();
      const exp = new Date();
      exp.setSeconds(exp.getSeconds() - 1);
      expiredAccessToken._token.expiresAt = exp.toISOString();
      expiredAccessToken._token.exp = Math.floor(new Date(expiredAccessToken._token.expiresAt).valueOf() / 1000);
      data = {
        code,
        brand,
        identifier,
        token: expiredAccessToken.toJWT(),
      };
      verificationStub = stubValidateVerificationCode();

      try {
        response = await axios.post(path, data);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    after(() => {
      verificationStub.restore();
    });
  });
});

/**
 * @returns {Object} The stub
 */
function stubValidateVerificationCode() {
  return sinon.stub(acs, 'validateVerificationCode').callsFake((identifierType, identifierValue, code) => {
    if (code === okCode) {
      return {
        status: 200,
      };
    }

    throw new errors.InvalidArgumentError(`code ${code} invalid`);
  });
}
