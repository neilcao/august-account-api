'use strict';

const model = require('august-model');
const test = require('august-test');

before('start server', async () => await require('../server'));

before('init test framework', async () => {
  await test.init({
    model,
    noRabbit: true,
    noRedis: true,
  });
});
