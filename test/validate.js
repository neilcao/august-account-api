'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const ms = require('ms');
const sinon = require('sinon');

const AccessToken = require('august-access-token');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const acs = require('../acs.js');
const config = require('../config/index.js');
const session = require('../middleware/session.js');

const validatePath = '/validate';

const removeDocuments = () => model.usersTokens.collection.remove({});

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${validatePath}`, () => {
  const identifierType = 'email';
  const identifierValue = faker.internet.email();
  const code = '123456';
  const path = `${validatePath}/${identifierType}`;
  const data = {
    [identifierType]: identifierValue,
    code,
  };
  const dataMissingCode = {
    [identifierType]: identifierValue,
  };
  let verificationStub;
  let response;
  let user;
  let userIdentifiers;
  let userToken;
  let acsToken;

  describe('valid request', () => {
    const expectedExpirationMS = Date.now() + ms('59 minutes');

    before(async () => {
      await removeDocuments();

      const chain = await Test.Helpers.user();
      user = chain.user;
      userIdentifiers = chain.userIdentifiers;

      const acsTokenObj = getAcsResponseSuccess(user, userIdentifiers, expectedExpirationMS);
      acsToken = new AccessToken();
      acsToken._token = acsTokenObj;

      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      verificationStub = stubValidateVerificationCodeSuccess(acsToken);

      try {
        response = await axios.post(path, data, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    after(() => verificationStub.restore());

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should return an account management token', () => {
      const actualUserID = jsUtil.crypto.decrypt(response.data, session.test.getEncryptionPassword());
      const expectedUserID = user._id;

      assert.strictEqual(actualUserID, expectedUserID);
    });

    it('should update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const jwtExpiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(
        userToken.autoExpiresAt.getTime() - Date.now() - config.get('tokenExpirationTimeInMS'),
        0,
        100
      );
      assert.approximately(userToken.expiresAt - Date.now() - config.get('tokenExpirationTimeInMS'), 0, 100);
      assert.approximately(jwtExpiresAtMS - Date.now() - config.get('tokenExpirationTimeInMS'), 0, 100);
      assert.approximately(jwtExpiresAtMS / 1000, token._token.exp, 1);
    });
  });

  describe('expired access token from rest api', () => {
    const expectedExpirationMS = Date.now() - 1000;

    before(async () => {
      await removeDocuments();

      const chain = await Test.Helpers.user();
      user = chain.user;
      userIdentifiers = chain.userIdentifiers;

      const acsTokenObj = getAcsResponseSuccess(user, userIdentifiers, expectedExpirationMS);
      acsToken = new AccessToken();
      acsToken._token = acsTokenObj;

      const refreshToken = null;
      await model.usersTokens.upsert(
        user._id,
        `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`,
        expectedExpirationMS,
        refreshToken,
        new Date(expectedExpirationMS)
      );

      verificationStub = stubValidateVerificationCodeSuccess(acsToken);

      try {
        response = await axios.post(path, data, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    after(() => verificationStub.restore());

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should not update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const expiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(userToken.autoExpiresAt.getTime(), expectedExpirationMS, 100);
      assert.approximately(userToken.expiresAt, expectedExpirationMS, 100);
      assert.approximately(expiresAtMS, expectedExpirationMS, 100);
    });
  });

  describe('not enough factors in token from rest api', () => {
    const expectedExpirationMS = faker.date.future().getTime();

    before(async () => {
      await removeDocuments();

      const chain = await Test.Helpers.user();
      user = chain.user;
      userIdentifiers = chain.userIdentifiers;

      const acsTokenObj = getAcsResponseSuccess(user, userIdentifiers, expectedExpirationMS);
      acsTokenObj.vEmail = false;
      acsTokenObj.vPassword = false;
      acsToken = new AccessToken();
      acsToken._token = acsTokenObj;

      const refreshToken = null;
      await model.usersTokens.upsert(
        user._id,
        `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`,
        expectedExpirationMS,
        refreshToken,
        new Date(expectedExpirationMS)
      );

      verificationStub = stubValidateVerificationCodeSuccess(acsToken);

      try {
        response = await axios.post(path, data, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    after(() => verificationStub.restore());

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should not update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const expiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(userToken.autoExpiresAt.getTime(), expectedExpirationMS, 100);
      assert.approximately(userToken.expiresAt, expectedExpirationMS, 100);
      assert.approximately(expiresAtMS, expectedExpirationMS, 100);
    });
  });

  describe('missing verification code', () => {
    const expectedExpirationMS = faker.date.future().getTime();

    before(async () => {
      await removeDocuments();

      const chain = await Test.Helpers.user();
      user = chain.user;

      acsToken = new AccessToken();

      const refreshToken = null;
      await model.usersTokens.upsert(
        user._id,
        `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`,
        expectedExpirationMS,
        refreshToken,
        new Date(expectedExpirationMS)
      );

      try {
        response = await axios.post(path, dataMissingCode, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    it('should return a 409', () => {
      assert.strictEqual(response.status, 409);
    });

    it('should not update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const expiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(userToken.autoExpiresAt.getTime(), expectedExpirationMS, 100);
      assert.approximately(userToken.expiresAt, expectedExpirationMS, 100);
      assert.approximately(expiresAtMS, new Date(acsToken.expiresAt).getTime(), 100);
    });
  });
});

/**
 *
 * @param {Object} user -
 * @returns {Object} The options to pass to the HTTP post method.
 */
function getPostOptions(user) {
  return {
    headers: {
      [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
    },
  };
}

/**
 * @param {Object} user -
 * @param {Object} userIdentifiers -
 * @param {number} expectedExpirationMS - epoch time
 * @returns {Object} The response
 */
function getAcsResponseSuccess(user, userIdentifiers, expectedExpirationMS) {
  return {
    installId: '',
    applicationId: '',
    userId: user._id,
    vInstallId: false,
    vPassword: true,
    vEmail: true,
    vPhone: false,
    hasInstallId: false,
    hasPassword: true,
    hasEmail: true,
    hasPhone: true,
    isLockedOut: false,
    captcha: '',
    email: userIdentifiers.email ? [userIdentifiers.email._value] : [],
    phone: userIdentifiers.phone ? [userIdentifiers.phone._value] : [],
    expiresAt: new Date(expectedExpirationMS).toISOString(),
    temporaryAccountCreationPasswordLink: '',
    iat: 1541021447,
    exp: null,
    LastName: user.LastName,
    FirstName: user.FirstName,
  };
}

/**
 *
 * @param {Object} token -
 * @returns {Object} The stub
 */
function stubValidateVerificationCodeSuccess(token) {
  return sinon.stub(acs, 'validateVerificationCode').callsFake(() => {
    return {
      status: 200,
      headers: {
        [config.get('acsAccessTokenHeader')]: token.toJWT(),
      },
    };
  });
}
