'use strict';

const _ = require('lodash');

const AccessToken = require('august-access-token');

module.exports.mockAxiosResponse = options => {
  return {
    status: _.get(options, 'status', 200),
    data: _.get(options, 'data', {}),
  };
};

module.exports.mockAxiosError = options => {
  return {
    response: {
      status: _.get(options, 'response.status', 500),
      data: _.get(options, 'response.data', {}),
    },
  };
};

module.exports.mockAcsActivityResponse = options => {
  return {
    status: _.get(options, 'status', 200),
    data: {
      events: _.get(options, 'data', []),
    },
  };
};

module.exports.mockAcsHousesResponse = options => {
  return {
    status: _.get(options, 'status', 200),
    data: _.get(options, 'data', []),
  };
};

module.exports.getAccessTokenForUser = user => {
  const token = new AccessToken();
  token._token.userId = user._id;
  token._token.vPassword = true;
  token._token.hasPassword = true;
  token._token.vEmail = true;
  token._token.hasEmail = true;
  token._token.vPhone = true;
  token._token.hasPhone = true;

  return token;
};
