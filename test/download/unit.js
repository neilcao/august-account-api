'use strict';

const assert = require('chai').assert;
const faker = require('faker');
const fs = require('fs');
const path = require('path');
const Promise = require('bluebird').Promise;
const sinon = require('sinon');

const AccessToken = require('august-access-token');
const babelfish = require('august-babelfish');
const constants = require('august-constants');
const entities = require('august-html-entities');
const model = require('august-model');
const Test = require('august-test');

const acs = require('../../acs.js');
const config = require('../../config/index.js');
const download = require('../../middleware/download');
const helper = require('../helper.js');
const s3util = require('../../s3.js');
const util = require('../../util.js');

const LOCAL_DIRECTORY_BASE_PATH = config.get('download:localDirectoryBasePath');
const ACTIVITY_FEED_FILENAME = config.get('download:activityFeed:saveFileName');
const brand = constants.Brands.YALE;

describe('download unit tests', () => {
  describe('deleteLocalDirectory', () => {
    let rmdirStub;

    beforeEach(() => (rmdirStub = sinon.stub(download.internals, 'rimraf')));
    afterEach(() => rmdirStub.restore());

    it('should delete if the directory is a child of the base directory', async () => {
      const directory = `${LOCAL_DIRECTORY_BASE_PATH}/${faker.random.uuid()}`;
      await download.internals.deleteLocalDirectory(directory);

      sinon.assert.calledOnce(rmdirStub);
      sinon.assert.calledWith(rmdirStub, directory);
    });

    it('should not delete if the directory is not a child of the base directory', async () => {
      try {
        await download.internals.deleteLocalDirectory(`${LOCAL_DIRECTORY_BASE_PATH}/../${faker.random.uuid()}`);
      } catch (err) {
        // console.log(err);
      }

      sinon.assert.notCalled(rmdirStub);
    });

    it('should not delete if the directory is blank', async () => {
      try {
        await download.internals.deleteLocalDirectory('');
      } catch (err) {
        // console.log(err);
      }

      sinon.assert.notCalled(rmdirStub);
    });
  });

  describe('createLocalDirectory', () => {
    let mkdirStub;

    beforeEach(() => (mkdirStub = sinon.spy(download.internals, 'mkdir')));
    afterEach(() => mkdirStub.restore());

    it('should create if the directory is a child of the base directory', async () => {
      const directory = `${LOCAL_DIRECTORY_BASE_PATH}/${faker.random.uuid()}`;
      await download.internals.createLocalDirectory(directory);
      fs.rmdirSync(directory);

      sinon.assert.calledOnce(mkdirStub);
      sinon.assert.calledWith(mkdirStub, directory);
    });

    it('should not create if the directory is not a child of the base directory', async () => {
      const directory = `${LOCAL_DIRECTORY_BASE_PATH}/../${faker.random.uuid()}`;
      let errorThrown = false;

      try {
        await download.internals.createLocalDirectory(directory);
      } catch (err) {
        errorThrown = true;
      }

      sinon.assert.notCalled(mkdirStub);
      assert.isTrue(errorThrown);
    });

    it('should not create if the directory is blank', async () => {
      const directory = '';
      let errorThrown = false;

      try {
        await download.internals.createLocalDirectory(directory);
      } catch (err) {
        errorThrown = true;
      }

      sinon.assert.notCalled(mkdirStub);
      assert.isTrue(errorThrown);
    });
  });

  describe('getActivityFeed', () => {
    const accessToken = 'mock access token';
    let acsGetHousesStub;
    let acsGetActivityStub;
    let houseMineEndpoint;
    let houseOne;
    let houseTwo;

    const createStubs = async (options = {}) => {
      const chainOne = await Test.Helpers.house();
      houseOne = chainOne.house;

      const chainTwo = await Test.Helpers.house();
      houseTwo = chainTwo.house;

      houseMineEndpoint = {
        status: 200,
        data: [
          {
            HouseID: houseOne.HouseID,
            HouseName: houseOne.HouseName,
            type: 'superuser',
            imageInfo: {
              height: 1080,
              width: 304,
              format: 'jpeg',
              created_at: '2020-02-11T21:19:42.205Z',
              bytes: 77472,
              secure_url: 'https://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              url: 'http://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              etag: 'ef855708224d05fe578db4aa50be764e',
            },
          },
          {
            HouseID: houseTwo.HouseID,
            HouseName: houseTwo.HouseName,
            type: 'user',
            imageInfo: {
              height: 1080,
              width: 304,
              format: 'jpeg',
              created_at: '2020-02-11T21:19:42.205Z',
              bytes: 77472,
              secure_url: 'https://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              url: 'http://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              etag: 'ef855708224d05fe578db4aa50be764e',
            },
          },
        ],
      };

      acsGetActivityStub = sinon
        .stub(acs, 'getActivityFeed')
        .callsFake(() => Promise.resolve(helper.mockAcsActivityResponse(options.activity)));
      acsGetHousesStub = sinon
        .stub(acs, 'getHouses') //        .callsFake(() => Promise.resolve(helper.mockAcsActivityResponse(options.activity)));
        .returns(houseMineEndpoint);
    };

    afterEach('restore stubs', () => {
      acsGetHousesStub.restore();
      acsGetActivityStub.restore();
    });

    describe('success', () => {
      it('should call acs get houses', async () => {
        await createStubs();

        try {
          await download.internals.getActivityFeed(accessToken, brand);
        } catch (err) {
          // console.log(err);
        }

        sinon.assert.calledOnce(acsGetHousesStub);
        sinon.assert.calledWith(acsGetHousesStub, accessToken);
      });

      it('should recursively get the activity feed for each house', async () => {
        const activity1 = {
          action: 'add',
          dateTime: Date.now(),
          user: {
            UserID: faker.random.uuid(),
            FirstName: faker.name.firstName(),
            LastName: faker.name.lastName(),
          },
        };

        const stubOptions = {
          house: {
            data: [
              {
                HouseID: faker.random.uuid(),
              },
              {
                HouseID: faker.random.uuid(),
              },
            ],
          },
          activity: [
            {
              data: [activity1],
            },
            {
              data: [], // no data means we've gotten the whole activity feed for a house.
            },
          ],
        };
        let result;

        await createStubs(stubOptions);

        acsGetActivityStub.restore();
        acsGetActivityStub = sinon.stub(acs, 'getActivityFeed');
        stubOptions.activity.forEach((item, index) => {
          acsGetActivityStub
            .onCall(index)
            .callsFake(() => Promise.resolve(helper.mockAcsActivityResponse(stubOptions.activity[index])));
        });

        try {
          result = await download.internals.getActivityFeed(accessToken, brand);
        } catch (err) {
          // console.log(err);
        }

        const exptedActivityFeed = [activity1];

        sinon.assert.callCount(acsGetActivityStub, 1);
        sinon.assert.calledWith(acsGetActivityStub, accessToken, houseOne.HouseID);
        sinon.assert.calledWith(acsGetActivityStub, accessToken, houseOne.HouseID, brand);
        assert.deepStrictEqual(result, exptedActivityFeed);
      });
    });

    describe('failure', () => {
      it('should handle the "acs get houses" endpoint failing', async () => {
        await createStubs();

        acsGetHousesStub.restore();
        acsGetHousesStub = sinon.stub(acs, 'getHouses').rejects(helper.mockAxiosError());

        try {
          await download.internals.getActivityFeed(accessToken, brand);
        } catch (err) {
          assert.fail('error not handled');
        }

        sinon.assert.calledOnce(acsGetHousesStub);
        sinon.assert.calledWith(acsGetHousesStub, accessToken);
      });

      it('should handle the "acs get activity" endpoint failing', async () => {
        await createStubs({
          house: {
            data: [
              {
                HouseID: faker.random.uuid(),
              },
            ],
          },
        });

        acsGetActivityStub.restore();
        acsGetActivityStub = sinon.stub(acs, 'getActivityFeed').rejects(helper.mockAxiosError());

        try {
          await download.internals.getActivityFeed(accessToken, brand);
        } catch (err) {
          assert.fail('error not handled');
        }

        sinon.assert.calledOnce(acsGetActivityStub);
        sinon.assert.calledWith(acsGetActivityStub, accessToken);
      });
    });
  });

  describe('getUserImages', () => {
    const accessToken = 'mock access token';
    let acsGetHousesStub;
    let acsGetUserStub;
    let houseMineEndpoint;
    let userMeEndpoint;
    let user;
    let houseOne;
    let houseTwo;
    let expectedUrls;

    const createStubs = async () => {
      user = await Test.Helpers.user();

      const chainOne = await Test.Helpers.house();
      houseOne = chainOne.house;

      const chainTwo = await Test.Helpers.house();
      houseTwo = chainTwo.house;

      userMeEndpoint = {
        status: 200,
        data: {
          imageInfo: {
            original: {
              width: 1008,
              height: 756,
              format: 'jpg',
              url: user.image.result.url,
              secure_url: user.image.result.secure_url,
            },
            thumbnail: {
              width: 128,
              height: 128,
              format: 'jpg',
              url: user.image.result.eager['0'].url,
              secure_url: user.image.result.eager['0'].secure_url + '.thumbnail',
            },
          },
        },
      };

      houseMineEndpoint = {
        status: 200,
        data: [
          {
            HouseID: houseOne.HouseID,
            HouseName: houseOne.HouseName,
            type: 'superuser',
            imageInfo: {
              height: 1080,
              width: 304,
              format: 'jpeg',
              created_at: '2020-02-11T21:19:42.205Z',
              bytes: 77472,
              secure_url: 'https://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              url: 'http://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              etag: 'ef855708224d05fe578db4aa50be764e',
            },
          },
          {
            HouseID: houseTwo.HouseID,
            HouseName: houseTwo.HouseName,
            type: 'user',
            imageInfo: {
              height: 1080,
              width: 304,
              format: 'jpeg',
              created_at: '2020-02-11T21:19:42.205Z',
              bytes: 77472,
              secure_url: 'https://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              url: 'http://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
              etag: 'ef855708224d05fe578db4aa50be764e',
            },
          },
        ],
      };

      expectedUrls = [
        houseMineEndpoint.data[0].imageInfo.secure_url,
        userMeEndpoint.data.imageInfo.original.secure_url,
        userMeEndpoint.data.imageInfo.thumbnail.secure_url,
      ];

      acsGetUserStub = sinon.stub(acs, 'getUser').returns(userMeEndpoint);
      acsGetHousesStub = sinon.stub(acs, 'getHouses').returns(houseMineEndpoint);
    };

    afterEach('restore stubs', () => {
      acsGetHousesStub.restore();
      acsGetUserStub.restore();
    });

    describe('success', () => {
      it('should call the correct endpoints and retrieve the expected urls', async () => {
        let urls;
        await createStubs();

        try {
          urls = await download.internals.getUserImages(accessToken, brand);
        } catch (err) {
          // console.log(err);
        }

        sinon.assert.calledOnce(acsGetHousesStub);
        sinon.assert.calledWith(acsGetHousesStub, accessToken);
        sinon.assert.calledOnce(acsGetUserStub);
        sinon.assert.calledWith(acsGetUserStub, accessToken);

        assert.deepEqual(urls.sort(), expectedUrls.sort());
      });
    });

    describe('failure', () => {
      it('should handle the "acs get houses" endpoint failing', async () => {
        await createStubs();

        acsGetHousesStub.restore();
        acsGetHousesStub = sinon.stub(acs, 'getHouses').rejects();

        try {
          await download.internals.getUserImages(accessToken, brand);
        } catch (err) {
          assert.fail('error not handled');
        }

        sinon.assert.calledOnce(acsGetHousesStub);
        sinon.assert.calledWith(acsGetHousesStub, accessToken);
      });

      it('should handle the "acs get user" endpoint failing', async () => {
        await createStubs({
          house: {
            data: [
              {
                HouseID: faker.random.uuid(),
              },
            ],
          },
        });

        acsGetUserStub.restore();
        acsGetUserStub = sinon.stub(acs, 'getUser').rejects();

        try {
          await download.internals.getUserImages(accessToken, brand);
        } catch (err) {
          assert.fail('error not handled');
        }

        sinon.assert.calledOnce(acsGetUserStub);
        sinon.assert.calledWith(acsGetUserStub, accessToken);
      });
    });
  });

  describe.skip('saveActivityFeed', () => {
    const directory = `${LOCAL_DIRECTORY_BASE_PATH}/${faker.random.uuid()}`;
    const accessToken = 'mock access token';
    let writeFileStub;

    const createStubs = () => (writeFileStub = sinon.stub(download.internals, 'writeFile'));

    afterEach('restore stubs', () => writeFileStub.restore());

    describe('success', () => {
      it('should write the activity feed to disk', async () => {
        createStubs();

        try {
          await download.internals.getActivityFeed(accessToken, brand);
        } catch (err) {
          // console.log(err);
        }

        sinon.assert.calledOnce(writeFileStub);
        sinon.assert.calledWith(writeFileStub, `${directory}/${ACTIVITY_FEED_FILENAME}`);
      });
    });

    describe('failure', () => {
      it('should handle the writeFile function failing', async () => {
        createStubs({
          house: {
            data: [
              {
                HouseID: faker.random.uuid(),
              },
            ],
          },
        });

        writeFileStub.restore();
        writeFileStub = sinon.stub(download.internals, 'writeFile').rejects(new Error());

        try {
          await download.internals.getActivityFeed(accessToken, brand);
        } catch (err) {
          assert.fail('error not handled');
        }

        sinon.assert.calledOnce(writeFileStub);
        sinon.assert.calledWith(writeFileStub, `${directory}/${ACTIVITY_FEED_FILENAME}`);
      });
    });
  });

  describe('saveMediaFromActivityFeed', () => {
    const activities = [
      {
        action: constants.Events.DOORBELL_MOTION_DETECTED,
        deviceName: faker.random.word(),
        deviceType: constants.Devices.DOORBELL,
        deviceID: faker.random.uuid(),
        dateTime: faker.date.past().getTime(),
        callingUser: {
          UserID: faker.random.uuid(),
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
        },
        house: {
          houseName: faker.random.word(),
          houseID: faker.random.uuid(),
        },
        info: {
          dvrID: faker.random.uuid(),
          image: {
            secure_url: faker.internet.url(),
          },
        },
      },
      {
        action: constants.LockActions.LOCK,
        deviceName: faker.random.word(),
        deviceType: constants.Devices.LOCK,
        deviceID: faker.random.uuid(),
        dateTime: faker.date.past().getTime(),
      },
    ];
    const listObjectsResponse = {
      Contents: [
        {
          Key: `${activities[0].house.houseID}/${activities[0].deviceID}/${activities[0].info.dvrID}/foo.txt`,
        },
        {
          Key: `${activities[0].house.houseID}/${activities[0].deviceID}/${activities[0].info.dvrID}/bar.txt`,
        },
      ],
    };
    const directory = `${LOCAL_DIRECTORY_BASE_PATH}/${faker.random.uuid()}`;
    const userID = faker.random.uuid();
    const includeVideos = true;
    const includeImages = true;

    let mkdirpStub;
    let listObjectsStub;
    let downloadFromS3Stub;
    let downloadImageStub;

    before(async () => {
      mkdirpStub = sinon.stub(download.internals, 'mkdirp');
      listObjectsStub = sinon.stub(s3util, 'listObjects').returns(listObjectsResponse);
      downloadFromS3Stub = sinon.stub(download.internals, 'downloadFromS3');
      downloadImageStub = sinon.stub(util, 'downloadFile');

      try {
        await download.internals.saveMediaFromActivityFeed(activities, directory, userID, includeVideos, includeImages);
      } catch (err) {
        // console.log('err: ', err);
      }
    });

    after(() => {
      mkdirpStub.restore();
      listObjectsStub.restore();
      downloadFromS3Stub.restore();
      downloadImageStub.restore();
    });

    it('should create the local directory for the media files', () => {
      const expectedPath = path.join(
        directory,
        entities.unescape(activities[0].house.houseName),
        entities.unescape(activities[0].deviceName),
        new Date(activities[0].dateTime).toISOString()
      );

      sinon.assert.calledOnce(mkdirpStub);
      sinon.assert.calledWithExactly(mkdirpStub, expectedPath);
    });

    it('should get a list of videos to download from S3', () => {
      const expectedParams = {
        Bucket: config.get('s3:videoBucket'),
        Prefix: `${activities[0].house.houseID}/${activities[0].deviceID}/${activities[0].info.dvrID}`,
      };
      sinon.assert.calledOnce(listObjectsStub);
      sinon.assert.calledWithExactly(listObjectsStub, expectedParams);
    });

    it('should download video files from S3', () => {
      const expectedPath = path.join(
        directory,
        entities.unescape(activities[0].house.houseName),
        entities.unescape(activities[0].deviceName),
        new Date(activities[0].dateTime).toISOString()
      );

      sinon.assert.calledTwice(downloadFromS3Stub);
      sinon.assert.calledWith(
        downloadFromS3Stub,
        config.get('s3:videoBucket'),
        listObjectsResponse.Contents[0].Key,
        expectedPath
      );
      sinon.assert.calledWith(
        downloadFromS3Stub,
        config.get('s3:videoBucket'),
        listObjectsResponse.Contents[1].Key,
        expectedPath
      );
    });

    it('should download the preview image', () => {
      const expectedPath = path.join(
        directory,
        activities[0].house.houseName,
        activities[0].deviceName,
        new Date(activities[0].dateTime).toISOString(),
        `${config.get('download:videoPreviewImageName')}${path.extname(activities[0].info.image.secure_url)}`
      );

      sinon.assert.calledOnce(downloadImageStub);
      sinon.assert.calledWith(downloadImageStub, activities[0].info.image.secure_url, expectedPath);
    });
  });

  describe('uploadToS3', () => {
    let uploadStub;

    afterEach(() => uploadStub.restore());

    it('should error if the source file does not exist', async () => {
      const userID = faker.random.uuid();
      const filePath = '/foo/bar.txt';
      let errorThrown = false;

      uploadStub = sinon.stub(s3util, 'upload');

      try {
        await download.internals.uploadToS3(filePath, userID);
      } catch (err) {
        errorThrown = true;
      }

      const args = uploadStub.getCall(0).args;

      sinon.assert.calledOnce(uploadStub);
      assert.strictEqual(args[0], config.get('s3:userBucket'));
      assert.strictEqual(args[1], `${userID}/${path.basename(filePath)}`);
      assert.strictEqual(args[2], filePath);
      assert.isTrue(errorThrown);
    });

    it('should error if the upload fails', async () => {
      const userID = faker.random.uuid();
      const filePath = `${__dirname}/bar.txt`;
      let errorThrown = false;

      uploadStub = sinon.stub(s3util, 'upload').yields(new Error(), null);
      fs.writeFileSync(filePath, 'bar.txt contents');

      try {
        await download.internals.uploadToS3(filePath, userID);
      } catch (err) {
        errorThrown = true;
      }

      fs.unlinkSync(filePath);

      const args = uploadStub.getCall(0).args;

      sinon.assert.calledOnce(uploadStub);
      assert.strictEqual(args[0], config.get('s3:userBucket'));
      assert.strictEqual(args[1], `${userID}/${path.basename(filePath)}`);
      assert.strictEqual(args[2], filePath);
      assert.isTrue(errorThrown);
    });

    it('should upload if the source file does exist', async () => {
      const userID = faker.random.uuid();
      const filePath = `${__dirname}/bar.txt`;
      let errorThrown = false;

      uploadStub = sinon.stub(s3util, 'upload').callsFake(() =>
        Promise.resolve({
          Key: filePath,
        })
      );

      try {
        await download.internals.uploadToS3(filePath, userID);
      } catch (err) {
        errorThrown = true;
      }

      const args = uploadStub.getCall(0).args;

      sinon.assert.calledOnce(uploadStub);
      assert.strictEqual(args[0], config.get('s3:userBucket'));
      assert.strictEqual(args[1], `${userID}/${path.basename(filePath)}`);
      assert.strictEqual(args[2], filePath);
      assert.isFalse(errorThrown);
    });
  });

  describe('deleteFromS3', () => {
    let deleteStub;

    afterEach(() => deleteStub.restore());

    it('should delete', async () => {
      const userID = faker.random.uuid();
      const baseUrl = `https://s3.us-west-2.amazonaws.com/${config.get('s3:userBucket')}`;
      const filePath = `${userID}/${download.internals.getCompressFileName()}`;
      const url = `${baseUrl}/${filePath}`;
      let errorThrown = false;

      deleteStub = sinon.stub(s3util, 'deleteObject').callsFake(() => Promise.resolve());

      try {
        await download.internals.deleteFromS3(url, userID);
      } catch (err) {
        errorThrown = true;
      }

      const args = deleteStub.getCall(0).args;

      sinon.assert.calledOnce(deleteStub);
      assert.strictEqual(args[0], `${config.get('s3:userBucket')}`);
      assert.strictEqual(args[1], filePath);
      assert.isFalse(errorThrown);
    });
  });

  describe('compressUserFiles', () => {
    let directory;
    let dataFilePath;
    let filePath;
    let compressFilePath;

    afterEach(() => {
      fs.unlinkSync(dataFilePath);
      fs.unlinkSync(compressFilePath);
      fs.rmdirSync(directory);
    });

    it('compresses files', async () => {
      directory = `${LOCAL_DIRECTORY_BASE_PATH}/${faker.random.uuid()}`;
      dataFilePath = `${directory}/foo.txt`;

      fs.mkdirSync(directory);
      fs.writeFileSync(dataFilePath, 'foo.txt contents');

      try {
        compressFilePath = await download.internals.compressUserFiles(directory, filePath);
      } catch (err) {
        // console.log(err);
      }

      try {
        assert.isOk(fs.statSync(compressFilePath));
      } catch (err) {
        assert.fail('compress file does not exist');
      }
    });
  });

  describe('processDownloadJob', () => {
    let createDirectoryStub;
    let getUserImagesStub;
    let saveUserImagesStub;
    let getActivityFeedStub;
    let saveActivityFeedStub;
    let saveMediaFromActivityFeedStub;
    let compressUserFilesStub;
    let uploadToS3Stub;
    let getSignedUrlStub;
    let databaseUpdateStub;
    let getEmailsStub;
    let sendEmailsStub;
    let getUserStub;

    afterEach(() => {
      createDirectoryStub.restore();
      getUserImagesStub.restore();
      saveUserImagesStub.restore();
      getActivityFeedStub.restore();
      saveActivityFeedStub.restore();
      saveMediaFromActivityFeedStub.restore();
      compressUserFilesStub.restore();
      uploadToS3Stub.restore();
      getSignedUrlStub.restore();
      databaseUpdateStub.restore();
      getEmailsStub.restore();
      sendEmailsStub.restore();
      getUserStub.restore();
    });

    it('should call the right functions', async () => {
      const { brandName, fromEmail } = constants.Brands.getBrand(brand);
      const jobData = {
        userID: faker.random.uuid(),
        brand,
        localDirectory: path.join(LOCAL_DIRECTORY_BASE_PATH, faker.lorem.word()),
        includeActivity: true,
        includeVideos: true,
        includeImages: true,
        userDownload: {
          _id: '5c789c025380b30f6492343d',
        },
        accessToken: new AccessToken().toJWT(),
      };
      const activityFeed = [];
      const compressFilePath = `${jobData.localDirectory}/${download.internals.getCompressFileName()}`;
      const uploadToS3Result = {
        Key: faker.lorem.word(),
      };
      const signedUrl = faker.internet.url();
      const userEmails = [faker.internet.email(), faker.internet.email()];
      const user = {
        FirstName: faker.name.firstName(),
      };
      const emailData = {
        from: fromEmail,
        subject: babelfish.t('downloadProcessed', { user, brand }),
        event: 'user_download_processed',
        template: {
          name: 'user-download-processed-yale.html',
          data: {
            name: entities.unescape(user.FirstName),
            brand,
            brandName,
          },
        },
      };

      const userImages = [
        'https://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultHouse.jpeg',
        'https://d33mytkkohwnk6.cloudfront.net/app/UserDefaults/defaultUser.jpeg',
      ];

      createDirectoryStub = sinon.stub(download.internals, 'createLocalDirectory');
      getUserImagesStub = sinon.stub(download.internals, 'getUserImages').returns(userImages);
      saveUserImagesStub = sinon.stub(download.internals, 'saveUserImages');
      getActivityFeedStub = sinon.stub(download.internals, 'getActivityFeed').returns(activityFeed);
      saveActivityFeedStub = sinon.stub(download.internals, 'saveActivityFeed');
      saveMediaFromActivityFeedStub = sinon.stub(download.internals, 'saveMediaFromActivityFeed');
      compressUserFilesStub = sinon.stub(download.internals, 'compressUserFiles').returns(compressFilePath);
      uploadToS3Stub = sinon.stub(download.internals, 'uploadToS3').returns(uploadToS3Result);
      getSignedUrlStub = sinon.stub(s3util, 'getSignedUrl').returns(signedUrl);
      databaseUpdateStub = sinon.stub(model.usersDownloads, 'update');
      getEmailsStub = sinon.stub(util, 'getUserEmails').returns(userEmails);
      sendEmailsStub = sinon.stub(util, 'sendEmailToUser');
      getUserStub = sinon.stub(model.users, 'findByID').returns(user);

      try {
        await download.internals.processDownloadJob(jobData);
      } catch (err) {
        // console.log(err);
      }

      sinon.assert.calledOnce(createDirectoryStub);
      sinon.assert.calledWithExactly(createDirectoryStub, jobData.localDirectory);

      sinon.assert.calledOnce(getUserImagesStub);
      sinon.assert.calledWithExactly(getUserImagesStub, jobData.accessToken, brand);

      sinon.assert.calledOnce(saveUserImagesStub);
      sinon.assert.calledWithExactly(saveUserImagesStub, userImages, jobData.localDirectory);

      sinon.assert.calledOnce(getActivityFeedStub);
      sinon.assert.calledWithExactly(getActivityFeedStub, jobData.accessToken, brand);

      sinon.assert.calledOnce(saveActivityFeedStub);
      sinon.assert.calledWithExactly(saveActivityFeedStub, activityFeed, jobData.localDirectory);

      sinon.assert.calledOnce(saveMediaFromActivityFeedStub);
      sinon.assert.calledWithExactly(
        saveMediaFromActivityFeedStub,
        activityFeed,
        jobData.localDirectory,
        jobData.userID,
        jobData.includeVideos,
        jobData.includeImages
      );

      sinon.assert.calledOnce(compressUserFilesStub);
      sinon.assert.calledWithExactly(compressUserFilesStub, jobData.localDirectory);

      sinon.assert.calledOnce(uploadToS3Stub);
      sinon.assert.calledWithExactly(uploadToS3Stub, compressFilePath, jobData.userID);

      sinon.assert.calledOnce(getSignedUrlStub);
      sinon.assert.calledWithExactly(
        getSignedUrlStub,
        config.get('s3:userBucket'),
        uploadToS3Result.Key,
        config.get('download:signedUrlExpirationInSeconds')
      );

      sinon.assert.calledTwice(databaseUpdateStub);
      sinon.assert.calledWithExactly(databaseUpdateStub, jobData.userDownload._id, {
        status: constants.UserDownloadStatuses.PROCESSING,
      });
      sinon.assert.calledWithExactly(databaseUpdateStub, jobData.userDownload._id, {
        url: signedUrl,
        s3key: uploadToS3Result.Key,
        status: constants.UserDownloadStatuses.AVAILABLE,
      });

      sinon.assert.calledOnce(sendEmailsStub);
      sinon.assert.calledWith(sendEmailsStub, jobData.userID, userEmails, emailData);
    });

    it('should mark the job as failed in the database', async () => {
      const jobData = {
        userID: faker.random.uuid(),
        localDirectory: path.join(LOCAL_DIRECTORY_BASE_PATH, faker.lorem.word()),
        includeActivity: true,
        includeVideos: true,
        includeImages: true,
        userDownload: {
          _id: '5c789c025380b30f6492343d',
        },
        accessToken: new AccessToken().toJWT(),
      };

      createDirectoryStub = sinon.stub(download.internals, 'createLocalDirectory').throws(new Error('mock error'));
      databaseUpdateStub = sinon.stub(model.usersDownloads, 'update');

      try {
        await download.internals.processDownloadJob(jobData);
      } catch (err) {
        // console.log(err);
      }

      sinon.assert.calledOnce(createDirectoryStub);

      sinon.assert.calledTwice(databaseUpdateStub);
      sinon.assert.calledWithExactly(databaseUpdateStub, jobData.userDownload._id, {
        status: constants.UserDownloadStatuses.PROCESSING,
      });
      sinon.assert.calledWithExactly(databaseUpdateStub, jobData.userDownload._id, {
        status: constants.UserDownloadStatuses.FAILED,
      });
    });
  });
});
