'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const constants = require('august-constants');
const jsUtil = require('august-js-utility');
const messages = require('august-messages');
const model = require('august-model');
const test = require('august-test');

const config = require('../../config/index.js');
const helper = require('../helper.js');
const downloadMiddleware = require('../../middleware/download.js');
const s3Util = require('../../s3');
const session = require('../../middleware/session.js');

const path = '/downloads';

const removeDocuments = () => model.usersDownloads.collection.remove({});

const ACCESS_TOKEN_HEADER = config.get('accessTokenHeader');

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${path}`, () => {
  let response;
  let user;

  before(() => messages.test.setup('notification.email'));

  describe('token exists', () => {
    let download, acsToken;

    before(async () => {
      await removeDocuments();

      const chain = await test.helpers.user();
      user = chain.user;

      acsToken = helper.getAccessTokenForUser(user);
      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);
    });

    describe('with activity feed', () => {
      before(() => {
        sinon.stub(downloadMiddleware.internals, 'saveActivityFeed');
        sinon.stub(s3Util, 'getSignedUrl').returns(faker.internet.url());
        sinon.stub(downloadMiddleware.internals, 'createLocalDirectory');
        sinon.stub(downloadMiddleware.internals, 'compressUserFiles');
        sinon.stub(downloadMiddleware.internals, 'uploadToS3');
        sinon.stub(downloadMiddleware.internals.downloadQueue, 'add');
      });

      after(() => {
        sinon.restore();
      });

      before('request download', async () => {
        try {
          const data = {
            // using Yale to test brand handling
            brand: constants.Brands.YALE,
            activity: true,
            videos: true,
            images: true,
          };
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          response = await axios.post(path, data, options);
          download = response.data;
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 202', () => {
        assert.strictEqual(response.status, 202);
      });

      it('should return download ID', () => {
        assert.isObject(download);
      });

      it('should insert a user download document', async () => {
        const userDownload = await model.usersDownloads.findByID(download._id.toString());

        assert.exists(userDownload);
      });
    });

    describe('without activity feed', () => {
      let downloadQueueAddStub;
      const brand = constants.Brands.YALE;

      before(() => {
        sinon.stub(downloadMiddleware.internals, 'saveActivityFeed');
        sinon.stub(s3Util, 'getSignedUrl').returns(faker.internet.url());
        sinon.stub(downloadMiddleware.internals, 'createLocalDirectory');
        sinon.stub(downloadMiddleware.internals, 'compressUserFiles');
        sinon.stub(downloadMiddleware.internals, 'uploadToS3');

        downloadQueueAddStub = sinon.stub(downloadMiddleware.internals.downloadQueue, 'add');
      });

      after(() => {
        sinon.restore();
      });

      before(async () => {
        try {
          const data = {
            brand,
            activity: false,
            videos: true,
            images: true,
          };
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          response = await axios.post(path, data, options);
          download = response.data;
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 202', () => {
        assert.strictEqual(response.status, 202);
      });

      it('should return download ID', () => {
        assert.isObject(download);
      });

      it('should insert a user download document', async () => {
        const userDownload = await model.usersDownloads.findByID(download._id.toString());

        assert.exists(userDownload);
      });

      it('should insert a download queue job', async () => {
        assert.equal(downloadQueueAddStub.callCount, 1);

        const userDownload = await model.usersDownloads.findByID(download._id.toString());
        delete userDownload.createdAt;
        delete userDownload.updatedAt;
        delete userDownload.expiresAt;
        delete userDownload.status; // might have been changed already by being processed through the queue

        const expectedJob = {
          accessToken: acsToken.toJWT(),
          userID: user._id,
          brand,
          userDownload,
          includeActivity: false,
          includeVideos: true,
          includeImages: true,
        };

        const actualJob = downloadQueueAddStub.getCall(0).args[0];
        delete actualJob.localDirectory; // internally random through uuid
        delete actualJob.userDownload.createdAt;
        delete actualJob.userDownload.updatedAt;
        delete actualJob.userDownload.expiresAt;
        delete actualJob.userDownload.status;

        assert.deepEqual(actualJob, expectedJob);
      });
    });

    describe('without videos', () => {
      before(() => {
        sinon.stub(downloadMiddleware.internals, 'saveActivityFeed');
        sinon.stub(s3Util, 'getSignedUrl').returns(faker.internet.url());
        sinon.stub(downloadMiddleware.internals, 'createLocalDirectory');
        sinon.stub(downloadMiddleware.internals, 'compressUserFiles');
        sinon.stub(downloadMiddleware.internals, 'uploadToS3');
        sinon.stub(downloadMiddleware.internals.downloadQueue, 'add');
      });

      after(() => {
        sinon.restore();
      });

      before(async () => {
        try {
          const data = {
            brand: constants.Brands.YALE,
            activity: true,
            videos: false,
            images: true,
          };
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          response = await axios.post(path, data, options);
          download = response.data;
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 202', () => {
        assert.strictEqual(response.status, 202);
      });

      it('should return download ID', () => {
        assert.isObject(download);
      });

      it('should insert a user download document', async () => {
        const userDownload = await model.usersDownloads.findByID(download._id.toString());

        assert.exists(userDownload);
      });
    });

    describe('without images', () => {
      before(() => {
        sinon.stub(downloadMiddleware.internals, 'saveActivityFeed');
        sinon.stub(s3Util, 'getSignedUrl').returns(faker.internet.url());
        sinon.stub(downloadMiddleware.internals, 'createLocalDirectory');
        sinon.stub(downloadMiddleware.internals, 'compressUserFiles');
        sinon.stub(downloadMiddleware.internals, 'uploadToS3');
        sinon.stub(downloadMiddleware.internals.downloadQueue, 'add');
      });

      after(() => {
        sinon.restore();
      });

      before(async () => {
        try {
          const data = {
            brand: constants.Brands.YALE,
            activity: true,
            videos: true,
            images: false,
          };
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          response = await axios.post(path, data, options);
          download = response.data;
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 202', () => {
        assert.strictEqual(response.status, 202);
      });

      it('should return download ID', () => {
        assert.isObject(download);
      });

      it('should insert a user download document', async () => {
        const userDownload = await model.usersDownloads.findByID(download._id.toString());

        assert.exists(userDownload);
      });
    });
  });

  describe('token does not exist', () => {
    it('should return a 401', async () => {
      try {
        const data = {};
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        };

        response = await axios.post(path, data, options);
        assert.fail('expected request to fail');
      } catch (err) {
        assert.strictEqual(err.response.status, 401);
      }
    });
  });
});
