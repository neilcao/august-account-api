'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const constants = require('august-constants');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const test = require('august-test');

const config = require('../../config/index.js');
const download = require('../../middleware/download.js');
const helper = require('../helper.js');
const session = require('../../middleware/session.js');

const path = '/downloads';

const ACCESS_TOKEN_HEADER = config.get('accessTokenHeader');

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`DELETE ${path}`, () => {
  let response;
  let user;

  describe('token exists', () => {
    let expectedDownload;
    let deleteStub;
    let url;

    describe('user owns the download', () => {
      before('set up user download', async () => {
        user = (await test.helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        const fake = await test.fake.userDownload({
          params: {
            userDownload: {
              userID: user._id,
            },
          },
        });

        expectedDownload = await model.usersDownloads.insert(fake.userID, fake.status, fake.expiresAt);
        url = faker.internet.url();
        await model.usersDownloads.update(expectedDownload._id.toString(), { url });

        deleteStub = sinon.stub(download.internals, 'deleteFromS3');
      });

      before(`call DELETE ${path}`, async () => {
        try {
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          response = await axios.delete(`${path}/${expectedDownload._id.toString()}`, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => deleteStub.restore());

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should call the delete s3 function', () => {
        sinon.assert.calledOnce(deleteStub);
        sinon.assert.calledWithExactly(deleteStub, url, user._id);
      });

      it('should mark the database document as deleted', async () => {
        const actualDownload = await model.usersDownloads.findByID(expectedDownload._id);
        assert.strictEqual(actualDownload.status, constants.UserDownloadStatuses.DELETED);
      });
    });

    describe('user does not own the download', () => {
      before(async () => {
        user = (await test.helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        const fake = await test.fake.userDownload({
          params: {
            userDownload: {
              userID: faker.random.uuid(),
            },
          },
        });

        expectedDownload = await model.usersDownloads.insert(fake.userID, fake.status, fake.expiresAt);
        url = faker.internet.url();
        await model.usersDownloads.update(expectedDownload._id.toString(), { url });

        deleteStub = sinon.stub(download.internals, 'deleteFromS3');

        try {
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          response = await axios.delete(`${path}/${expectedDownload._id.toString()}`, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => deleteStub.restore());

      it('should return a 403', () => {
        assert.strictEqual(response.status, 403);
      });

      it('should not call the delete s3 function', () => {
        sinon.assert.notCalled(deleteStub);
      });

      it('should not mark the database document as deleted', async () => {
        const actualDownload = await model.usersDownloads.findByID(expectedDownload._id);
        assert.strictEqual(actualDownload.status, expectedDownload.status);
      });
    });
  });

  describe('token does not exist', () => {
    before(async () => {
      try {
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        };

        response = await axios.delete(`${path}/${faker.random.uuid()}`, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });
});
