'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');

const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const config = require('../../config/index.js');
const download = require('../../middleware/download.js');
const helper = require('../helper.js');
const session = require('../../middleware/session.js');

const path = '/downloads';

const ACCESS_TOKEN_HEADER = config.get('accessTokenHeader');

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`GET ${path}`, () => {
  let response;
  let user;

  describe('token exists', () => {
    const numDownloads = 2;
    let expectedDownloads;
    let actualDownloads;

    before(async () => {
      user = (await Test.Helpers.user()).user;

      const acsToken = helper.getAccessTokenForUser(user);
      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      for (let i = 0; i < numDownloads; i++) {
        const fake = await Test.Fakev1.userDownload({
          params: {
            userDownload: {
              userID: user._id,
            },
          },
        });
        const download = await model.usersDownloads.insert(fake.userID, fake.status, fake.expiresAt);
        await model.usersDownloads.update(download._id.toString(), {
          url: faker.internet.url(),
        });
      }

      expectedDownloads = await model.usersDownloads.findActiveByUserID(user._id, {
        userID: 0,
        updatedAt: 0,
      });
      download.internals.makeHumanReadable(expectedDownloads);

      try {
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
          },
        };

        response = await axios.get(path, options);
        actualDownloads = response.data;
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should return a downloads array', () => {
      for (let i = 0; i < actualDownloads.length; i++) {
        assert.strictEqual(actualDownloads[0]._id.toString(), expectedDownloads[0]._id.toString());
        assert.strictEqual(actualDownloads[0].status, expectedDownloads[0].status);
        assert.strictEqual(actualDownloads[0].createdAt, expectedDownloads[0].createdAt);
        assert.strictEqual(actualDownloads[0].expiresAt, expectedDownloads[0].expiresAt);
        assert.strictEqual(actualDownloads[0].url, expectedDownloads[0].url);
      }
    });
  });

  describe('token does not exist', () => {
    before(async () => {
      try {
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        };

        response = await axios.get(path, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });
});
