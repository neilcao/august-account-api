'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const ms = require('ms');
const sinon = require('sinon');

const AccessToken = require('august-access-token');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const acs = require('../acs.js');
const config = require('../config/index.js');
const session = require('../middleware/session.js');

const path = '/login/polyauth';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe('POST /login/polyauth', () => {
  describe('valid wechat login', () => {
    let loginStub, response, user, userToken;
    const expectedExpirationMS = Date.now() + ms('59 minutes');
    const identifierType = 'wechat';
    const identifierValue = faker.random.uuid();
    let tokenJWT;

    before(async () => {
      user = (await Test.Helpers.user()).user;
      const expectTokenValue = {
        hasPhone: true,
        hasPolyAuth: true,
        vPolyAuth: true,
        expiresAt: new Date(expectedExpirationMS).toISOString(),
      };
      loginStub = stubAcsLogin(user, expectTokenValue);
    });

    after(() => {
      loginStub.restore();
    });
    describe('not rate limited', () => {
      before(async () => {
        try {
          response = await axios.post(path, {
            identifierType: identifierType,
            identifier: identifierValue,
          });
        } catch (err) {
          response = err.response;
        }
      });

      before(async () => {
        userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
        tokenJWT = userToken.token.split(':')[1];
      });

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should return an encrypted user ID', () => {
        assert.strictEqual(
          jsUtil.crypto.decrypt(response.data.tokenToSend, session.test.getEncryptionPassword()),
          user._id
        );
      });

      it('should insert a document in the userstokens collection', () => {
        const token = new AccessToken();
        token.parseJWT(tokenJWT);

        assert(userToken);
        assert.equal(userToken.UserID, user._id);
      });
    });

    describe('rate limited', () => {
      before(async () => {
        try {
          response = await axios.post(path, {
            identifierType: identifierType,
            identifier: identifierValue,
          });
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 429', () => {
        assert.strictEqual(response.status, 429);
      });

      it('should set the Retry-After header', () => {
        assert.strictEqual(response.headers['retry-after'], String(config.get('limits:login:timeframeInMS') / 1000));
      });
    });
  });

  describe('missing identifier', () => {
    let response, user, userToken;
    before(async () => {
      user = (await Test.Helpers.user()).user;

      try {
        response = await axios.post(path, {});
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    it('should return a 409', () => {
      assert.equal(response.status, 400);
    });

    it('should not insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });
  });
  describe('not enough factors in access token from rest api', () => {
    let loginStub, response, user, userToken;
    const expectedExpirationMS = Date.now() + ms('59 minutes');
    const identifierType = 'wechat';
    const identifierValue = faker.random.uuid();

    before(async () => {
      user = (await Test.Helpers.user()).user;
      const tokenValue = {
        hasPhone: true,
        hasPolyAuth: true,
        vPolyAuth: false,
        expiresAt: new Date(expectedExpirationMS).toISOString(),
      };
      loginStub = stubAcsLogin(user, tokenValue);

      try {
        response = await axios.post(path, {
          identifierType: identifierType,
          identifier: identifierValue,
        });
      } catch (err) {
        response = err.response;
      }
    });

    after(() => {
      loginStub.restore();
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should not insert document in the userstokens collection', () => {
      assert.notExists(userToken);
    });
  });
  describe('no userID in access token from rest api', () => {
    let loginStub, response, user, userToken;
    const identifierType = 'wechat';
    const identifierValue = faker.random.uuid();

    before(async () => {
      user = (await Test.Helpers.user()).user;
      const expectTokenValue = {
        hasPhone: true,
        hasPolyAuth: true,
        vPolyAuth: true,
      };
      loginStub = stubAcsLogin({}, expectTokenValue);

      try {
        response = await axios.post(path, {
          identifierType: identifierType,
          identifier: identifierValue,
        });
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    after(() => {
      loginStub.restore();
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });
  });
  describe('unbind wechat login', () => {
    let loginStub, response, user;
    const expectedExpirationMS = Date.now() + ms('59 minutes');
    const identifierType = 'wechat';
    const identifierValue = faker.random.uuid();

    before(async () => {
      user = (await Test.Helpers.user()).user;
      const expectTokenValue = {
        vPassword: false,
        hasPassword: true,
        hasPhone: true,
        hasPolyAuth: false,
        vPolyAuth: false,
        expiresAt: new Date(expectedExpirationMS).toISOString(),
      };
      loginStub = stubAcsLogin(user, expectTokenValue);
    });

    after(() => {
      loginStub.restore();
    });
    describe('Token invalid', () => {
      before(async () => {
        try {
          response = await axios.post(
            path,
            {
              identifierType: identifierType,
              identifier: identifierValue,
            },
            {
              headers: {
                [config.get('callingUserHeader')]: user._id,
              },
            }
          );
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 401', () => {
        assert.strictEqual(response.status, 401);
      });
    });
  });
});

/**
 * Create test stubs.
 *
 * @param {Object} user -
 * @param {object} tokenValue - expect token value
 * @returns {Array} The test stubs
 */
function stubAcsLogin(user, tokenValue) {
  const assignValue = Object.assign(tokenValue, {
    userId: user._id,
    LastName: user.LastName,
    FirstName: user.FirstName,
  });
  const acsToken = new AccessToken();
  Object.assign(acsToken._token, assignValue);

  return sinon.stub(acs, 'polyAuthlogin').callsFake(() => {
    return {
      status: 200,
      headers: {
        [config.get('acsAccessTokenHeader')]: acsToken.toJWT(),
      },
      data: acsToken._token,
    };
  });
}
