'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const constants = require('august-constants');
const babelfish = require('august-babelfish');
const entities = require('august-html-entities');
const jsUtil = require('august-js-utility');
const messages = require('august-messages');
const model = require('august-model');
const test = require('august-test');

const acs = require('../../acs.js');
const config = require('../../config/index.js');
const helper = require('../helper.js');
const session = require('../../middleware/session.js');

const path = '/user';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`DELETE ${path}`, () => {
  let response;
  let user;
  let email1;
  const email2 = faker.internet.email();
  let acsStub;
  let acsToken;

  before(() => messages.test.setup('notification.email'));

  describe('valid request', () => {
    before('set up user', async () => {
      const chain = await test.helpers.user();
      user = chain.user;
      email1 = model.userIdentifiers.getValue(chain.userIdentifiers.email);
      chain.email = email2;
      await test.mock.userIdentifier(chain, 'email');

      acsToken = helper.getAccessTokenForUser(user);
      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      acsStub = sinon.stub(acs, 'deleteUser').callsFake(() => {
        return {
          status: 200,
        };
      });

      try {
        response = await axios.delete(path, {
          headers: {
            [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(
              acsToken.UserID,
              session.test.getEncryptionPassword()
            ),
          },
          params: {
            // using Yale to test brand handling
            brand: constants.Brands.YALE,
          },
        });
      } catch (err) {
        response = err.response;
      }
    });

    after(() => acsStub.restore());

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should call the delete user endpoint on acs', () => {
      sinon.assert.calledOnce(acsStub);
      sinon.assert.calledWithExactly(acsStub, acsToken.toJWT(), constants.Brands.YALE);
    });

    it('should send email(s) to the user', async () => {
      const publishedMessages = await messages.test.getMessages('notification.email', 50);

      assert.deepEqual(publishedMessages[0], {
        from: config.get('email'),
        to: email2,
        subject: babelfish.t('deleteUserEmailSubject', { user, brand: constants.Brands.YALE }),
        event: 'user_deleted',
        template: {
          name: 'user-deleted.html',
          data: {
            name: entities.unescape(user.FirstName),
          },
        },
      });

      assert.deepEqual(publishedMessages[1], {
        from: config.get('email'),
        to: email1,
        subject: babelfish.t('deleteUserEmailSubject', { user, brand: constants.Brands.YALE }),
        event: 'user_deleted',
        template: {
          name: 'user-deleted.html',
          data: {
            name: entities.unescape(user.FirstName),
          },
        },
      });
    });
  });

  describe('invalid account management token', () => {
    before(async () => {
      try {
        response = await axios.get(path, {
          headers: {
            [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        });
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });

  describe('no account management token', () => {
    before(async () => {
      try {
        response = await axios.get(path);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });
});
