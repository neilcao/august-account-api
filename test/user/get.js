'use strict';

const _ = require('lodash');
const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const entities = require('august-html-entities');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const acs = require('../../acs.js');
const config = require('../../config/index.js');
const helper = require('../helper.js');
const session = require('../../middleware/session.js');

const getUserPath = '/user';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`GET ${getUserPath}`, () => {
  let response;
  let user;
  let emails;
  let phones;
  let acsGetUserStub;
  let acsGetAppsStub;
  let acsGetLocksStub;
  let acsGetDoorbellsStub;

  describe('valid request', () => {
    const mockUserData = {
      FirstName: 'First Name &apos;',
      LastName: 'Last Name &amp;',
      [faker.lorem.word()]: faker.lorem.words(),
      [faker.lorem.word()]: faker.lorem.words(),
    };

    before(async () => {
      const chain = await Test.Helpers.user();
      user = chain.user;

      emails = [faker.internet.email(), model.userIdentifiers.getValue(chain.userIdentifiers.email)];
      phones = [faker.phone.phoneNumber('+###########'), model.userIdentifiers.getValue(chain.userIdentifiers.phone)];
      await model.userIdentifiers.upsertByUserIDAndValue(user._id, `email:${emails[0]}`);
      await model.userIdentifiers.upsertByUserIDAndValue(user._id, `phone:${phones[0]}`);

      const acsToken = helper.getAccessTokenForUser(user);
      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      acsGetUserStub = sinon.stub(acs, 'getUser').callsFake(() => {
        return {
          status: 200,
          data: _.clone(mockUserData),
        };
      });
      acsGetAppsStub = sinon.stub(acs, 'getUserApps');
      acsGetLocksStub = sinon.stub(acs, 'getUserLocks');
      acsGetDoorbellsStub = sinon.stub(acs, 'getUserDoorbells');

      try {
        response = await axios.get(getUserPath, {
          headers: {
            [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(
              acsToken.UserID,
              session.test.getEncryptionPassword()
            ),
          },
        });
      } catch (err) {
        response = err.response;
      }
    });

    after(() => {
      acsGetUserStub.restore();
      acsGetAppsStub.restore();
      acsGetLocksStub.restore();
      acsGetDoorbellsStub.restore();
    });

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should return the user data', () => {
      const expectedInfo = _.clone(mockUserData);
      ['FirstName', 'LastName'].forEach(key => (expectedInfo[key] = entities.unescape(expectedInfo[key])));
      assert.deepEqual(response.data.info, expectedInfo);
      assert.deepEqual(response.data.emails, emails);
      assert.deepEqual(response.data.phones, phones);
    });
  });

  describe('invalid account management token', () => {
    before(async () => {
      try {
        response = await axios.get(getUserPath, {
          headers: {
            [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        });
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });

  describe('no account management token', () => {
    before(async () => {
      try {
        response = await axios.get(getUserPath);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });
});
