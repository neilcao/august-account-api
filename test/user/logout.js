'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const jsUtil = require('august-js-utility');
const constants = require('august-constants');
const model = require('august-model');
const Test = require('august-test/v2');

const acs = require('../../acs.js');
const config = require('../../config/index.js');
const helper = require('../helper.js');
const session = require('../../middleware/session.js');

const path = '/user/logout';
const brand = constants.Brands.YALE;

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`PUT ${path}`, () => {
  let response;
  let user;
  let acsToken;
  let acsGetUserStub;
  let acsLogoutStub;

  describe('valid request', () => {
    const mockUserResponse = {
      Email: faker.internet.email(),
      PhoneNo: faker.phone.phoneNumber(),
      FirstName: faker.name.firstName(),
      LastName: faker.name.lastName(),
    };

    before(async () => {
      const chain = await Test.Helpers.user();
      user = chain.user;

      acsToken = helper.getAccessTokenForUser(user);
      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      acsGetUserStub = sinon.stub(acs, 'getUser').callsFake(() => {
        return {
          status: 200,
          data: mockUserResponse,
        };
      });
      acsLogoutStub = sinon.stub(acs, 'userAppLogout').callsFake(() => {
        return {
          status: 200,
          data: {},
        };
      });

      try {
        const body = {};
        response = await axios.put(path, body, {
          headers: {
            [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(
              acsToken.UserID,
              session.test.getEncryptionPassword()
            ),
            [config.get('brandingHeader')]: brand,
          },
        });
      } catch (err) {
        response = err.response;
      }
    });

    after(() => {
      acsGetUserStub.restore();
      acsLogoutStub.restore();
    });

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should call acs to get user info', () => {
      sinon.assert.calledOnce(acsGetUserStub);
      sinon.assert.calledWithExactly(acsGetUserStub, acsToken.toJWT(), brand);
    });

    it('should call acs to log the user out', () => {
      sinon.assert.calledOnce(acsLogoutStub);
      sinon.assert.calledWithExactly(acsLogoutStub, acsToken.toJWT(), brand, mockUserResponse);
    });
  });

  describe('invalid account management token', () => {
    before(async () => {
      try {
        const body = {};
        response = await axios.put(path, body, {
          headers: {
            [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
            [config.get('brandingHeader')]: brand,
          },
        });
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });

  describe('no account management token', () => {
    before(async () => {
      try {
        const body = {};
        response = await axios.put(path, body);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });
});
