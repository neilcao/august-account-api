'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const ms = require('ms');
const sinon = require('sinon');

const AccessToken = require('august-access-token');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const acs = require('../acs.js');
const config = require('../config/index.js');
const session = require('../middleware/session.js');

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe('POST /validate/polyauth/:identifier', () => {
  describe('valid password validate', () => {
    let verificationStub, response, user, userToken, acsToken;
    const identifierType = 'password';
    const identifierValue = faker.random.uuid();
    const path = `/validate/polyauth/${identifierType}`;
    const data = {
      [identifierType]: identifierValue,
    };

    const expectedExpirationMS = Date.now() + ms('59 minutes');

    before(async () => {
      const chain = await Test.Helpers.user();
      user = chain.user;

      const assignValue = {
        userId: user._id,
        LastName: user.LastName,
        FirstName: user.FirstName,
        hasPhone: true,
        vPassword: true,
        hasPolyAuth: true,
        vPolyAuth: true,
        expiresAt: new Date(expectedExpirationMS).toISOString(),
      };
      acsToken = getAcsToken(assignValue);
      const refreshToken = null;
      await model.usersTokens.upsert(
        user._id,
        `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`,
        expectedExpirationMS,
        refreshToken,
        new Date(expectedExpirationMS)
      );
      verificationStub = stubValidatePolyAuthIdentifier(acsToken);

      try {
        response = await axios.post(path, data, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    after(() => verificationStub.restore());

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should return an account management token', () => {
      const actualUserID = jsUtil.crypto.decrypt(response.data, session.test.getEncryptionPassword());
      const expectedUserID = user._id;

      assert.strictEqual(actualUserID, expectedUserID);
    });

    it('should update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const jwtExpiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(
        userToken.autoExpiresAt.getTime() - Date.now() - config.get('tokenExpirationTimeInMS'),
        0,
        100
      );
      assert.approximately(userToken.expiresAt - Date.now() - config.get('tokenExpirationTimeInMS'), 0, 100);
      assert.approximately(jwtExpiresAtMS - Date.now() - config.get('tokenExpirationTimeInMS'), 0, 100);
      assert.approximately(jwtExpiresAtMS / 1000, token._token.exp, 1);
    });
  });
  describe('not enough factors in token from rest api', () => {
    let verificationStub, response, user, userToken, acsToken;
    const identifierType = 'password';
    const identifierValue = faker.random.uuid();
    const path = `/validate/polyauth/${identifierType}`;
    const data = {
      [identifierType]: identifierValue,
    };

    const expectedExpirationMS = Date.now() + ms('59 minutes');

    before(async () => {
      const chain = await Test.Helpers.user();
      user = chain.user;

      const assignValue = {
        userId: user._id,
        LastName: user.LastName,
        FirstName: user.FirstName,
        hasPhone: true,
        vPassword: false,
        hasPolyAuth: true,
        vPolyAuth: true,
        expiresAt: new Date(expectedExpirationMS).toISOString(),
      };
      acsToken = getAcsToken(assignValue);

      const refreshToken = null;
      await model.usersTokens.upsert(
        user._id,
        `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`,
        expectedExpirationMS,
        refreshToken,
        new Date(expectedExpirationMS)
      );

      verificationStub = stubValidatePolyAuthIdentifier(acsToken);

      try {
        response = await axios.post(path, data, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    after(() => verificationStub.restore());

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should not update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const expiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(userToken.autoExpiresAt.getTime(), expectedExpirationMS, 100);
      assert.approximately(userToken.expiresAt, expectedExpirationMS, 100);
      assert.approximately(expiresAtMS, expectedExpirationMS, 100);
    });
  });

  describe('missing params', () => {
    let verificationStub, response, user, userToken, acsToken;
    const identifierType = 'password';
    const path = `/validate/polyauth/${identifierType}`;
    const data = {};

    const expectedExpirationMS = Date.now() + ms('59 minutes');

    before(async () => {
      const chain = await Test.Helpers.user();
      user = chain.user;

      const assignValue = {
        userId: user._id,
        LastName: user.LastName,
        FirstName: user.FirstName,
        hasPhone: true,
        vPassword: false,
        hasPolyAuth: true,
        vPolyAuth: true,
        expiresAt: new Date(expectedExpirationMS).toISOString(),
      };
      acsToken = getAcsToken(assignValue);

      const refreshToken = null;
      await model.usersTokens.upsert(
        user._id,
        `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`,
        expectedExpirationMS,
        refreshToken,
        new Date(expectedExpirationMS)
      );

      verificationStub = stubValidatePolyAuthIdentifier(acsToken);

      try {
        response = await axios.post(path, data, getPostOptions(user));
      } catch (err) {
        response = err.response;
      }

      userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
    });

    after(() => verificationStub.restore());

    it('should return a 409', () => {
      assert.strictEqual(response.status, 409);
    });

    it('should not update the document in the userstokens collection', () => {
      const tokenStr = userToken.token.split(':')[1];
      const token = new AccessToken();
      token.parseJWT(tokenStr);
      const expiresAtMS = new Date(token.expiresAt).getTime();

      assert.approximately(userToken.autoExpiresAt.getTime(), expectedExpirationMS, 100);
      assert.approximately(userToken.expiresAt, expectedExpirationMS, 100);
      assert.approximately(expiresAtMS, new Date(acsToken.expiresAt).getTime(), 100);
    });
  });
});

/**
 *
 * @param {Object} token -
 * @returns {Object} The stub
 */
function stubValidatePolyAuthIdentifier(token) {
  return sinon.stub(acs, 'validatePolyAuthIdentifier').returns(
    Promise.resolve({
      status: 200,
      headers: {
        [config.get('acsAccessTokenHeader')]: token.toJWT(),
      },
    })
  );
}

/**
 *
 * @param {Object} user -
 * @returns {Object} The options to pass to the HTTP post method.
 */
function getPostOptions(user) {
  return {
    headers: {
      [config.get('accessTokenHeader')]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
    },
  };
}

/**
 * generate user token
 *
 * @param {object} tokenValue - token values
 * @returns {object} user token
 */
function getAcsToken(tokenValue) {
  const token = new AccessToken();
  Object.assign(token._token, tokenValue);

  return token;
}
