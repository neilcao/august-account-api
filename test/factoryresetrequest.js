'use strict';

const uuid = require('uuid');
const assert = require('chai').assert;
const axios = require('axios');
const nock = require('nock');
const _ = require('lodash');

const constants = require('august-constants');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const random = require('august-random');
const test = require('august-test');
const messages = require('august-messages');

const helper = require('./helper.js');
const config = require('../config/index');
const session = require('../middleware/session.js');

const ACCESS_TOKEN_HEADER = config.get('accessTokenHeader');

const path = '/factoryresetrequests';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

const acsPort = config.get('acs:port');
const acsUrl = config.get('acs:host') + (acsPort ? `:${acsPort}` : '');

describe('GET /factoryresetrequests', function() {
  let res, responseData, acsToken, expectedDocuments, unexpectedDocument;

  before('insert documents', async function() {
    let chain;

    chain = await test.helpers.lock();
    const otherLockID = random.generateRandomString(32, 'hex').toUpperCase();
    const otherserialNumber = 'L5ABCDEF';
    await model.locks.collection.insertOne({ LockID: otherLockID, serialNumber: otherserialNumber, LockName: 'Test' });
    await model.lockSuperUsers.insert(otherLockID, chain.user._id);
    await test.helpers.lock(chain.user);
    expectedDocuments = await Promise.all([
      model.factoryResetRequest.insert(constants.Devices.LOCK, chain.lock.serialNumber, uuid.v4()),
      model.factoryResetRequest.insert(constants.Devices.LOCK, otherserialNumber, uuid.v4()),
      model.factoryResetRequest.insert(constants.Devices.LOCK, 'L3ABCDEF', chain.user._id),
      model.factoryResetRequest.insert(constants.Devices.KEYPAD, 'K1ABCDEF', chain.user._id),
    ]);
    unexpectedDocument = await model.factoryResetRequest.insert(
      constants.Devices.LOCK,
      'notUsedSerialNumber',
      uuid.v4()
    );

    acsToken = helper.getAccessTokenForUser(chain.user);
    await model.usersTokens.upsert(chain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

    const options = {
      headers: {
        [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(chain.user._id, session.test.getEncryptionPassword()),
      },
    };
    try {
      res = await axios.get(path, options);
      responseData = res.data;
    } catch (err) {
      responseData = [];
    }
  });

  after('remove docs', async function() {
    await model.factoryResetRequest.collection.remove({});
  });

  it('should get correct factory reset requests', function() {
    for (const doc of expectedDocuments) {
      doc._id = doc._id.toString();
      delete doc.createdAt;
      delete doc.updatedAt;
    }
    for (const doc of responseData.factoryResetRequests) {
      delete doc.createdAt;
      delete doc.updatedAt;
    }
    let count = 0;
    for (const expected of expectedDocuments) {
      for (const actual of responseData.factoryResetRequests) {
        if (expected.serialNumber === actual.serialNumber) {
          assert.deepEqual(_.omit(actual, 'LockName'), expected);
          count++;
        }

        if (actual.serialNumber === unexpectedDocument.serialNumber) {
          assert.fail(`${unexpectedDocument.serialNumber} should not appear in GET ${path}`);
        }
      }
    }
    assert.equal(count, 4, `Expected 4 total factory reset requests, got ${count}`);
  });
});

describe('PUT /factoryresetrequests', function() {
  let response, acsToken;

  before(async () => {
    await messages.test.setup('event.resetrequest.denied');
    await messages.test.setup('event.resetrequest.approved');
  });

  describe('with invalid params', function() {
    before('insert documents', async function() {
      let chain;

      chain = await test.helpers.lock();
      acsToken = helper.getAccessTokenForUser(chain.user);
      await model.usersTokens.upsert(chain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      const options = {
        headers: {
          [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(chain.user._id, session.test.getEncryptionPassword()),
        },
      };
      const body = {};
      try {
        response = await axios.put(path, body, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should get correct factory reset requests', function() {
      assert.strictEqual(response.status, 409);
    });
  });

  describe('with non-superuser permissions params', function() {
    before('insert documents', async function() {
      let chain;

      chain = await test.helpers.lock();
      await model.lockSuperUsers.removeByUserID(chain.user._id);
      acsToken = helper.getAccessTokenForUser(chain.user);
      await model.usersTokens.upsert(chain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      const options = {
        headers: {
          [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(chain.user._id, session.test.getEncryptionPassword()),
        },
      };
      const body = { serialNumber: chain.lock.serialNumber };
      try {
        response = await axios.put(path, body, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should respond with Forbidden', function() {
      assert.strictEqual(response.status, 403);
    });
  });

  describe('with no request ID', function() {
    before('insert documents', async function() {
      let chain;

      chain = await test.helpers.lock();
      acsToken = helper.getAccessTokenForUser(chain.user);
      await model.usersTokens.upsert(chain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      const options = {
        headers: {
          [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(chain.user._id, session.test.getEncryptionPassword()),
        },
      };
      const body = { serialNumber: chain.lock.serialNumber };
      try {
        response = await axios.put(path, body, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should respond with Forbidden', function() {
      assert.strictEqual(response.status, 400);
    });
  });

  describe('with undefined approval boolean', function() {
    before('insert documents', async function() {
      let chain;

      chain = await test.helpers.lock();
      acsToken = helper.getAccessTokenForUser(chain.user);
      await model.usersTokens.upsert(chain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      const options = {
        headers: {
          [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(chain.user._id, session.test.getEncryptionPassword()),
        },
      };
      const body = {
        serialNumber: chain.lock.serialNumber,
        requestID: 'bogus',
      };
      try {
        response = await axios.put(path, body, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should respond with Forbidden', function() {
      assert.strictEqual(response.status, 400);
    });
  });

  describe('approved success', function() {
    let acsDeleteLockScope, requestDoc, lockChain;
    let resetRequestUserChain;

    before('insert documents and setup nock and make request', async function() {
      lockChain = await test.helpers.lock();
      resetRequestUserChain = await test.helpers.user();
      requestDoc = await model.factoryResetRequest.insert(
        constants.Devices.LOCK,
        lockChain.lock.serialNumber,
        resetRequestUserChain.user._id
      );
      acsToken = helper.getAccessTokenForUser(lockChain.user);
      await model.usersTokens.upsert(lockChain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      const expectedResponse = {
        message: 'success',
      };
      nock.cleanAll();
      acsDeleteLockScope = nock(acsUrl, { allowUnmocked: true })
        .delete(`/locks/${lockChain.lock.LockID}`)
        .reply(200, expectedResponse);

      const options = {
        headers: {
          [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(lockChain.user._id, session.test.getEncryptionPassword()),
        },
      };
      const body = {
        serialNumber: lockChain.lock.serialNumber,
        requestID: requestDoc._id.toString(),
        approved: true,
      };
      try {
        response = await axios.put(path, body, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should respond with success', function() {
      assert.strictEqual(response.status, 200);
      assert.isTrue(acsDeleteLockScope.isDone());
    });

    it('should publish event.resetrequest.approved', async function() {
      const publishedMessages = await messages.test.getMessages('event.resetrequest.approved', 500);
      assert.lengthOf(publishedMessages, 1);
      const approvedMessage = publishedMessages[0];

      assert.strictEqual(approvedMessage.resetRequestID, requestDoc._id.toString());
      assert.strictEqual(approvedMessage.serialNumber, requestDoc.serialNumber);
      assert.strictEqual(approvedMessage.requestingUserID, resetRequestUserChain.user._id);
      assert.strictEqual(approvedMessage.updatingUserID, lockChain.user._id);
      assert.strictEqual(approvedMessage.lock.LockID, lockChain.lock.LockID);
      assert.strictEqual(approvedMessage.lock.appBrand, lockChain.lock.appBrand);
    });
  });

  describe('denied success', function() {
    let acsDeleteLockScope, requestDoc, lockChain;
    let resetRequestUserChain;

    before('insert documents and setup nock and make request', async function() {
      lockChain = await test.helpers.lock();
      resetRequestUserChain = await test.helpers.user();
      requestDoc = await model.factoryResetRequest.insert(
        constants.Devices.LOCK,
        lockChain.lock.serialNumber,
        resetRequestUserChain.user._id
      );
      acsToken = helper.getAccessTokenForUser(lockChain.user);
      await model.usersTokens.upsert(lockChain.user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      const expectedResponse = {
        message: 'success',
      };
      nock.cleanAll();
      acsDeleteLockScope = nock(acsUrl, { allowUnmocked: true })
        .delete(`/locks/${lockChain.lock.LockID}`)
        .reply(200, expectedResponse);

      const options = {
        headers: {
          [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(lockChain.user._id, session.test.getEncryptionPassword()),
        },
      };
      const body = {
        serialNumber: lockChain.lock.serialNumber,
        requestID: requestDoc._id.toString(),
        approved: false,
      };
      try {
        response = await axios.put(path, body, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should respond with success', function() {
      assert.strictEqual(response.status, 200);
      assert.isFalse(acsDeleteLockScope.isDone());
    });

    it('should publish event.resetrequest.denied', async function() {
      const publishedMessages = await messages.test.getMessages('event.resetrequest.denied', 500);
      assert.isNotEmpty(publishedMessages);
      const approvedMessage = publishedMessages[0];
      assert.strictEqual(approvedMessage.resetRequestID, requestDoc._id.toString());
      assert.strictEqual(approvedMessage.serialNumber, requestDoc.serialNumber);
      assert.strictEqual(approvedMessage.requestingUserID, resetRequestUserChain.user._id);
      assert.strictEqual(approvedMessage.updatingUserID, lockChain.user._id);
    });
  });

  after('remove docs', async function() {
    await model.factoryResetRequest.collection.remove({});
  });
});
