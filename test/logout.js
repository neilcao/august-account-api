'use strict';

const assert = require('chai').assert;
const axios = require('axios');

const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const config = require('../config/index.js');
const helper = require('./helper.js');
const session = require('../middleware/session.js');

const path = '/logout';

const ACCESS_TOKEN_HEADER = config.get('accessTokenHeader');

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${path}`, () => {
  let response;
  let user;

  describe('valid request', () => {
    let userToken;

    before(async () => {
      user = (await Test.Helpers.user()).user;

      const acsToken = helper.getAccessTokenForUser(user);
      await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

      try {
        const data = {};
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
          },
        };

        response = await axios.post(path, data, options);
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });

    it('should remove a document from the userstokens collection', () => {
      assert.notExists(userToken);
    });
  });

  describe('invalid account management token', () => {
    before(async () => {
      try {
        const data = {};
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        };

        response = await axios.post(path, data, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 200', () => {
      assert.strictEqual(response.status, 200);
    });
  });
});
