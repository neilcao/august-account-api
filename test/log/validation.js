const _ = require('lodash');
const assert = require('chai').assert;
const faker = require('faker');
const joi = require('joi');

const log = require('../../validation/log.js');
const shared = require('./shared.js');

const shouldNotHaveValidatedMessage = 'Expected params to fail validation but they did not.';
const shouldHaveValidatedMessage = 'Expected params to pass validation but they did not.';

describe('log schema', () => {
  it('should validate without optional parameters', () => {
    assert.isNull(joi.validate(shared.validParams(), log.processLog).error, shouldHaveValidatedMessage);
  });

  it('should validate with optional parameters', () => {
    assert.isNull(joi.validate(shared.validParamsWithOptional(), log.processLog).error, shouldHaveValidatedMessage);
  });

  it('should not validate with unknown parameters', () => {
    const params = _.cloneDeep(shared.validParams());
    params.body[faker.random.word()] = faker.random.word();

    assert.isNotNull(joi.validate(params, log.processLog).error, shouldNotHaveValidatedMessage);
  });

  it('should not validate with missing parameters', () => {
    let params = _.cloneDeep(shared.validParams());
    delete params.body.component;
    assert.isNotNull(joi.validate(params, log.processLog).error, shouldNotHaveValidatedMessage);

    params = _.cloneDeep(shared.validParams());
    delete params.body.event;
    assert.isNotNull(joi.validate(params, log.processLog).error, shouldNotHaveValidatedMessage);

    params = _.cloneDeep(shared.validParams());
    delete params.body.type;
    assert.isNotNull(joi.validate(params, log.processLog).error, shouldNotHaveValidatedMessage);
  });
});
