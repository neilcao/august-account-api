'use strict';

const _ = require('lodash');
const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const jsUtil = require('august-js-utility');
const logger = require('august-logger');
const model = require('august-model');
const Test = require('august-test/v2');

const config = require('../../config/index.js');
const helper = require('../helper.js');
const session = require('../../middleware/session.js');
const shared = require('./shared.js');

const path = '/log';

const ACCESS_TOKEN_HEADER = config.get('accessTokenHeader');

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${path}`, () => {
  let response;
  let user;
  let loggerStub;
  let expectedParams;

  describe('token exists', () => {
    describe('logger.warn', () => {
      before(async () => {
        user = (await Test.Helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        try {
          expectedParams = _.cloneDeep(shared.validParamsWithOptional({ type: 'warn' }).body);
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          loggerStub = sinon.stub(logger, expectedParams.type);
          response = await axios.post(path, expectedParams, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => loggerStub.restore());

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should call the right logger function', () => {
        const args = loggerStub.getCall(0).args;

        assert.strictEqual(args[0].data.component, expectedParams.component);
        assert.strictEqual(args[0].data.event, expectedParams.event);
        assert.strictEqual(args[0].data[Object.keys(expectedParams)[0]], Object.values(expectedParams)[0]);
      });
    });

    describe('logger.debug', () => {
      before(async () => {
        user = (await Test.Helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        try {
          expectedParams = _.cloneDeep(shared.validParams({ type: 'debug' }).body);
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          loggerStub = sinon.stub(logger, expectedParams.type);
          response = await axios.post(path, expectedParams, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => loggerStub.restore());

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should call the right logger function', () => {
        const args = loggerStub.getCall(0).args;

        assert.strictEqual(args[0].data.component, expectedParams.component);
        assert.strictEqual(args[0].data.event, expectedParams.event);
        assert.strictEqual(args[0].data[Object.keys(expectedParams)[0]], Object.values(expectedParams)[0]);
      });
    });

    describe('logger.error', () => {
      before(async () => {
        user = (await Test.Helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        try {
          expectedParams = _.cloneDeep(
            shared.validParamsWithOptional({
              type: 'error',
              error: faker.lorem.word(),
            }).body
          );

          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          loggerStub = sinon.stub(logger, expectedParams.type);
          response = await axios.post(path, expectedParams, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => loggerStub.restore());

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should call the right logger function', () => {
        const args = loggerStub.getCall(0).args;

        assert.strictEqual(args[0].data.component, expectedParams.component);
        assert.strictEqual(args[0].data.event, expectedParams.event);
        assert.strictEqual(args[0].error.message, expectedParams.error);
      });
    });

    describe('logger.info', () => {
      before(async () => {
        user = (await Test.Helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        try {
          expectedParams = _.cloneDeep(shared.validParamsWithOptional({ type: 'info' }).body);

          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          loggerStub = sinon.stub(logger, expectedParams.type);
          response = await axios.post(path, expectedParams, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => loggerStub.restore());

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should call the right logger function', () => {
        const wasCalled = loggerStub.getCalls().some(item => {
          return (
            item.args[0].data.component === expectedParams.component &&
            item.args[0].data.event === expectedParams.event &&
            item.args[0].data[Object.keys(expectedParams)[0]] === Object.values(expectedParams)[0]
          );
        });

        assert.isTrue(wasCalled);
      });
    });

    describe('invalid log type', () => {
      before(async () => {
        user = (await Test.Helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        try {
          expectedParams = _.cloneDeep(shared.validParamsWithOptional({ type: 'invalid log type' }).body);
          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          loggerStub = sinon.stub(logger, 'info');
          response = await axios.post(path, expectedParams, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => loggerStub.restore());

      it('should return a 400', () => {
        assert.strictEqual(response.status, 400);
      });
    });

    describe('rate limited', () => {
      before(async () => {
        user = (await Test.Helpers.user()).user;

        const acsToken = helper.getAccessTokenForUser(user);
        await model.usersTokens.upsert(user._id, `${config.get('userTokenPrefix')}:${acsToken.toJWT()}`);

        try {
          expectedParams = _.cloneDeep(shared.validParamsWithOptional({ type: 'info' }).body);

          const options = {
            headers: {
              [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(user._id, session.test.getEncryptionPassword()),
            },
          };

          loggerStub = sinon.stub(logger, expectedParams.type);
          await axios.post(path, expectedParams, options);
          response = await axios.post(path, expectedParams, options);
        } catch (err) {
          response = err.response;
        }
      });

      after(() => loggerStub.restore());

      it('should return a 429', () => {
        assert.strictEqual(response.status, 429);
      });

      it('should set the Retry-After header', () => {
        assert.strictEqual(response.headers['retry-after'], String(config.get('limits:logging:timeframeInMS') / 1000));
      });
    });
  });

  describe('token does not exist', () => {
    before(async () => {
      try {
        const data = {};
        const options = {
          headers: {
            [ACCESS_TOKEN_HEADER]: jsUtil.crypto.encrypt(
              'invalid account management token',
              session.test.getEncryptionPassword()
            ),
          },
        };

        response = await axios.post(path, data, options);
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });
  });
});
