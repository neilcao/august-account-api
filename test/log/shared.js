const faker = require('faker');

const logTypes = ['info', 'error', 'warn', 'debug'];

module.exports.validParams = (options = {}) => {
  return {
    body: {
      component: options.component || faker.lorem.word(),
      event: options.event || faker.lorem.word(),
      type: options.type || faker.random.arrayElement(logTypes),
    },
  };
};

module.exports.validParamsWithOptional = (options = {}) => {
  return {
    body: {
      component: options.component || faker.lorem.word(),
      event: options.event || faker.lorem.word(),
      type: options.type || faker.random.arrayElement(logTypes),
      error: options.error || faker.lorem.word(),
      data: options.data || {
        [faker.random.word()]: faker.random.word(),
      },
    },
  };
};
