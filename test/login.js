'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const ms = require('ms');
const sinon = require('sinon');

const AccessToken = require('august-access-token');
const jsUtil = require('august-js-utility');
const model = require('august-model');
const Test = require('august-test/v2');

const acs = require('../acs.js');
const config = require('../config/index.js');
const session = require('../middleware/session.js');

const path = '/login';
const phoneNumberFormat = '+###########';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${path}`, () => {
  let loginStub;
  let response;
  let user;
  let userToken;
  let verificationCodeStub;

  describe('valid email request', () => {
    const expectedExpirationMS = Date.now() + ms('59 minutes');
    const identifierType = 'email';
    const identifierValue = faker.internet.email();
    let tokenJWT;

    before(async () => {
      user = (await Test.Helpers.user()).user;
      loginStub = stubAcsLoginSuccess(user, expectedExpirationMS);
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();
    });

    after(() => {
      loginStub.restore();
      verificationCodeStub.restore();
    });

    describe('not rate limited', () => {
      before(async () => {
        try {
          response = await axios.post(path, {
            identifier: `${identifierType}:${identifierValue}`,
            password: faker.internet.password(),
          });
        } catch (err) {
          response = err.response;
        }
      });

      before(async () => {
        userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
        tokenJWT = userToken.token.split(':')[1];
      });

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should return an encrypted user ID', () => {
        assert.strictEqual(jsUtil.crypto.decrypt(response.data, session.test.getEncryptionPassword()), user._id);
      });

      it('should insert a document in the userstokens collection', () => {
        const token = new AccessToken();
        token.parseJWT(tokenJWT);
        const expiresAtMS = new Date(token._token.expiresAt).getTime();

        assert.exists(userToken);
        assert.approximately(expiresAtMS - Date.now() - config.get('tokenExpirationTimeInMS'), 0, 100);
        assert.approximately(expiresAtMS / 1000, token._token.exp, 1);
      });

      it('should send a verification code to the user', () => {
        sinon.assert.calledOnce(verificationCodeStub);
        sinon.assert.calledWith(verificationCodeStub, identifierType, identifierValue, tokenJWT);
      });
    });

    describe('rate limited', () => {
      before(async () => {
        try {
          response = await axios.post(path, {
            identifier: `${identifierType}:${identifierValue}`,
            password: faker.internet.password(),
          });
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 429', () => {
        assert.strictEqual(response.status, 429);
      });

      it('should set the Retry-After header', () => {
        assert.strictEqual(response.headers['retry-after'], String(config.get('limits:login:timeframeInMS') / 1000));
      });
    });
  });

  describe('valid phone request', () => {
    const expectedExpirationMS = Date.now() + ms('59 minutes');
    const identifierType = 'phone';
    const identifierValue = faker.phone.phoneNumber(phoneNumberFormat);
    let tokenJWT;

    before(async () => {
      user = (await Test.Helpers.user()).user;
      loginStub = stubAcsLoginSuccess(user, expectedExpirationMS);
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();
    });

    after(() => {
      loginStub.restore();
      verificationCodeStub.restore();
    });

    describe('not rate limited', () => {
      before(async () => {
        try {
          response = await axios.post(path, {
            identifier: `${identifierType}:${identifierValue}`,
            password: faker.internet.password(),
          });
        } catch (err) {
          response = err.response;
        }
      });

      before(async () => {
        userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix'));
        tokenJWT = userToken.token.split(':')[1];
      });

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should return an encrypted user ID', () => {
        assert.strictEqual(jsUtil.crypto.decrypt(response.data, session.test.getEncryptionPassword()), user._id);
      });

      it('should insert a document in the userstokens collection', () => {
        const token = new AccessToken();
        token.parseJWT(tokenJWT);
        const expiresAtMS = new Date(token._token.expiresAt).getTime();

        assert.exists(userToken);
        assert.approximately(expiresAtMS - Date.now() - config.get('tokenExpirationTimeInMS'), 0, 100);
        assert.approximately(expiresAtMS / 1000, token._token.exp, 1);
      });

      it('should send a verification code to the user', () => {
        sinon.assert.calledOnce(verificationCodeStub);
        sinon.assert.calledWith(verificationCodeStub, identifierType, identifierValue, tokenJWT);
      });
    });

    describe('rate limited', () => {
      before(async () => {
        try {
          response = await axios.post(path, {
            identifier: `${identifierType}:${identifierValue}`,
            password: faker.internet.password(),
          });
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 429', () => {
        assert.strictEqual(response.status, 429);
      });

      it('should set the Retry-After header', () => {
        assert.strictEqual(response.headers['retry-after'], String(config.get('limits:login:timeframeInMS') / 1000));
      });
    });
  });

  describe('expired access token from rest api', () => {
    const identifierType = 'email';
    const identifierValue = faker.internet.email();

    before(async () => {
      user = (await Test.Helpers.user()).user;
      loginStub = stubAcsLoginFailure(user, { type: 'expiredToken' });
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();

      try {
        response = await axios.post(path, {
          identifier: `${identifierType}:${identifierValue}`,
          password: faker.internet.password(),
        });
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    after(() => {
      loginStub.restore();
      verificationCodeStub.restore();
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });

    it('should not send a verification code to the user', () => {
      sinon.assert.notCalled(verificationCodeStub);
    });
  });

  describe('no userID in access token from rest api', () => {
    const identifierType = 'email';
    const identifierValue = faker.internet.email();

    before(async () => {
      user = (await Test.Helpers.user()).user;
      loginStub = stubAcsLoginFailure(user, { type: 'missingUserID' });
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();

      try {
        response = await axios.post(path, {
          identifier: `${identifierType}:${identifierValue}`,
          password: faker.internet.password(),
        });
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    after(() => {
      loginStub.restore();
      verificationCodeStub.restore();
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });

    it('should not send a verification code to the user', () => {
      sinon.assert.notCalled(verificationCodeStub);
    });
  });

  describe('not enough factors in access token from rest api (wrong password)', () => {
    const identifierType = 'email';
    const identifierValue = faker.internet.email();

    before(async () => {
      user = (await Test.Helpers.user()).user;
      loginStub = stubAcsLoginFailure(user, { type: 'notEnoughFactors' });
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();

      try {
        response = await axios.post(path, {
          identifier: `${identifierType}:${identifierValue}`,
          password: faker.internet.password(),
        });
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    after(() => {
      loginStub.restore();
      verificationCodeStub.restore();
    });

    it('should return a 401', () => {
      assert.strictEqual(response.status, 401);
    });

    it('should insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });

    it('should not send a verification code to the user', () => {
      sinon.assert.notCalled(verificationCodeStub);
    });
  });

  describe('missing identifier', () => {
    before(async () => {
      user = (await Test.Helpers.user()).user;
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();

      try {
        response = await axios.post(path, {
          password: faker.internet.password(),
        });
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    after(() => verificationCodeStub.restore());

    it('should return a 409', () => {
      assert.strictEqual(response.status, 409);
    });

    it('should not insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });

    it('should not send a verification code to the user', () => {
      sinon.assert.notCalled(verificationCodeStub);
    });
  });

  describe('missing password', () => {
    before(async () => {
      user = (await Test.Helpers.user()).user;
      verificationCodeStub = stubAcsSendVerificationCodeSuccess();

      try {
        response = await axios.post(path, {
          identifier: `email:${faker.internet.email()}`,
        });
      } catch (err) {
        response = err.response;
      }
    });

    before(
      async () =>
        (userToken = await model.usersTokens.findByUserIDAndValueType(user._id, config.get('userTokenPrefix')))
    );

    after(() => verificationCodeStub.restore());

    it('should return a 409', () => {
      assert.strictEqual(response.status, 409);
    });

    it('should not insert a document in the userstokens collection', () => {
      assert.notExists(userToken);
    });

    it('should not send a verification code to the user', () => {
      sinon.assert.notCalled(verificationCodeStub);
    });
  });
});

/**
 * Pretend the call to acs to login was successful.
 * @param {Object} user -
 * @param {number} expectedExpirationMS - epoch time
 * @returns {Object} The response
 */
function getAcsLoginSuccess(user, expectedExpirationMS) {
  return {
    installId: '',
    applicationId: '',
    userId: user._id,
    vInstallId: false,
    vPassword: true,
    vEmail: false,
    vPhone: false,
    hasInstallId: false,
    hasPassword: true,
    hasEmail: true,
    hasPhone: true,
    isLockedOut: false,
    captcha: '',
    email: [],
    phone: [],
    expiresAt: new Date(expectedExpirationMS).toISOString(),
    temporaryAccountCreationPasswordLink: '',
    iat: 1541021447,
    exp: null,
    LastName: user.LastName,
    FirstName: user.FirstName,
  };
}

/**
 * Pretend the call to acs to send a verification code was successful.
 *
 * @returns {Object} The mock success object.
 */
function stubAcsSendVerificationCodeSuccess() {
  return sinon.stub(acs, 'sendVerificationCode').callsFake(() => {
    return {
      status: 200,
    };
  });
}

/**
 * Create test stubs.
 *
 * @param {Object} user -
 * @param {number} expectedExpirationMS - epoch time
 * @returns {Array} The test stubs
 */
function stubAcsLoginSuccess(user, expectedExpirationMS) {
  const acsTokenObj = getAcsLoginSuccess(user, expectedExpirationMS);
  const acsToken = new AccessToken();
  acsToken._token = acsTokenObj;

  return sinon.stub(acs, 'login').callsFake(() => {
    return {
      status: 200,
      headers: {
        [config.get('acsAccessTokenHeader')]: acsToken.toJWT(),
      },
      data: acsToken._token,
    };
  });
}

/**
 *
 * @param {Object} user -
 * @param {string} type -
 * @returns {Object} The stub
 */
function stubAcsLoginFailure(user, type) {
  let data;
  let expiresAt = faker.date.future();

  switch (type) {
    case 'expiredToken':
      expiresAt = faker.date.past();
      data = {
        installId: '',
        applicationId: '',
        userId: user._id,
        vInstallId: false,
        vPassword: true,
        vEmail: true,
        vPhone: false,
        hasInstallId: true,
        hasPassword: true,
        hasEmail: true,
        hasPhone: true,
        isLockedOut: false,
        captcha: '',
        email: [],
        phone: [],
        expiresAt: expiresAt.toISOString(),
        temporaryAccountCreationPasswordLink: '',
        iat: Math.floor((expiresAt.getTime() - ms('30 days')) / 1000),
        exp: Math.floor(expiresAt.getTime() / 1000),
        LastName: user.LastName,
        FirstName: user.FirstName,
      };
      break;

    case 'missingUserID':
      data = {
        installId: '',
        applicationId: '',
        userId: '',
        vInstallId: false,
        vPassword: true,
        vEmail: true,
        vPhone: false,
        hasInstallId: true,
        hasPassword: true,
        hasEmail: true,
        hasPhone: true,
        isLockedOut: false,
        captcha: '',
        email: [],
        phone: [],
        expiresAt: expiresAt.toISOString(),
        temporaryAccountCreationPasswordLink: '',
        iat: Math.floor((expiresAt.getTime() - ms('30 days')) / 1000),
        exp: Math.floor(expiresAt.getTime() / 1000),
        FirstName: '',
        LastName: '',
      };
      break;

    case 'notEnoughFactors':
      data = {
        installId: '',
        applicationId: '',
        userId: '',
        vInstallId: false,
        vPassword: false,
        vEmail: false,
        vPhone: false,
        hasInstallId: true,
        hasPassword: true,
        hasEmail: true,
        hasPhone: true,
        isLockedOut: false,
        oauth: {},
        homeAccess: '',
        captcha: '',
        email: [
          // TODO: no hardcode
          'email:boolean@gmail.com',
        ],
        phone: [],
        expiresAt: expiresAt.toISOString(),
        temporaryAccountCreationPasswordLink: '',
        iat: Math.floor((expiresAt.getTime() - ms('30 days')) / 1000),
        exp: Math.floor(expiresAt.getTime() / 1000),
        FirstName: '',
        LastName: '',
      };
      break;
  }

  return sinon.stub(acs, 'login').callsFake(() => data);
}
