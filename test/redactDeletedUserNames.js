'use strict';

const _ = require('lodash');
const { assert } = require('chai');
const { Promise } = require('bluebird');
const sinon = require('sinon');

const logger = require('august-logger');
const model = require('august-model');

const redactNameFromDeletedUsers = require('../redactNameFromDeletedUsers');

describe('redactNameFromDeletedUsers', () => {
  describe('success', () => {
    let loggingSpy;
    let modelStub;
    const modelStubResponse = { updatedCount: 2, userIDs: ['a', 'b'] };
    let sinonTimer;
    const intervalMs = 10000;
    let timeoutObj;

    before('set spy, get configs, and pass time', () => {
      loggingSpy = sinon.spy(logger, 'info');
      modelStub = sinon.stub(model.users, 'redactDeletedUserNames').returns(Promise.resolve(modelStubResponse));
      sinonTimer = sinon.useFakeTimers({
        now: Date.now(),
        shouldAdvanceTime: true,
      });
      timeoutObj = redactNameFromDeletedUsers(intervalMs, 'foobar', {
        logSkipping: true,
        logSuccess: true,
      });

      return sinonTimer.tickAsync(intervalMs);
    });

    after(() => {
      loggingSpy.restore();
      modelStub.restore();
      sinonTimer.restore();
      clearInterval(timeoutObj);
    });

    it('should return an object', () => {
      assert.isObject(timeoutObj);
    });

    it('should call the success loggingSpy', async () => {
      assert.isOk(loggingSpy.calledTwice, 'logging Spy was not called twice');
      const secondCallArgs = _.get(loggingSpy, 'secondCall.args[0]');
      assert.strictEqual(secondCallArgs.event, 'redactDeletedUserNames');
      assert.strictEqual(secondCallArgs.data.updatedCount, modelStubResponse.updatedCount);
    });

    it('should have called the modelSpy', () => {
      assert.isOk(modelStub.calledOnce);
    });
  });
});
