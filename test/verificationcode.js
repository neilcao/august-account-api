'use strict';

const assert = require('chai').assert;
const axios = require('axios');
const faker = require('faker');
const sinon = require('sinon');

const acs = require('../acs.js');
const config = require('../config/index.js');

const path = '/sendcode';

axios.defaults.baseURL = `http://localhost:${config.get('port')}`;

describe(`POST ${path}`, () => {
  let response;
  let verificationCodeStub;

  describe('valid request', () => {
    const identifierType = 'email';
    const identifierValue = faker.internet.email();

    before(async () => {
      verificationCodeStub = sinon.stub(acs, 'sendVerificationCode').callsFake(() => {
        return {
          status: 200,
        };
      });
    });

    after(() => {
      verificationCodeStub.restore();
    });

    describe('not rate limited', () => {
      before(async () => {
        try {
          const data = {
            identifier: `${identifierType}:${identifierValue}`,
          };

          response = await axios.post(path, data);
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 200', () => {
        assert.strictEqual(response.status, 200);
      });

      it('should send a verification code to the user', () => {
        sinon.assert.calledOnce(verificationCodeStub);
        sinon.assert.calledWith(verificationCodeStub, identifierType, identifierValue);
      });
    });

    describe('rate limited', () => {
      before(async () => {
        const data = {
          identifier: `${identifierType}:${identifierValue}`,
        };

        try {
          response = await axios.post(path, data);
        } catch (err) {
          response = err.response;
        }
      });

      it('should return a 429', () => {
        assert.strictEqual(response.status, 429);
      });

      it('should set the Retry-After header', () => {
        assert.strictEqual(response.headers['retry-after'], String(config.get('limits:codes:timeframeInMS') / 1000));
      });
    });
  });

  describe('missing identifier', () => {
    before(async () => {
      try {
        response = await axios.post(path, {});
      } catch (err) {
        response = err.response;
      }
    });

    it('should return a 409', () => {
      assert.strictEqual(response.status, 409);
    });
  });
});
