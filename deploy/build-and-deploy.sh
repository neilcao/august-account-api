#!/bin/bash
set -ex

# Workspace is a jenkins provided env variable for the root job directory
cd ${WORKSPACE}
LD_LIBRARY_PATH=""

BITBUCKET_REPO="august-account-api"

DEV_REGION="us-east-1"
IRELAND_REGION="eu-west-1"

DEV_REG_ADDRESS="084307953142.dkr.ecr.us-east-1.amazonaws.com"
IRELAND_REG_ADDRESS="084307953142.dkr.ecr.eu-west-1.amazonaws.com"

DOCKERFILE_NAME="Dockerfile-account-api"
DEV_SERVICE_NAME="account-api-dev"
IRELAND_STAGE_SERVICE_NAME="account-api-ireland-stage"
IRELAND_PROD_SERVICE_NAME="account-api-ireland-prod"

DOCKERFILE_NGINX="Dockerfile-nginx"
DEV_NGINX="account-api-dev-nginx"
IRELAND_STAGE_NGINX="account-api-ireland-stage-nginx"
IRELAND_PROD_NGINX="account-api-ireland-prod-nginx"

IMAGE_VERSION=${GIT_COMMIT}
IMAGE_TAG=${IMAGE_VERSION}_b${BUILD_NUMBER}
SERVER_TAG=$(git -C ${BITBUCKET_REPO} describe --abbrev=0 --tags)

NAMESPACE="default"

case "$ENV" in
dev-aws)
  DASHBOARD_HOSTNAME="dev-k8s-dashboard.august.com"

  # Adding bitbucket ssh key
  cat "${AUGUST_BITBUCKET_KEY}" >id_rsa_august_bitbucket

  # Adding august-runtime-creds so we can test the container
  mkdir august-runtime-creds
  cat "${VAULT_CRT}" >august-runtime-creds/vault.crt
  cat "${AUGUST_COM_CHAINED_CRT}" >august-runtime-creds/august.com.chained.crt
  cat "${AUGUST_COM_KEY}" >august-runtime-creds/august.com.key

  cp -pr ./${BITBUCKET_REPO}/deploy/. .

  # Build for dev, stage, and prod
  docker build --no-cache -f ${DOCKERFILE_NAME} \
    -t ${DEV_SERVICE_NAME}:${IMAGE_TAG} \
    -t ${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG} \
    -t ${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG} \
    .
  docker build --no-cache -f ${DOCKERFILE_NGINX} \
    -t ${DEV_NGINX}:${IMAGE_TAG} \
    -t ${IRELAND_STAGE_NGINX}:${SERVER_TAG} \
    -t ${IRELAND_PROD_NGINX}:${SERVER_TAG} \
    .

  docker rm -f ${DEV_SERVICE_NAME} || true
  docker rm -f ${DEV_NGINX} || true

  #/root/august-runtime-creds managed separately on the Jenkins box in each environment
  docker run -d --name ${DEV_SERVICE_NAME} \
    -e NODE_ENV=${ENV} \
    -e VAULT_TOKEN=${VAULT_TOKEN} \
    --mount "type=bind,src=${WORKSPACE}/august-runtime-creds,target=/root/august-runtime-creds" \
    --network test \
    -p 12481:10000 \
    -i ${DEV_SERVICE_NAME}:${IMAGE_TAG}

  #/root/august-runtime-creds managed separately on the Jenkins box in each environment
  docker run -d --name ${DEV_NGINX} \
    --mount "type=bind,src=${WORKSPACE}/august-runtime-creds,target=/root/august-runtime-creds" \
    --network test \
    -p 12482:999 \
    -i ${DEV_NGINX}:${IMAGE_TAG}

  ATTEMPTS=1
  while ! curl -k --fail "http://localhost:12481/health"; do
    sleep 1
    let "ATTEMPTS+=1"
    if [ ${ATTEMPTS} -gt 120 ]; then
      echo "${DEV_SERVICE_NAME} FAILED TO RESPOND WITHIN 2 MINUTES"
      echo "${DEV_SERVICE_NAME} LOGS ################################################"
      docker logs ${DEV_SERVICE_NAME}
      exit 1
    fi
  done

  ATTEMPTS=1
  while ! curl -k --fail "http://localhost:12482/ping"; do
    sleep 1
    let "ATTEMPTS+=1"
    if [ ${ATTEMPTS} -gt 120 ]; then
      echo "${DEV_SERVICE_NAME} FAILED TO RESPOND WITHIN 2 MINUTES"
      echo "${DEV_NGINX} LOGS ################################################"
      docker logs ${DEV_NGINX}
      exit 1
    fi
  done

  docker rm -f ${DEV_SERVICE_NAME} || true
  docker rm -f ${DEV_NGINX} || true

  # Removing bitbucket ssh key
  rm -f id_rsa_august_bitbucket

  # Removing august-runtime-creds
  rm -rf august-runtime-creds

  echo "Finished cleaning up prerequisites."

  # Login to ECR Repository
  aws ecr get-login-password --region ${DEV_REGION} | docker login --username AWS --password-stdin ${DEV_REG_ADDRESS}

  #service
  docker tag ${DEV_SERVICE_NAME}:${IMAGE_TAG} ${DEV_REG_ADDRESS}/${DEV_SERVICE_NAME}:${IMAGE_TAG}
  docker tag ${DEV_SERVICE_NAME}:${IMAGE_TAG} ${DEV_REG_ADDRESS}/${DEV_SERVICE_NAME}:latest
  aws ecr batch-delete-image --region ${DEV_REGION} --repository-name "${DEV_SERVICE_NAME}" --image-ids imageTag="latest"
  docker push ${DEV_REG_ADDRESS}/${DEV_SERVICE_NAME}:${IMAGE_TAG}
  docker push ${DEV_REG_ADDRESS}/${DEV_SERVICE_NAME}:latest

  #nginx
  docker tag ${DEV_NGINX}:${IMAGE_TAG} ${DEV_REG_ADDRESS}/${DEV_NGINX}:${IMAGE_TAG}
  docker tag ${DEV_NGINX}:${IMAGE_TAG} ${DEV_REG_ADDRESS}/${DEV_NGINX}:latest
  aws ecr batch-delete-image --region ${DEV_REGION} --repository-name "${DEV_NGINX}" --image-ids imageTag="latest"
  docker push ${DEV_REG_ADDRESS}/${DEV_NGINX}:${IMAGE_TAG}
  docker push ${DEV_REG_ADDRESS}/${DEV_NGINX}:latest

  kubectl set image deployment/${DEV_SERVICE_NAME} \
    "${DEV_SERVICE_NAME}=${DEV_REG_ADDRESS}/${DEV_SERVICE_NAME}:${IMAGE_TAG}" \
    "${DEV_NGINX}=${DEV_REG_ADDRESS}/${DEV_NGINX}:${IMAGE_TAG}" \
    --record \
    --token=${K8S_TOKEN}

  if kubectl rollout status deployment/${DEV_SERVICE_NAME} --namespace=default --token=${K8S_TOKEN}; then
    curl -X POST -H 'Content-type: application/json' --data "{\"attachments\": [
        {
            \"fallback\": \"Deployment Successful: ${DEV_SERVICE_NAME}\",
            \"color\": \"good\",
            \"author_name\": \"K8s Deployment Rollout\",
            \"title\": \"Successful deployment for ${DEV_SERVICE_NAME}\",
            \"title_link\": \"https://${DASHBOARD_HOSTNAME}/#!/deployment/${NAMESPACE}/${DEV_SERVICE_NAME}\",
            \"text\": \"Image tag: ${IMAGE_TAG}\",
            \"footer\": \"K8\",
            \"footer_icon\": \"https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.png\",
            \"ts\": $(date +%s)
        }
      ]}" ${SLACK_URL}

    echo "Deployment successful"

  else
    echo "Deployment failed"

    kubectl rollout undo deployment/${DEV_SERVICE_NAME} --token=${K8S_TOKEN}

    curl -X POST -H 'Content-type: application/json' --data "{\"attachments\": [
        {
            \"fallback\": \"Deployment Failed: ${DEV_SERVICE_NAME}. Rollback Successful\",
            \"color\": \"danger\",
            \"author_name\": \"K8s Deployment Rollout\",
            \"title\": \"Deployment for ${DEV_SERVICE_NAME} FAILED \",
            \"title_link\": \"https://${DASHBOARD_HOSTNAME}/#!/deployment/${NAMESPACE}/${DEV_SERVICE_NAME}\",
            \"text\": \"Image tag: ${IMAGE_TAG}\",
            \"footer\": \"K8\",
            \"footer_icon\": \"https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.png\",
            \"ts\": $(date +%s)
        }
      ]}" ${SLACK_URL}

    exit 1
  fi
  # Check to see if git commit hash has tag
  cd "${WORKSPACE}/${BITBUCKET_REPO}"
  COMMIT_HAS_TAG=$(git describe --exact-match "${IMAGE_VERSION}") || true
  if [ -z "${COMMIT_HAS_TAG}" ]; then
    echo "No tag found"
    exit 0
  else
    echo "Found tag: ${COMMIT_HAS_TAG}"
  fi
  cd "${WORKSPACE}"

  LOGIN_STRING=$(aws ecr get-login-password --region ${IRELAND_REGION} | docker login --username AWS --password-stdin ${IRELAND_REG_ADDRESS})

  echo "Setting up Ireland STAGE environment"
  docker tag ${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG}
  docker tag ${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_SERVICE_NAME}:latest
  aws ecr batch-delete-image --region ${IRELAND_REGION} --repository-name "${IRELAND_STAGE_SERVICE_NAME}" --image-ids imageTag="latest"
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG}
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_SERVICE_NAME}:latest

  docker tag ${IRELAND_STAGE_NGINX}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_NGINX}:${SERVER_TAG}
  docker tag ${IRELAND_STAGE_NGINX}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_NGINX}:latest
  aws ecr batch-delete-image --region ${IRELAND_REGION} --repository-name "${IRELAND_STAGE_NGINX}" --image-ids imageTag="latest"
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_NGINX}:${SERVER_TAG}
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_NGINX}:latest

  echo "Setting up Ireland prod environment"
  docker tag ${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG}
  docker tag ${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_SERVICE_NAME}:latest
  aws ecr batch-delete-image --region ${IRELAND_REGION} --repository-name "${IRELAND_PROD_SERVICE_NAME}" --image-ids imageTag="latest"
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG}
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_SERVICE_NAME}:latest

  docker tag ${IRELAND_PROD_NGINX}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_NGINX}:${SERVER_TAG}
  docker tag ${IRELAND_PROD_NGINX}:${SERVER_TAG} ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_NGINX}:latest
  aws ecr batch-delete-image --region ${IRELAND_REGION} --repository-name "${IRELAND_PROD_NGINX}" --image-ids imageTag="latest"
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_NGINX}:${SERVER_TAG}
  docker push ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_NGINX}:latest

  exit 0
  ;;
ireland-stage-aws)
  DASHBOARD_HOSTNAME="ireland-stage-k8s-dashboard.august.com"

  # Adding bitbucket ssh key
  cat "${AUGUST_BITBUCKET_KEY}" >id_rsa_august_bitbucket

  # Adding august-runtime-creds so we can test the container
  mkdir august-runtime-creds
  cat "${VAULT_CRT}" >august-runtime-creds/vault.crt
  cat "${AUGUST_COM_CHAINED_CRT}" >august-runtime-creds/august.com.chained.crt
  cat "${AUGUST_COM_KEY}" >august-runtime-creds/august.com.key

  cp -pr ./${BITBUCKET_REPO}/deploy/. .

  docker rm -f ${IRELAND_STAGE_NGINX} || true

  LOGIN_STRING=$(aws ecr get-login-password --region ${IRELAND_REGION} | docker login --username AWS --password-stdin ${IRELAND_REG_ADDRESS})

  #/root/august-runtime-creds managed separately on the Jenkins box in each environment
  docker run -d --name ${IRELAND_STAGE_SERVICE_NAME} \
    -e NODE_ENV=${ENV} \
    -e VAULT_TOKEN=${VAULT_TOKEN} \
    --mount "type=bind,src=${WORKSPACE}/august-runtime-creds,target=/root/august-runtime-creds" \
    --network test \
    -p 12481:10000 \
    -i ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG}

  #/root/august-runtime-creds managed separately on the Jenkins box in each environment
  docker run -d --name ${IRELAND_STAGE_NGINX} \
    --mount "type=bind,src=${WORKSPACE}/august-runtime-creds,target=/root/august-runtime-creds" \
    --network test \
    -p 12482:999 \
    -i ${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_NGINX}:${SERVER_TAG}

  ATTEMPTS=1
  while ! curl -k --fail "http://localhost:12481/health"; do
    sleep 1
    let "ATTEMPTS+=1"
    if [ ${ATTEMPTS} -gt 120 ]; then
      echo "${IRELAND_STAGE_SERVICE_NAME} FAILED TO RESPOND WITHIN 2 MINUTES"
      echo "${IRELAND_STAGE_SERVICE_NAME} LOGS ################################################"
      docker logs ${IRELAND_STAGE_SERVICE_NAME}
      exit 1
    fi
  done

  ATTEMPTS=1
  while ! curl -k --fail "http://localhost:12482/ping"; do
    sleep 1
    let "ATTEMPTS+=1"
    if [ ${ATTEMPTS} -gt 120 ]; then
      echo "${IRELAND_STAGE_SERVICE_NAME} FAILED TO RESPOND WITHIN 2 MINUTES"
      echo "${IRELAND_STAGE_NGINX} LOGS ################################################"
      docker logs ${IRELAND_STAGE_NGINX}
      exit 1
    fi
  done

  docker rm -f ${IRELAND_STAGE_SERVICE_NAME} || true
  docker rm -f ${IRELAND_STAGE_NGINX} || true

  # Removing bitbucket ssh key
  rm -f id_rsa_august_bitbucket

  # Removing august-runtime-creds
  rm -rf august-runtime-creds

  echo "Finished cleaning up prerequisites."

  kubectl set image deployment/${IRELAND_STAGE_SERVICE_NAME} \
    "${IRELAND_STAGE_SERVICE_NAME}=${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_SERVICE_NAME}:${SERVER_TAG}" \
    "${IRELAND_STAGE_NGINX}=${IRELAND_REG_ADDRESS}/${IRELAND_STAGE_NGINX}:${SERVER_TAG}" \
    --record \
    --kubeconfig ~/.kube/ireland-stage-config \
    --token=${K8S_TOKEN}

  if kubectl rollout status deployment/${IRELAND_STAGE_SERVICE_NAME} --namespace=default --token=${K8S_TOKEN} --kubeconfig ~/.kube/ireland-stage-config; then
    curl -X POST -H 'Content-type: application/json' --data "{\"attachments\": [
        {
            \"fallback\": \"Deployment Successful: ${IRELAND_STAGE_SERVICE_NAME}\",
            \"color\": \"good\",
            \"author_name\": \"K8s Deployment Rollout\",
            \"title\": \"Successful deployment for ${IRELAND_STAGE_SERVICE_NAME}\",
            \"title_link\": \"https://${DASHBOARD_HOSTNAME}/#!/deployment/${NAMESPACE}/${IRELAND_STAGE_SERVICE_NAME}\",
            \"text\": \"Image tag: ${SERVER_TAG}\",
            \"footer\": \"K8\",
            \"footer_icon\": \"https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.png\",
            \"ts\": $(date +%s)
        }
      ]}" ${SLACK_URL}

    echo "Deployment successful"

    exit 0
  else
    echo "Deployment failed"

    kubectl rollout undo deployment/${IRELAND_STAGE_SERVICE_NAME} --token=${K8S_TOKEN} --kubeconfig ~/.kube/ireland-stage-config

    curl -X POST -H 'Content-type: application/json' --data "{\"attachments\": [
        {
            \"fallback\": \"Deployment Failed: ${IRELAND_STAGE_SERVICE_NAME}. Rollback Successful\",
            \"color\": \"danger\",
            \"author_name\": \"K8s Deployment Rollout\",
            \"title\": \"Deployment for ${IRELAND_STAGE_SERVICE_NAME} FAILED\",
            \"title_link\": \"https://${DASHBOARD_HOSTNAME}/#!/deployment/${NAMESPACE}/${IRELAND_STAGE_SERVICE_NAME}\",
            \"text\": \"Image tag: ${SERVER_TAG}\",
            \"footer\": \"K8\",
            \"footer_icon\": \"https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.png\",
            \"ts\": $(date +%s)
        }
      ]}" ${SLACK_URL}

    exit 1
  fi
  ;;
ireland-prod-aws)
  DASHBOARD_HOSTNAME="ireland-prod-k8s-dashboard.august.com"

  # Adding bitbucket ssh key
  cat "${AUGUST_BITBUCKET_KEY}" >id_rsa_august_bitbucket

  # Adding august-runtime-creds so we can test the container
  mkdir august-runtime-creds
  cat "${VAULT_CRT}" >august-runtime-creds/vault.crt
  cat "${AUGUST_COM_CHAINED_CRT}" >august-runtime-creds/august.com.chained.crt
  cat "${AUGUST_COM_KEY}" >august-runtime-creds/august.com.key

  cp -pr ./${BITBUCKET_REPO}/deploy/. .

  docker rm -f ${IRELAND_PROD_NGINX} || true

  LOGIN_STRING=$(aws ecr get-login-password --region ${IRELAND_REGION} | docker login --username AWS --password-stdin ${IRELAND_REG_ADDRESS})

  #/root/august-runtime-creds managed separately on the Jenkins box in each environment
  docker run -d --name ${IRELAND_PROD_SERVICE_NAME} \
    -e NODE_ENV=${ENV} \
    -e VAULT_TOKEN=${VAULT_TOKEN} \
    --mount "type=bind,src=${WORKSPACE}/august-runtime-creds,target=/root/august-runtime-creds" \
    --network test \
    -p 12481:10000 \
    -i ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG}

  #/root/august-runtime-creds managed separately on the Jenkins box in each environment
  docker run -d --name ${IRELAND_PROD_NGINX} \
    --mount "type=bind,src=${WORKSPACE}/august-runtime-creds,target=/root/august-runtime-creds" \
    --network test \
    -p 12482:999 \
    -i ${IRELAND_REG_ADDRESS}/${IRELAND_PROD_NGINX}:${SERVER_TAG}

  ATTEMPTS=1
  while ! curl -k --fail "http://localhost:12481/health"; do
    sleep 1
    let "ATTEMPTS+=1"
    if [ ${ATTEMPTS} -gt 120 ]; then
      echo "${IRELAND_PROD_SERVICE_NAME} FAILED TO RESPOND WITHIN 2 MINUTES"
      echo "${IRELAND_PROD_SERVICE_NAME} LOGS ################################################"
      docker logs ${IRELAND_PROD_SERVICE_NAME}
      exit 1
    fi
  done

  ATTEMPTS=1
  while ! curl -k --fail "http://localhost:12482/ping"; do
    sleep 1
    let "ATTEMPTS+=1"
    if [ ${ATTEMPTS} -gt 120 ]; then
      echo "${IRELAND_PROD_SERVICE_NAME} FAILED TO RESPOND WITHIN 2 MINUTES"
      echo "${IRELAND_PROD_NGINX} LOGS ################################################"
      docker logs ${IRELAND_PROD_NGINX}
      exit 1
    fi
  done

  docker rm -f ${IRELAND_PROD_SERVICE_NAME} || true
  docker rm -f ${IRELAND_PROD_NGINX} || true

  # Removing bitbucket ssh key
  rm -f id_rsa_august_bitbucket

  # Removing august-runtime-creds
  rm -rf august-runtime-creds

  echo "Finished cleaning up prerequisites."

  kubectl set image deployment/${IRELAND_PROD_SERVICE_NAME} \
    "${IRELAND_PROD_SERVICE_NAME}=${IRELAND_REG_ADDRESS}/${IRELAND_PROD_SERVICE_NAME}:${SERVER_TAG}" \
    "${IRELAND_PROD_NGINX}=${IRELAND_REG_ADDRESS}/${IRELAND_PROD_NGINX}:${SERVER_TAG}" \
    --record \
    --kubeconfig ~/.kube/ireland-prod-config \
    --token=${K8S_TOKEN}

  if kubectl rollout status deployment/${IRELAND_PROD_SERVICE_NAME} --namespace=default --token=${K8S_TOKEN} --kubeconfig ~/.kube/ireland-prod-config; then
    curl -X POST -H 'Content-type: application/json' --data "{\"attachments\": [
        {
            \"fallback\": \"Deployment Successful: ${IRELAND_PROD_SERVICE_NAME}\",
            \"color\": \"good\",
            \"author_name\": \"K8s Deployment Rollout\",
            \"title\": \"Successful deployment for ${IRELAND_PROD_SERVICE_NAME}\",
            \"title_link\": \"https://${DASHBOARD_HOSTNAME}/#!/deployment/${NAMESPACE}/${IRELAND_PROD_SERVICE_NAME}\",
            \"text\": \"Image tag: ${SERVER_TAG}\",
            \"footer\": \"K8\",
            \"footer_icon\": \"https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.png\",
            \"ts\": $(date +%s)
        }
      ]}" ${SLACK_URL}

    echo "Deployment successful"

    exit 0
  else
    echo "Deployment failed"

    kubectl rollout undo deployment/${IRELAND_PROD_SERVICE_NAME} --token=${K8S_TOKEN} --kubeconfig ~/.kube/ireland-prod-config

    curl -X POST -H 'Content-type: application/json' --data "{\"attachments\": [
        {
            \"fallback\": \"Deployment Failed: ${IRELAND_PROD_SERVICE_NAME}. Rollback Successful\",
            \"color\": \"danger\",
            \"author_name\": \"K8s Deployment Rollout\",
            \"title\": \"Deployment for ${IRELAND_PROD_SERVICE_NAME} FAILED\",
            \"title_link\": \"https://${DASHBOARD_HOSTNAME}/#!/deployment/${NAMESPACE}/${IRELAND_PROD_SERVICE_NAME}\",
            \"text\": \"Image tag: ${SERVER_TAG}\",
            \"footer\": \"K8\",
            \"footer_icon\": \"https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.png\",
            \"ts\": $(date +%s)
        }
      ]}" ${SLACK_URL}

    exit 1
  fi
  ;;
*)
  echo "Invalid ENV variable provided"
  exit 1
  ;;
esac
