local limit_req = require "resty.limit.req"
local jwt = require "resty.jwt"

local RateLimiter = {}
local rate_limit_store_name = "lua_rate_limit_store"
local limits = {}

--- Rate limit a request
--- @param endpoint String: The endpoint to rate limit
--- @param num Number: The number of requests to allow per second
--- @param seconds Number: The time period over which to allow num requests
--- @param burst Number: Allow for a burst of requests exceeding the rate limit. Excess requests will be buffered
---                      and will be serviced according to the rate limit schedule. Requests that exceed the burst limit
---                      will be rejected.
--- @example: rate_limit("/endpoint1", 5.0, 1.0, 0) => 5 requests per second, no burst
--- @example: rate_limit("/endpoint2", 1.0, 1.0, 5) => 1 request per second, burst of 5 requests. The first request
---                        will return immediately. The next 4 requests within 1 second will be delayed to fit the
---                        schedule. The 5th request within 1 second will be rejected (429)
function RateLimiter.rate_limit(endpoint, num, seconds, burst)
  -- check args
  if not (num > 0.0 and seconds > 0.0 and burst >= 0.0) then
    ngx.log(ngx.ERR, "invalid rate limit: ", num, seconds, burst)
    ngx.exit(500)
  end

  -- create rate limiter
  local lim, err = limit_req.new(rate_limit_store_name, num / seconds, burst)
  if not lim then
      ngx.log(ngx.ERR, "failed to instantiate a resty.limit.req object: ", err)
      return ngx.exit(500)
  end

  -- extract jwt token
  local jwt_token = ngx.req.get_headers()["x-august-access-token"]
  local jwt_obj = jwt:verify("lua-resty-jwt", jwt_token)
  local requesting_ip = ngx.var.remote_addr

  -- get key to rate limit by in the format `endpoint:userID`
  local limit_key = endpoint .. ":"
  if jwt_obj.payload ~= nil and jwt_obj.payload.userId ~= nil then
    limit_key = limit_key .. jwt_obj.payload.userId
  else
    limit_key = limit_key .. requesting_ip
  end

  -- get rate limit
  local delay, err = lim:incoming(limit_key, true)

  -- apply rate limit
  if not delay then
    if err == "rejected" then
      ngx.log(ngx.ERR, limit_key, " has been rate limited")
      return ngx.exit(429)
    end
    ngx.log(ngx.ERR, "failed to limit req: ", err)
    return ngx.exit(500)
  end

  -- apply burst limit
  if delay >= 0.001 then
    ngx.log(ngx.ERR, limit_key, " has been burst delayed")
    return ngx.sleep(delay)
  end
end

return RateLimiter
