'use strict';

const requestUtility = require('august-request-utility');
const keys = require('august-keys').Keys();
const logger = require('august-logger');
const statsdClient = require('august-statsd').statsdClient;

const config = require('./config/index.js');
const repoPackage = require('./package.json');
const util = require('./util.js');

const acsPort = config.get('acs:port');
const acsUrl = config.get('acs:host') + (acsPort ? `:${acsPort}` : '');

let _acsClient;

const getAcsClient = () => {
  if (!_acsClient) {
    _acsClient = requestUtility.acs.of(acsUrl, {
      serviceMeta: { name: repoPackage.name, version: repoPackage.version },
      headers: {
        'x-august-api-key': keys.getServiceKeys('august-account-api').acsApiKey,
      },
    });
  }

  return _acsClient;
};

/**
 * Login
 *
 * @async
 * @param {string} identifier - email:blueberry@spicehead.com or phone:+16505551212
 * @param {string} password -
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.login = async (identifier, password) => {
  try {
    const data = {
      identifier,
      password,
    };
    const response = await getAcsClient().post('/session', data);

    statsdClient.increment('acs.login.success');
    logger.info({
      file: __filename,
      event: 'acsLogin',
      data: {
        identifier,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.login.failed');
    logger.error({
      file: __filename,
      event: 'acsLoginError',
      error: err,
      data: {
        identifier,
      },
    });

    throw err;
  }
};

/**
 * Send verification code
 *
 * @async
 * @param {string} identifierType - `email` or `phone`
 * @param {string} identifierValue - The email or phone
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.sendVerificationCode = async (identifierType, identifierValue, accessToken, brand) => {
  try {
    const data = {
      value: identifierValue,
    };
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().post(`/validation/${identifierType}`, data, options);

    statsdClient.increment('acs.send-verification-code.success');
    logger.info({
      file: __filename,
      event: 'acsSendVerificationCode',
      data: {
        identifierType,
        identifierValue,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.send-verification-code.failed');
    logger.error({
      file: __filename,
      event: 'acsSendVerificationCodeError',
      error: err,
      data: {
        identifierType,
        identifierValue,
      },
    });

    throw err;
  }
};

/**
 * Validate a verification code
 *
 * @async
 * @param {string} identifierType - `email` or `phone`
 * @param {string} identifierValue - The email or phone
 * @param {string} code - Verification code from the client
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @param {boolean} useVerificationLink - enable/disable verification link in email
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.validateVerificationCode = async (
  identifierType,
  identifierValue,
  code,
  accessToken,
  brand,
  useVerificationLink
) => {
  try {
    const data = {
      [identifierType]: identifierValue,
      code,
      useVerificationLink,
    };
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().post(`/validate/${identifierType}`, data, options);

    statsdClient.increment('acs.validate-verification-code.success');
    logger.info({
      file: __filename,
      event: 'acsValidateVerificationCode',
      data: {
        identifierType,
        identifierValue,
        code, // used or invalid at this point, so we log it
        useVerificationLink,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.validate-verification-code.failed');
    logger.error({
      file: __filename,
      event: 'acsValidateVerificationCodeError',
      error: err,
      data: {
        identifierType,
        identifierValue,
        code, // used or invalid at this point, so we log it
      },
    });

    throw err;
  }
};

/**
 * Get a user's info.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.getUser = async (accessToken, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().get(`/users/me`, options);

    statsdClient.increment('acs.get-user.success');
    logger.info({
      file: __filename,
      event: 'acsGetUser',
      data: {
        userID,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.get-user.failed');
    logger.error({
      file: __filename,
      event: 'acsGetUserError',
      error: err,
      data: {
        userID,
      },
    });

    throw err;
  }
};

/**
 * Update a user's info.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token.
 * @param {string} body - Request body to send.
 * @param {string} brand - Something from constants.Brands
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.putUser = async (accessToken, body, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().put(`/users`, body, options);

    statsdClient.increment('acs.put-user.success');
    logger.info({
      file: __filename,
      event: 'acsPutUser',
      data: {
        userID,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.put-user.failed');
    logger.error({
      file: __filename,
      event: 'acsPutUserError',
      error: err,
      data: {
        userID,
      },
    });
  }
};

/**
 * Log a user out of mobile apps.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token.
 * @param {string} brand - Something from constants.Brands
 * @param {Object} user -
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.userAppLogout = async (accessToken, brand, user) => {
  const userID = util.getUserIDFromAccessToken(accessToken);
  const identifier = user.Email ? `email:${user.Email}` : `phone:${user.PhoneNo}`;

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().delete(`/session/${identifier}`, options);

    statsdClient.increment('acs.app-logout.success');
    logger.info({
      file: __filename,
      event: 'acsUserAppLogout',
      data: {
        userID,
        identifier,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.app-logout.failed');
    logger.error({
      file: __filename,
      event: 'acsUserAppLogoutError',
      error: err,
      data: {
        userID,
        identifier,
      },
    });
  }
};

/**
 * Get a user's houses.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token.
 * @param {string} brand - Something from constants.Brands
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.getHouses = async (accessToken, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().get(`/users/houses/mine`, options);

    statsdClient.increment('acs.get-houses-feed.success');
    logger.info({
      file: __filename,
      event: 'acsGetHouses',
      data: {
        userID,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.get-houses-feed.failed');
    logger.error({
      file: __filename,
      event: 'acsGetHousesError',
      error: err,
      data: {
        userID,
      },
    });

    throw err;
  }
};

/**
 * Get a user's activity feed.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token.
 * @param {string} houseID -
 * @param {string} brand - Something from constants.Brands
 * @param {Object} options -
 * @param {number} [options.nextPage] - The full URL for next page
 * @returns {Promise<Object>} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.getActivityFeed = async (accessToken, houseID, brand, options) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    let url = `/houses/${houseID}/activities?`;
    if (options.nextPage) {
      const currentURL = new URL(options.nextPage);
      url = `${currentURL.pathname}${currentURL.search}`;
    }

    const requestOptions = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
        'accept-version': '4.0.0',
      },
    };
    const response = await getAcsClient().get(url, requestOptions);

    statsdClient.increment('acs.get-activity-feed.success');
    logger.info({
      file: __filename,
      event: 'acsGetActivityFeed',
      data: {
        userID,
        houseID,
        options,
      },
    });

    return response;
  } catch (err) {
    statsdClient.increment('acs.get-activity-feed.failed');
    logger.error({
      file: __filename,
      event: 'acsGetActivityFeedError',
      error: err,
      data: {
        userID,
        houseID,
        options,
      },
    });

    throw err;
  }
};

/**
 * Delete a user's account.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @returns {Object} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.deleteUser = async (accessToken, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().delete(`/users/${userID}?source=${repoPackage.name}`, options);

    statsdClient.increment('acs.delete-user.success');
    logger.info({
      file: __filename,
      event: 'acsDeleteUser',
      data: {
        userID,
      },
    });

    return response;
  } catch (error) {
    statsdClient.increment('acs.delete-user.failed');
    logger.error({
      file: __filename,
      event: 'acsDeleteUserError',
      error,
      data: {
        userID,
      },
    });

    throw error;
  }
};

/**
 * Get a list of 3rd party apps the user has oauth'd with.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @returns {Object} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.getUserApps = async (accessToken, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().get(`/apps/mine`, options);

    statsdClient.increment('acs.get-user-apps.success');
    logger.info({
      file: __filename,
      event: 'acsGetUserApps',
      data: {
        userID,
      },
    });

    return response;
  } catch (error) {
    statsdClient.increment('acs.get-user-apps.failed');
    logger.error({
      file: __filename,
      event: 'acsGetUserAppsError',
      error,
      data: {
        userID,
      },
    });

    throw error;
  }
};

/**
 * Get a list of locks the user is on.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @returns {Object} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.getUserLocks = async (accessToken, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().get(`/users/locks/mine`, options);

    statsdClient.increment('acs.get-user-locks.success');
    logger.info({
      file: __filename,
      event: 'acsGetUserLocks',
      data: {
        userID,
      },
    });

    return response;
  } catch (error) {
    statsdClient.increment('acs.get-user-locks.failed');
    logger.error({
      file: __filename,
      event: 'acsGetUserLocksError',
      error,
      data: {
        userID,
      },
    });

    throw error;
  }
};

/**
 * Get a list of doorbells the user is on.
 *
 * @async
 * @param {string} accessToken - The user's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @returns {Object} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.getUserDoorbells = async (accessToken, brand) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().get(`/users/doorbells/mine`, options);

    statsdClient.increment('acs.get-user-doorbells.success');
    logger.info({
      file: __filename,
      event: 'acsGetUserDoorbells',
      data: {
        userID,
      },
    });

    return response;
  } catch (error) {
    statsdClient.increment('acs.get-user-doorbells.failed');
    logger.error({
      file: __filename,
      event: 'acsGetUserDoorbellsError',
      error,
      data: {
        userID,
      },
    });

    throw error;
  }
};

/**
 * Factory reset lock
 *
 * @async
 * @param {string} accessToken - The user's/lock owner's rest api access token
 * @param {string} brand - Something from constants.Brands
 * @param {string} lockID - The lock to be reset
 * @returns {Object} The rest api response
 * @see https://www.npmjs.com/package/axios#response-schema
 * @throws {Error}
 */
module.exports.factoryResetLock = async (accessToken, brand, lockID) => {
  const userID = util.getUserIDFromAccessToken(accessToken);

  try {
    const options = {
      headers: {
        [config.get('acsAccessTokenHeader')]: accessToken,
        [config.get('brandingHeader')]: brand,
      },
    };
    const response = await getAcsClient().delete(`/locks/${lockID}`, options);

    statsdClient.increment('acs.factory-reset-request-approved.success');
    logger.info({
      file: __filename,
      event: 'acsFactoryResetLock',
      data: {
        userID,
        lockID,
        deleteLockStatusCode: response.status,
      },
    });

    return response;
  } catch (error) {
    statsdClient.increment('acs.factory-reset-request-approved.failed');
    logger.error({
      file: __filename,
      event: 'acsFactoryResetLockFailed',
      error,
      data: {
        userID,
        lockID,
      },
    });

    throw error;
  }
};
