# august-account-api

This repo provides the API portion of [account.august.com](https://account.august.com). It consists of a Restify app that talks with the [UI frontend](https://bitbucket.org/august_team/august-account-ui).

[Documentation on Confluence](https://augusthome.atlassian.net/wiki/spaces/ST/pages/716243032/Account+Management+Website)

## Project setup

```
npm install
```
Set local your apicreds.json file to have a 32 character string under `auguest-account-api.encryptionPassword`

### Run for development

```
npm run start
```

### Run tests

```
npm run test
npm run test:coverage
```

### Lint and fix files

```
npm run lint
```

###Deployment

Note this repo uses a single branch deployment model as of 10/23/2020. There is no need to merge into staging/production.
