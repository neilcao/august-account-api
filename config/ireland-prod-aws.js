'use strict';

module.exports = {
  port: 3030,
  acs: {
    host: 'https://ireland-prod-rest-api-internal.august.com',
  },
  messages: {
    instanceKey: 'account-api-prod',
  },
  cors: {
    origins: ['https://account.august.com', 'https://ireland-prod-account-api.august.com'],
  },
  redactNameFromDeletedUsers: {
    enabled: false,
  },
};
