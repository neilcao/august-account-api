'use strict';

module.exports = {
  port: 3030,
  acs: {
    host: 'http://rest-api-internal-prod.china.internal',
  },
  messages: {
    instanceKey: 'account-api-prod',
  },
  cors: {
    origins: ['https://account.aaecosys.com'],
  },
  redactNameFromDeletedUsers: {
    enabled: false,
  },
};
