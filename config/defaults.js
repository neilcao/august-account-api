'use strict';

const ms = require('ms');
const os = require('os');

const hostname = os.hostname();
const appname = process.env.appname || 'august-account-api';

module.exports = {
  accessTokenHeader: 'x-account-management-token',
  acsAccessTokenHeader: 'x-august-access-token',
  brandingHeader: 'x-august-branding',
  tokenExpirationTimeInMS: ms('1 hour'),
  healthPort: 10000,
  statsdConfig: {
    servicePrefix: 'august-account-api',
  },
  cacheKeys: {
    maxDownloadsPerUser: 'account_api_max_downloads_per_user',
    concurrentUserDownloadJobs: 'account_api_max_user_download_jobs',
    concurrentActivityDownloadsPerJob: 'account_api_max_activity_downloads_per_job',
    concurrentDownloadsPerActivity: 'account_api_max_downloads_per_activity',
    loginWhitelist: 'account_api_login_whitelist',
  },
  limits: {
    login: {
      timeframeInMS: ms('1 hour'),
      limit: 10,
    },
    codes: {
      timeframeInMS: ms('1 hour'),
      limit: 10,
    },
    logging: {
      timeframeInMS: ms('1 hour'),
      limit: 120,
    },
    activity: {
      limit: 200,
    },
  },
  delays: {
    activityFeedInMS: 1000,
  },
  logger: {
    file: {
      filename: `/logs/app/${hostname}.{pid}.${appname}.log`,
    },
    console: {
      silent: true,
    },
    logger: {
      maxLogLength: 50000,
    },
  },
  userTokenPrefix: 'account-management',
  s3: {
    userBucket: 'com.august.user-ireland-prod-aws',
    videoBucket: 'com.august.video-ireland-prod-aws',
    region: 'eu-west-1',
  },
  download: {
    saveDoorbellMedia: false,
    archive: {
      // We probably won't be able to compress the images and videos much
      // so set the compression low.
      compressionLevel: 1,
    },
    maxDownloadCopiesPerUser: 2,
    expirationTimeInMS: ms('30 days'),
    compressedFileBaseName: 'august_data',
    compressedFileDateformat: 'YYYY-MM-DD-HH-mm-ssZZ',
    localDirectoryBasePath: '/tmp',
    activityFeed: {
      saveFileName: 'activity.csv',
      saveFileHeaders: ['Time', 'Action', 'User'],
    },
    videoPreviewImageName: 'preview',
    // How many copies of user data each node process will process at the same time.
    concurrentUserDownloadJobs: 2,
    // Each job will need to download a set of files for each activity that is a doorbell video.
    // This is how many of those activities will be processed at the same time.
    concurrentActivityDownloadsPerJob: 10,
    // Download this many files at the same time when downloading files for an activity.
    concurrentDownloadsPerActivity: 10,
    signedUrlExpirationInSeconds: ms('30 days') / 1000,
    failedExpirationInMS: ms('1 hour'),
  },
  i18n: {
    defaultLanguage: 'en',
  },
  email: 'August Home <noreply@august.com>',
  useLoginWhitelist: false,
  messages: {
    connectionManagerEnabled: true,
    shutdownOnExitEnabled: true,
  },
  cors: {
    origins: [
      // placeholder
    ],
  },
  redactNameFromDeletedUsers: {
    enabled: true,
    intervalMs: ms('20 min'),
    key: 'redactNameFromDeletedUsers',
    options: {
      logSkipping: true,
      logSuccess: true,
    },
  },
};
