'use strict';

module.exports = {
  port: 3030,
  acs: {
    host: 'https://api-dev-aws.august.com',
  },
  messages: {
    exchangePrefix: 'dev-aws',
    instanceKey: 'account-api-dev',
  },
  useLoginWhitelist: true,
  cors: {
    origins: ['https://account-dev-aws.august.com'],
  },
};
