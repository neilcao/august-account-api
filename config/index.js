'use strict';

const configLoader = require('august-config-loader');
const messages = require('august-messages');
const model = require('august-model');

const cacheModule = require('../cache');

const env = process.env.NODE_ENV || 'local';

const defaultConfig = require(`./defaults`);
const envConfig = require(`./${env}`);
const nconfInstance = configLoader.instantiateNconfInstance(defaultConfig, envConfig);

exports.init = async () => {
  await configLoader.overridesNconfInstance(nconfInstance);
};

/**
 * On Change method.
 *
 * Requires that cacheModule already be initialized
 *
 * @returns {undefined}
 */
exports.onChange = function onChange() {
  const options = {
    model,
    messages,
    cache: [cacheModule.cache],
  };

  configLoader.onChange(options);
};

exports.get = nconfInstance.get.bind(nconfInstance);
exports.set = nconfInstance.set.bind(nconfInstance);
