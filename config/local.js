'use strict';

const ms = require('ms');

module.exports = {
  port: 4040,
  healthPort: 10010,
  acs: {
    host: 'http://localhost',
    port: 3030,
  },
  db: {
    august: 'mongodb://localhost/august',
    logging: 'mongodb://localhost/logging',
  },
  redis: {
    connection: {
      host: '127.0.0.1',
      port: 6379,
    },
  },
  limits: {
    login: {
      timeframeInMS: ms('1 hour'),
      limit: 100,
    },
    codes: {
      timeframeInMS: ms('1 hour'),
      limit: 100,
    },
  },
  delays: {
    activityFeedInMS: 1,
  },
  logger: {
    console: {
      silent: false,
      format: ['colorize', 'simple', 'align', 'timestamp'],
    },
    file: {
      silent: true,
    },
  },
  messages: {
    instanceKey: 'account-api',
    exchangePrefix: 'local',
  },
  cors: {
    origins: [
      'http://localhost:8080', // npm serve for Vue.js dev env
    ],
  },
};
