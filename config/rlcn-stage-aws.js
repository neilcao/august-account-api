'use strict';

module.exports = {
  port: 3030,
  acs: {
    host: 'http://rest-api-internal-stage.china.internal',
  },
  messages: {
    exchangePrefix: 'stage',
    instanceKey: 'account-api-stage',
  },
  download: {
    saveDoorbellMedia: false,
  },
  cors: {
    origins: ['https://account-stage.aaecosys.com'],
  },
  redactNameFromDeletedUsers: {
    enabled: false,
  },
};
