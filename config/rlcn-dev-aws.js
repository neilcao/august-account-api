'use strict';

module.exports = {
  port: 4040,
  acs: {
    host: 'http://rest-api.rlcn-std.svc:3030',
  },
  logger: {
    console: {
      silent: false,
    },
    logger: {
      maxStackFrames: 10,
      minimumLogLevel: 'debug',
    },
  },
  messages: {
    exchangePrefix: 'dev-aws',
    instanceKey: 'account-api-dev',
  },
  cors: {
    origins: [
      'https://account-dev.aaecosys.com',
      'https://account-ft1.aaecosys.com',
      'https://account-ft2.aaecosys.com',
    ],
  },
  redactNameFromDeletedUsers: {
    enabled: false,
  },
};
