'use strict';

const ms = require('ms');

module.exports = {
  port: 4545,
  healthPort: 10011,
  acs: {
    host: 'http://localhost',
    port: 3030,
  },
  db: {
    august: 'mongodb://localhost/august-test',
    logging: 'mongodb://localhost/logging-test',
  },
  redis: {
    connection: {
      host: '127.0.0.1',
      port: 6379,
    },
  },
  limits: {
    login: {
      timeframeInMS: ms('10 seconds'),
      limit: 1,
    },
    codes: {
      timeframeInMS: ms('10 seconds'),
      limit: 1,
    },
    logging: {
      timeframeInMS: ms('1 hour'),
      limit: 1,
    },
  },
  delays: {
    activityFeedInMS: 1,
  },
  logger: {
    console: {
      silent: false,
    },
    file: {
      silent: true,
    },
  },
  messages: {
    instanceKey: 'account-api',
    exchangePrefix: 'test',
  },
  download: {
    maxDownloadCopiesPerUser: 20,
    saveDoorbellMedia: true,
  },
  cors: {
    origins: ['*'],
  },
  redactNameFromDeletedUsers: {
    enabled: false,
  },
};
