'use strict';

module.exports = {
  port: 3030,
  acs: {
    host: 'https://ireland-stage-rest-api.august.com',
  },
  messages: {
    exchangePrefix: 'stage',
    instanceKey: 'account-api-stage',
  },
  download: {
    saveDoorbellMedia: true,
  },
  cors: {
    origins: ['https://ireland-stage-account.august.com', 'https://account-stage-aws.august.com'],
  },
};
