'use strict';

const joi = require('joi');
const constants = require('august-constants');
const validatePostSessionPolyAuthType = [
  constants.UserIdentifierType.PHONE,
  constants.UserIdentifierType.EMAIL,
  constants.UserIdentifierType.PASSWORD,
  ...constants.UserIdentifierType.getExternalTypes(),
];

module.exports.postLoginPolyauth = joi.object().keys({
  params: joi.object(),
  query: joi.object(),
  body: joi
    .object()
    .keys({
      identifierType: joi
        .string()
        .valid(...validatePostSessionPolyAuthType)
        .required(),
      password: joi.string().optional(),
      identifier: joi.string().required(),
    })
    .unknown(true),
});

module.exports.postValidatePolyauth = joi.object().keys({
  params: joi
    .object()
    .keys({
      identifierType: joi
        .string()
        .valid(...validatePostSessionPolyAuthType)
        .required(),
    })
    .unknown(true),
  query: joi.object(),
  body: joi.object().keys({
    phone: joi.string().optional(),
    email: joi
      .string()
      .email()
      .optional(),
    wechat: joi
      .string()
      .min(1)
      .optional(),
    apple: joi
      .string()
      .min(1)
      .optional(),
    password: joi.string().optional(),
    code: joi
      .string()
      .when('phone', {
        is: joi.exist(),
        then: joi.string().required(),
      })
      .when('email', {
        is: joi.exist(),
        then: joi.string().required(),
      }),
  }),
});
