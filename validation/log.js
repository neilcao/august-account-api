'use strict';

const joi = require('joi');

const allowedLoggerMethods = ['info', 'debug', 'warn', 'error'];

exports.processLog = joi.object().keys({
  params: joi.object(),
  query: joi.object(),
  body: joi.object().keys({
    data: joi.object(),
    component: joi.string().required(),
    error: joi.string(),
    event: joi.string().required(),
    type: joi
      .string()
      .required()
      .valid(allowedLoggerMethods),
  }),
});
