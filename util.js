const _ = require('lodash');
const axios = require('axios');
const fs = require('fs');

const AccessToken = require('august-access-token');
const logger = require('august-logger');
const messages = require('august-messages');
const model = require('august-model');
const statsdClient = require('august-statsd').statsdClient;

/**
 * Parse an access token and return userID.
 *
 * @param {string} tokenString - The encoded access token.
 * @returns {string} The user ID of the access token.
 */
module.exports.getUserIDFromAccessToken = tokenString => {
  const token = new AccessToken();
  token.parseJWT(tokenString);

  return _.get(token, 'UserID', 'Unknown');
};

/**
 * Download a file.
 *
 * @param {string} url -
 * @param {string} path - Save the file to this path.
 * @returns {Promise.<undefined|Error>} Resolves when the file has been downloaded.
 */
module.exports.downloadFile = (url, path) => {
  return new Promise((resolve, reject) => {
    const writeStream = fs.createWriteStream(path);
    writeStream.on('error', reject);
    writeStream.on('close', resolve);

    return axios({
      method: 'get',
      url,
      responseType: 'stream',
    }).then(readStream => {
      readStream.data.on('error', reject);
      readStream.data.pipe(writeStream);
    });
  });
};

/**
 * Send an email to all the email addresses we have for a user.
 *
 * @async
 * @param {Object} userID -
 * @param {Array} emails - The user's email addresses.
 * @param {Object} emailData - Data to pass in the queue message to notification server.
 * @returns {Promise<undefined>} - Resolves when the queue messages have been published.
 */
module.exports.sendEmailToUser = async (userID, emails, emailData) => {
  for (let email of emails) {
    const data = {
      to: email,
      ...emailData,
    };

    try {
      const context = await messages.publish('notification.email', data);

      logger.info({
        file: __filename,
        event: 'sendEmailToUser',
        data: {
          messageKey: context.key,
          messageHistory: context.history,
          userID,
          email,
        },
      });
      statsdClient.increment('message.publish.success');
    } catch (err) {
      logger.error({
        file: __filename,
        event: 'sendEmailToUserError',
        error: err,
        data: {
          userID,
          email,
        },
      });
      statsdClient.increment('message.publish.failure');
    }
  }
};

/**
 * Get all a user's emails.
 *
 * @param {string} userID -
 * @returns {Promise<Array|Error>} The user's emails.
 */
module.exports.getUserEmails = async userID => {
  const limit = 0;
  const emails = await model.userIdentifiers.findEmailForUserID(userID, limit).map(identifier => {
    return model.userIdentifiers.getValue(identifier);
  });

  return emails;
};
