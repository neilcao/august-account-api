'use strict';

const factoryresetrequest = require('../middleware/factoryresetrequest');
const locks = require('../middleware/locks');
const assert = require('../middleware/assert');

exports = module.exports = function(api) {
  api.get(
    {
      path: '/factoryresetrequests',
    },
    [locks.getSuperUserLocks, factoryresetrequest.getAllFactoryResetRequests]
  );

  api.put(
    {
      path: '/factoryresetrequests',
    },
    [assert.assertLockSuperUser, factoryresetrequest.process]
  );
};
