'use strict';

const download = require('../middleware/download.js');

exports = module.exports = function (api) {
  api.post('/downloads', [download.createDownload]);
  api.get('/downloads', [download.getDownloads]);
  api.del('/downloads/:downloadID', [download.getDownload, download.assertUserOwnsDownload, download.deleteDownload]);
};
