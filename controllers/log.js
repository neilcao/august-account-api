'use strict';

const log = require('../middleware/log.js');

const validation = require('../validation/log.js');

exports = module.exports = function (api) {
  api.post(
    {
      path: '/log',
      validate: validation.processLog,
    },
    [log.processLog]
  );
};
