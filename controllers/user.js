'use strict';

const user = require('../middleware/user.js');

exports = module.exports = function (api) {
  api.get('/user', [user.getUser]);
  api.put('/user', [user.putUser]);
  api.del('/user', [user.deleteUser]);
  api.put('/user/logout', [user.appLogout]);
};
