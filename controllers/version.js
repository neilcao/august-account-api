'use strict';

const version = require('../middleware/version.js');

exports = module.exports = function (api) {
  api.get(
    {
      path: '/version',
      overrideAuth: true,
    },
    [version.getVersion]
  );
};
