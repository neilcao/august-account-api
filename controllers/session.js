'use strict';

const joi = require('joi');

const constants = require('august-constants');

const session = require('../middleware/session.js');

const validateFromLinkRequestSchema = joi.object().keys({
  params: joi.object().unknown(),
  query: joi.object(),
  body: joi
    .object()
    .keys({
      token: joi.string().required(),
      code: joi
        .string()
        .length(6)
        .required(),
      identifier: joi
        .string()
        .regex(/^(phone|email):.+/)
        .required(),
      brand: joi
        .string()
        .valid(...constants.Brands.toArray())
        .required(),
    })
    .required(),
});

exports = module.exports = function(api) {
  api.post(
    {
      path: '/login',
      overrideAuth: true,
    },
    [session.login]
  );

  api.put(
    {
      path: '/login',
    },
    [session.extendSession]
  );

  api.post(
    {
      path: '/logout',
      overrideAuth: true,
    },
    [session.logout]
  );

  api.post(
    {
      path: '/validate/:identifier',
      overrideAuth: true,
    },
    [session.validateVerificationCode]
  );

  api.post(
    {
      path: '/sendcode',
      overrideAuth: true,
    },
    [session.sendVerificationCode]
  );

  api.post(
    {
      path: '/validateFromLink',
      overrideAuth: true,
      validate: validateFromLinkRequestSchema,
    },
    [session.validateFromLinkByToken]
  );
};
