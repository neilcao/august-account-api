fmt='
	r=%(refname)
	t=%(*objecttype)
	T=${r#refs/tags/}

	o=%(*objectname:short)
	n=%(*authorname)
	e=%(*authoremail)
	s=%(*subject)
	d=%(*authordate)
	b=%(*body)
    c=%(taggerdate:short)

	if test "z$t" = z;then
        t=%(objecttype)
		o=%(objectname:short)
		n=%(authorname)
		e=%(authoremail)
		s=%(subject)
		d=%(authordate)
		b=%(body)
	fi

    if  [[  "$e" =~ ".*assaabloy.*"  ]];then
        if test "z$c" = z;then
            echo "Tag $T points at a $t object $o which was created by authored $n $e without standard."
        else
            echo "Tag $T points at a $t object $o which was created at $c by authored $n $e with standard."
        fi
    fi
'

eval=`git for-each-ref --shell --format="$fmt"  --sort=-taggerdate --sort=-refname refs/tags`

TOKEN=$(aws ecr get-authorization-token --output text --query 'authorizationData[].authorizationToken')
ecr_tags_json=$(curl -s -H "Authorization: Basic $TOKEN" https://968187346014.dkr.ecr.cn-northwest-1.amazonaws.com.cn/v2/account-api/tags/list)

i=1
ecr_tags_arry=()
echo $ecr_tags_json | jq -c '.tags[]'  | while read aws_line; do
    ecr_tag=${aws_line//\"/}
    ecr_tags_arry[$i]=$ecr_tag
    let i=i+1
done

eval "$eval" | head -5 | while read git_line
do
    echo $git_line
    git_tag=`echo $git_line | awk '{print $2}'`
    if [[ ${ecr_tags_arry[@]/${git_tag}/} != ${ecr_tags_arry[@]} ]];then
        echo "yes"
    else
        echo "no"
    fi
done
