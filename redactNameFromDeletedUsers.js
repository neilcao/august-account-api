'use strict';

const timers = require('august-timer');
const model = require('august-model');
const logger = require('august-logger');

module.exports = function redactNameFromDeletedUsers(intervalMs, key, options) {
  return timers.standAlonePeriodicTimer(
    intervalMs,
    key,
    () => {
      const timeStart = Date.now();
      model.users
        .redactDeletedUserNames()
        .then(({ updatedCount, userIDs }) => {
          logger.info({
            file: __filename,
            event: 'redactDeletedUserNames',
            data: {
              updatedCount,
              userIDsStr: JSON.stringify(userIDs),
              userIDsLength: userIDs.length,
              timingMs: Date.now() - timeStart,
            },
          });
        })
        .catch(err => {
          logger.error({
            file: __filename,
            event: 'redactDeletedUserNamesError',
            error: err,
            data: {
              timingMs: Date.now() - timeStart,
            },
          });
        });
    },
    options
  );
};
