'use strict';

const _ = require('lodash');
const Bull = require('bull');
const archiver = require('archiver');
const fs = require('fs-extra');
const https = require('https');
const moment = require('moment');
const path = require('path');
const Promise = require('bluebird');
const rimraf = require('rimraf');
const uuidv4 = require('uuid/v4');

const babelfish = require('august-babelfish');
const constants = require('august-constants');
const entities = require('august-html-entities');
const errors = require('restify-errors');
const extract = require('august-extract');
const logger = require('august-logger');
const model = require('august-model');

const statsdClient = require('august-statsd').statsdClient;

const acs = require('../acs.js');
const augustCache = require('../cache.js');
const config = require('../config/index.js');
const util = require('../util.js');
const s3Util = require('../s3.js');

const readdir = Promise.promisify(fs.readdir);
const lstat = Promise.promisify(fs.lstat);

const ACTIVITY_FEED_CSV_HEADERS = config.get('download:activityFeed:saveFileHeaders');
const ACTIVITY_FEED_FILE_NAME = config.get('download:activityFeed:saveFileName');
const DEFAULT_LANGUAGE_CODE = config.get('i18n:defaultLanguage');
const LOCAL_DIRECTORY_BASE_PATH = config.get('download:localDirectoryBasePath');
const S3_BUCKET = config.get('s3:userBucket');

const ACTIVITIES_WITH_MEDIA = [
  constants.Events.DOORBELL_CALL_ANSWERED,
  constants.Events.DOORBELL_CALL_ANSWERED_DUPLICATE,
  constants.Events.DOORBELL_CALL_DECLINED,
  constants.Events.DOORBELL_CALL_HANGUP,
  constants.Events.DOORBELL_CALL_INITIATED,
  constants.Events.DOORBELL_CALL_MISSED,
  constants.Events.DOORBELL_MOTION_DETECTED,
];

const internals = (module.exports.internals = {
  rimraf: Promise.promisify(rimraf),
  mkdir: Promise.promisify(fs.mkdir),
  mkdirp: fs.mkdirp,
  writeFile: Promise.promisify(fs.writeFile),
});

/**
 * init download queue
 *
 * @returns {Promise.<undefined>} -
 */
module.exports.initDownloadQueue = async () => {
  internals.downloadQueue = await createDownloadQueue();
};

/**
 * Create a copy of a user's data and prepare it for download.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.createDownload = async (req, res, next) => {
  const userID = req.get('userID');
  const accessToken = req.get('acsAccessToken');
  const brand = req.body.brand || constants.Brands.AUGUST;

  const localDirectory = `${LOCAL_DIRECTORY_BASE_PATH}/${uuidv4()}`;
  const expiresAt = new Date(Date.now() + config.get('download:expirationTimeInMS'));
  const user = await model.users.findByID(userID);

  let userDownload;
  let includeActivity = true;
  let includeVideos = true;
  let includeImages = true;

  try {
    includeActivity = extract.external(req, 'body.activity');
    includeVideos = extract.external(req, 'body.videos');
    includeImages = extract.external(req, 'body.images');
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'createDownloadParamError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.create.failure');

    return next(err);
  }

  const numExistingDownloads = await model.usersDownloads.countActiveByUserID(userID);
  const maxDownloadsPerUserCache = await augustCache.cache.get(config.get('cacheKeys:maxDownloadsPerUser'));
  const maxDownloadsPerUserConfig = config.get('download:maxDownloadCopiesPerUser');
  const maxDownloadsPerUser = maxDownloadsPerUserCache || maxDownloadsPerUserConfig;

  if (numExistingDownloads >= maxDownloadsPerUser) {
    const options = {
      user,
      maxCopies: maxDownloadsPerUser,
    };
    const err = new errors.BadRequestError(babelfish.t('downloadCreateFailureTooMany', options));

    logger.error({
      file: __filename,
      event: 'createDownloadMaxCopiesError',
      error: err,
      data: {
        userID,
        numExistingDownloads,
        maxDownloadsPerUser,
      },
    });

    return next(err);
  }

  try {
    userDownload = await model.usersDownloads.insert(userID, constants.UserDownloadStatuses.REQUESTED, expiresAt);
    internals.makeHumanReadable(userDownload, user);

    res.send(202, userDownload);
    next();

    await internals.downloadQueue.add({
      localDirectory,
      accessToken,
      userID,
      brand,
      userDownload,
      includeActivity,
      includeVideos,
      includeImages,
    });

    logger.info({
      file: __filename,
      event: 'createDownload',
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.create.success');
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'createDownloadError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.create.failure');

    return next(err);
  }
};

/**
 * Get the metadata for all a user's data copies.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.getDownloads = async (req, res, next) => {
  const userID = req.get('userID');

  try {
    const projection = {
      userID: 0,
      updatedAt: 0,
    };
    const userDownloads = await model.usersDownloads.findActiveByUserID(userID, projection);
    const user = await model.users.findByID(userID);
    internals.makeHumanReadable(userDownloads, user);

    res.send(userDownloads);
    next();

    logger.info({
      file: __filename,
      event: 'getDownloads',
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.get.success');
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getDownloadsError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.get.failure');

    return next(err);
  }
};

/**
 * Get the metadata for one copy of a user's data.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {Promise<undefined>} -
 */
module.exports.getDownload = async (req, res, next) => {
  const userID = req.get('userID');
  const downloadID = req.params.downloadID;

  try {
    const download = await model.usersDownloads.findByID(downloadID);
    if (!download) {
      throw new errors.ResourceNotFoundError(`Download ${downloadID} does not exist`);
    }

    req.set('download', download);
    next();

    logger.info({
      file: __filename,
      event: 'getDownload',
      data: {
        userID,
        downloadID,
      },
    });
    statsdClient.increment('downloads.get-one.success');
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getDownloadError',
      error: err,
      data: {
        userID,
        downloadID,
      },
    });
    statsdClient.increment('downloads.get-one.failure');

    return next(err);
  }
};

/**
 * Delete a download.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {Promise<undefined>} -
 */
module.exports.deleteDownload = async (req, res, next) => {
  const userID = req.get('userID');
  const download = req.get('download');

  try {
    if (download.url) {
      await internals.deleteFromS3(download.url, userID);
    }

    await model.usersDownloads.update(download._id, {
      status: constants.UserDownloadStatuses.DELETED,
    });

    res.send();
    next();

    logger.info({
      file: __filename,
      event: 'deleteDownload',
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.delete.success');
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'deleteDownloadError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('downloads.delete.failure');

    return next(err);
  }
};

/**
 * Make sure the calling user is the one who owns a download.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {Promise<undefined>} -
 */
module.exports.assertUserOwnsDownload = async (req, res, next) => {
  const userID = req.get('userID');
  const download = req.get('download');
  const downloadID = download._id.toString();

  if (userID !== download.userID) {
    const err = new errors.ForbiddenError(`User ${userID} does not own download ${downloadID}`);

    logger.error({
      file: __filename,
      event: 'assertUserOwnsDownloadError',
      error: err,
      data: {
        userID,
        downloadID,
      },
    });
    statsdClient.increment('downloads.assert-owner.failure');

    return next(err);
  }

  logger.info({
    file: __filename,
    event: 'assertUserOwnsDownload',
    data: {
      userID,
      downloadID,
    },
  });
  statsdClient.increment('downloads.assert-owner.success');

  next();
};

/**
 * Create the local directory needed to store the user's files before compressing and sending to S3.
 *
 * @async
 * @param {string} directory - Create this directory
 * @returns {Promise.<undefined>} Resolves when the directories are created.
 */
internals.createLocalDirectory = async directory => {
  if (!path.normalize(directory).startsWith(path.normalize(LOCAL_DIRECTORY_BASE_PATH))) {
    const err = new Error(`Cannot create directories outside of ${LOCAL_DIRECTORY_BASE_PATH}`);

    logger.error({
      file: __filename,
      event: 'createLocalDirectoryError',
      error: err,
      data: {
        directory,
        LOCAL_DIRECTORY_BASE_PATH,
      },
    });

    throw err;
  }

  await internals.mkdir(directory);

  logger.info({
    file: __filename,
    event: 'createLocalDirectory',
    data: {
      directory,
      LOCAL_DIRECTORY_BASE_PATH,
    },
  });
};

/**
 *
 * Delete the local directory needed to store the user's files before compressing and sending to S3.
 *
 * @async
 * @param {string} directory - Delete this directory and all it's files.
 * @returns {Promise.<undefined>} Resolves when the directory is deleted.
 */
internals.deleteLocalDirectory = async directory => {
  if (!path.normalize(directory).startsWith(path.normalize(LOCAL_DIRECTORY_BASE_PATH))) {
    const err = new Error(`Cannot delete directories outside of ${LOCAL_DIRECTORY_BASE_PATH}`);

    logger.error({
      file: __filename,
      event: 'deleteLocalDirectoryError',
      error: err,
      data: {
        directory,
        LOCAL_DIRECTORY_BASE_PATH,
      },
    });

    return;
  }

  await internals.rimraf(directory);

  logger.info({
    file: __filename,
    event: 'deleteLocalDirectory',
    data: {
      directory,
      LOCAL_DIRECTORY_BASE_PATH,
    },
  });
};

/**
 * Get a user's profile and house pictures
 *
 * @param {string} accessToken - The user's acs access token.
 * @param {string} brand - Something from constants.Brands
 * @returns {Promise<Array>} An array of urls to download
 */
internals.getUserImages = async function getUserImages(accessToken, brand) {
  let houses;
  let user;
  let urls = [];

  try {
    user = (await acs.getUser(accessToken, brand)).data;

    let url = _.get(user, 'imageInfo.original.secure_url');
    if (url) {
      urls.push(url);
    }
    url = _.get(user, 'imageInfo.thumbnail.secure_url');
    if (url) {
      urls.push(url);
    }

    houses = await acs.getHouses(accessToken, brand);

    for (let house of houses.data) {
      url = _.get(house, 'imageInfo.secure_url');
      if (url) {
        urls.push(url);
      }
    }

    let urlsSet = new Set(urls);
    urls = Array.from(urlsSet);

    logger.info({
      file: __filename,
      event: 'getUserImages',
      data: {
        urls,
      },
    });

    return urls;
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getUserImagesError',
      error: err,
    });
  }
};

/**
 * Save user images to disk
 *
 * @param {Array<String>} urls - Image urls to download
 * @param {string} localDirectory - Save the activity feed to a file in this directory.
 * @returns {Array<Promise>} Returns when the activity feed has been saved to disk.
 */
internals.saveUserImages = function saveUserImages(urls, localDirectory) {
  return Promise.all(
    urls.map(async url => {
      let fileName = path.basename(url);
      let downloadPath = `${localDirectory}/${fileName}`;

      // eslint-disable-next-line security/detect-non-literal-fs-filename
      let file = fs.createWriteStream(downloadPath);

      return new Promise(function(resolve, reject) {
        https
          .get(url, function(response) {
            response.pipe(file);
            file.on('finish', function() {
              logger.info({
                file: __filename,
                event: 'saveUserImages',
                data: {
                  url,
                },
              });
              file.close();
              resolve();
            });
          })
          .on('error', function(err) {
            file.close();
            fs.unlink(downloadPath);
            logger.error({
              file: __filename,
              event: 'saveUserImagesError',
              error: err,
              data: {
                url,
              },
            });
            reject(err);
          });
      });
    })
  );
};

/**
 * Get a user's activity feed, which is actually the activity feed for all the user's houses.
 *
 * @async
 * @param {string} accessToken - The user's acs access token.
 * @param {string} brand - Something from constants.Brands
 * @returns {Promise<Array>} The user's activity feed.
 */
internals.getActivityFeed = async (accessToken, brand) => {
  let housesResult;
  let houseIDs;
  let houseID;
  let activityFeedAll = [];
  let activityFeedCurrent;
  let nextPage;
  let options = {};

  try {
    housesResult = await acs.getHouses(accessToken, brand);
    houseIDs = _.get(housesResult, 'data', []).map(house => {
      if (house.type === 'superuser') {
        return house.HouseID;
      }
    });

    houseIDs = houseIDs.filter(Boolean);

    for (houseID of houseIDs) {
      do {
        if (nextPage) {
          options.nextPage = nextPage;
        }

        activityFeedCurrent = await acs.getActivityFeed(accessToken, houseID, brand, options);

        if (_.get(activityFeedCurrent, 'data.events.length', 0) > 0) {
          activityFeedAll = activityFeedAll.concat(activityFeedCurrent.data.events);
        }

        if (_.get(activityFeedCurrent, 'data.nextPage')) {
          nextPage = activityFeedCurrent.data.nextPage;
        } else {
          nextPage = null;
        }

        await Promise.delay(config.get('delays:activityFeedInMS'));
      } while (_.get(activityFeedCurrent, 'data.nextPage'));
    }

    activityFeedAll.sort((lhs, rhs) => lhs.dateTime - rhs.dateTime);

    const activityFeedCsv = activityFeedAll.map(item => {
      const actionKey = constants.ActionKeys.fromAcsV2Log(item);
      const title = constants.ActionKeys.toTitle(
        actionKey,
        item.deviceName,
        _.get(item, 'otherUser.FirstName'),
        _.get(item, 'otherUser.LastName')
      );
      const time = new Date(item.dateTime);

      let fullName = '';
      if (_.get(item, 'user.UserID') !== constants.UnknownUser.UserID) {
        const firstName = _.get(item, 'user.FirstName', '');
        const lastName = _.get(item, 'user.LastName', '');
        fullName = `${firstName} ${lastName}`;
      }

      return [time, title, fullName];
    });
    activityFeedCsv.unshift(ACTIVITY_FEED_CSV_HEADERS);

    logger.info({
      file: __filename,
      event: 'getActivityFeed',
      data: {
        houseIDs,
        activities: activityFeedAll.length,
      },
    });

    return activityFeedAll;
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getActivityFeedError',
      error: err,
    });
  }
};

/**
 * Save an activity feed to disk.
 *
 * @async
 * @param {Array} activities - The activity feed to save to disk.
 * @param {string} localDirectory - Save the activity feed to a file in this directory.
 * @returns {undefined} Returns when the activity feed has been saved to disk.
 */
internals.saveActivityFeed = async (activities, localDirectory) => {
  const activityFeedFilePath = `${localDirectory}/${ACTIVITY_FEED_FILE_NAME}`;

  try {
    const activityFeedCsv = activities.map(item => {
      let title = item.title;
      title = title.replace(/<b>|<\/b>/g, '');
      const time = new Date(item.timestamp);

      let fullName = '';
      if (_.get(item, 'user.UserID') !== constants.UnknownUser.UserID) {
        const firstName = _.get(item, 'user.FirstName', '');
        const lastName = _.get(item, 'user.LastName', '');
        fullName = `${firstName} ${lastName}`;
      }

      return [time, title, fullName];
    });
    activityFeedCsv.unshift(ACTIVITY_FEED_CSV_HEADERS);

    await internals.writeFile(activityFeedFilePath, activityFeedCsv.join('\n'));

    logger.info({
      file: __filename,
      event: 'saveActivityFeed',
      data: {
        activityFeedFilePath,
        activities: activities.length,
      },
    });
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'saveActivityFeedError',
      error: err,
      data: {
        activityFeedFilePath,
        activities: activities.length,
      },
    });
  }
};

/**
 * Compress the files in a directory.
 *
 * @param {string} directory - Compress all files in this directory.
 * @returns {Promise.<string>} Resolves over the compressed file's path.
 */
internals.compressUserFiles = directory => {
  logger.info({
    file: __filename,
    event: 'compressUserFilesStart',
    data: {
      directory,
    },
  });

  // eslint-disable-next-line no-async-promise-executor
  return new Promise(async (resolve, reject) => {
    const compressFileName = internals.getCompressFileName();
    const compressFilePath = `${directory}/${compressFileName}`;
    const output = fs.createWriteStream(compressFilePath);
    const archive = archiver('zip', {
      zlib: { level: config.get('download:archive:compressionLevel') },
      forceZip64: true,
    });

    output.on('close', async () => {
      logger.info({
        file: __filename,
        event: 'compressUserFiles',
        data: {
          directory,
          compressFilePath,
        },
      });

      resolve(compressFilePath);
    });

    archive.on('warning', err => {
      if (err.code === 'ENOENT') {
        // TODO: log warning?
      } else {
        logger.error({
          file: __filename,
          event: 'compressUserFilesArchiveCreationError',
          error: err,
          data: {
            directory,
            compressFilePath,
          },
        });

        reject(err);
      }
    });

    archive.on('progress', function() {
      // TODO: log progress?
    });

    archive.on('error', function(err) {
      // TODO: Retry archive creation.
      logger.error({
        file: __filename,
        event: 'compressUserFilesArchiveCreationError2',
        error: err,
        data: {
          directory,
          compressFilePath,
        },
      });

      reject(err);
    });

    archive.pipe(output);

    const files = await readdir(directory);
    for (let file of files) {
      const stat = await lstat(`${directory}/${file}`);

      if (file === path.basename(compressFilePath)) {
        // Skip the filehandle for the compressed file, which already exists at this point.
      } else if (stat.isFile()) {
        archive.file(`${directory}/${file}`, { name: file });
      } else if (stat.isDirectory()) {
        archive.directory(`${directory}/${file}`, file);
      }
    }

    archive.finalize();
  });
};

/**
 * Upload a file to S3.
 *
 * @param {string} filePath - Path to the file to be uploaded.
 * @param {string} userID -
 * @returns {Promise.<undefined>} Resolves when the file is uploaded.
 */
internals.uploadToS3 = (filePath, userID) => {
  // TODO: Ensure filePath isn't outside of the allowed base directory.
  logger.info({
    file: __filename,
    event: 'uploadToS3Start',
    data: {
      filePath,
      userID,
    },
  });

  const s3Key = `${userID}/${path.basename(filePath)}`;

  return s3Util
    .upload(S3_BUCKET, s3Key, filePath)
    .tap(result => {
      logger.info({
        file: __filename,
        event: 'uploadToS3',
        data: {
          filePath,
          userID,
          key: result.Key,
        },
      });
    })
    .tapCatch(err => {
      // TODO: Retry S3 upload.
      logger.error({
        file: __filename,
        event: 'uploadToS3StreamError',
        error: err,
        data: {
          filePath,
          userID,
        },
      });
    });
};

/**
 * Delete a file from S3.
 *
 * @param {string} url - URL of file to delete.
 * @param {string} userID -
 * @returns {Promise.<undefined>} Resolves when the file is deleted.
 */
internals.deleteFromS3 = (url, userID) => {
  // TODO: Ensure url isn't outside of the allowed base directory.

  url = url.replace(/%3A/g, ':').replace(/%2B/g, '+');
  const s3Key = `${userID}/${path.basename(url)}`;

  return s3Util
    .deleteObject(S3_BUCKET, s3Key)
    .tap(() => {
      logger.info({
        file: __filename,
        event: 'deleteFromS3',
        data: {
          userID,
          Key: s3Key,
        },
      });
    })
    .tapCatch(err => {
      // TODO: Retry S3 delete.
      logger.error({
        file: __filename,
        event: 'deleteFromS3Error',
        error: err,
        data: {
          userID,
          Key: s3Key,
        },
      });
    });
};

/**
 * Tkae one or more user download documents and make the fields more human readable.
 *
 * @param {Object|Array} userDownloads - One or more documents from db.usersdownloads.
 * @param {Object} user - The document from the users collection.
 * @returns {Object} The modified document(s).
 */
internals.makeHumanReadable = (userDownloads, user) => {
  const languageCode = _.get(user, 'languageCode', DEFAULT_LANGUAGE_CODE);

  moment.locale(languageCode);

  if (!Array.isArray(userDownloads)) {
    userDownloads = [userDownloads];
  }

  userDownloads.forEach(item => {
    ['createdAt', 'expiresAt', 'updatedAt'].forEach(key => {
      if (item[key]) {
        item[key] = moment(item[key]).fromNow();
      }
    });

    item.status = babelfish.t(item.status, { user });
  });
};

internals.getCompressFileName = () => {
  return `${config.get('download:compressedFileBaseName')}_${moment().format(
    config.get('download:compressedFileDateFormat')
  )}.zip`;
};

/**
 * For every doorbell video entry in a user's activity feed, save doorbell videos and images to local disk.
 *
 * @param {Array} activities - The activity feed from acs.
 * @param {string} directory - Save the media to this directory.
 * @param {string} userID -
 * @param {boolean} includeVideos - If true, include videos in the download.
 * @param {boolean} includeImages - If true, include images in the download.
 * @returns {Promise.<undefined>} -
 */
internals.saveMediaFromActivityFeed = async (activities, directory, userID, includeVideos, includeImages) => {
  const concurrencyCache = await augustCache.cache.get(config.get('cacheKeys:concurrentActivityDownloadsPerJob'));
  const concurrencyConfig = config.get('download:concurrentActivityDownloadsPerJob');
  const concurrency = +concurrencyCache || concurrencyConfig;
  const options = {
    concurrency,
  };

  const activitiesWithMedia = activities.filter(item => ACTIVITIES_WITH_MEDIA.includes(item.action));

  logger.info({
    file: __filename,
    event: 'saveMediaFromActivityFeedStart',
    data: {
      activities: activitiesWithMedia.length,
      userID,
      directory,
      includeVideos,
      includeImages,
      concurrentActivityDownloadsPerJob: concurrency,
    },
  });

  await Promise.map(
    activitiesWithMedia,
    activity => internals.saveMediaFromActivity(activity, directory, userID, includeVideos, includeImages),
    options
  );

  logger.info({
    file: __filename,
    event: 'saveMediaFromActivityFeed',
    data: {
      activities: activities.length,
      userID,
      directory,
      includeVideos,
      includeImages,
      concurrentActivityDownloadsPerJob: concurrency,
    },
  });
};

/**
 * For a single activity feed entry, save doorbell videos and images to local disk.
 *
 * @param {Array} activity - The activity feed entry.
 * @param {string} directory - Save the media to this directory.
 * @param {string} userID -
 * @param {boolean} includeVideos - If true, include videos in the download.
 * @param {boolean} includeImages - If true, include images in the download.
 * @returns {Promise.<undefined>} -
 */
internals.saveMediaFromActivity = async (activity, directory, userID, includeVideos, includeImages) => {
  const houseName = entities.unescape(_.get(activity, 'house.houseName'));
  const houseID = _.get(activity, 'house.houseID');
  const doorbellName = entities.unescape(_.get(activity, 'deviceName'));
  const doorbellID = _.get(activity, 'deviceID');
  const time = new Date(_.get(activity, 'dateTime')).toISOString();
  const dvrID = _.get(activity, 'info.dvrID');
  const imagePreviewURL = _.get(activity, 'info.image.secure_url') || _.get(activity, 'info.image');

  const s3Params = {
    Bucket: config.get('s3:videoBucket'),
    Prefix: `${houseID}/${doorbellID}/${dvrID}`,
  };
  let s3Objects;
  let imagePath;

  const concurrencyCache = await augustCache.cache.get(config.get('cacheKeys:concurrentDownloadsPerActivity'));
  const concurrencyConfig = config.get('download:concurrentDownloadsPerActivity');
  const concurrency = +concurrencyCache || concurrencyConfig;

  const localDirectory = `${directory}/${houseName}/${doorbellName}/${time}`;
  await internals.mkdirp(localDirectory);

  try {
    if (includeVideos) {
      s3Objects = await s3Util.listObjects(s3Params);

      await Promise.map(
        s3Objects.Contents,
        async object => {
          await internals.downloadFromS3(config.get('s3:videoBucket'), object.Key, localDirectory);
        },
        { concurrency }
      );
    }

    if (includeImages && imagePreviewURL) {
      imagePath = `${localDirectory}/${config.get('download:videoPreviewImageName')}${path.extname(imagePreviewURL)}`;
      await util.downloadFile(imagePreviewURL, imagePath);
    }

    logger.info({
      file: __filename,
      event: 'saveMediaFromActivity',
      data: {
        activity,
        directory,
        userID,
        includeVideos,
        includeImages,
        imagePath,
        s3Objects: _.get(s3Objects, 'Contents', []).length,
        concurrentDownloadsPerActivity: concurrency,
      },
    });
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'saveMediaFromActivityError',
      error: err,
      data: {
        activity,
        directory,
        userID,
        includeVideos,
        includeImages,
        imagePath,
        s3Objects: _.get(s3Objects, 'Contents', []).length,
        concurrentDownloadsPerActivity: concurrency,
      },
    });
  }
};

/**
 * This is called when a user requests a copy of their data.
 *
 * @async
 * @param {Object} data -
 * @param {string} data.localDirectory - Use this as a temporary working directory.
 * @param {string} data.accessToken - The user's acs access token.
 * @param {string} data.userID - The user's ID.
 * @param {string} data.brand - August / Yale / ...
 * @param {string} data.userDownload - The document from the usersdownloads collection.
 * @param {boolean} data.includeActivity - If true, include activity feed in the download.
 * @param {boolean} data.includeVideos - If true, include videos in the download.
 * @param {boolean} data.includeImages - If true, include images in the download.
 * @returns {Promise<undefined>} -
 */
internals.processDownloadJob = async data => {
  const brandProperties = constants.Brands.getBrand(data.brand);
  const brandName = brandProperties.brandName;
  const fromEmail = brandProperties.fromEmail;

  try {
    statsdClient.gaugeDelta('downloads.processing', 1);

    await model.usersDownloads.update(data.userDownload._id, {
      status: constants.UserDownloadStatuses.PROCESSING,
    });

    // Create local temp directory
    await internals.createLocalDirectory(data.localDirectory);

    // Get activity feed
    const activityFeed = await internals.getActivityFeed(data.accessToken, data.brand);

    // Save activity feed
    if (data.includeActivity) {
      await internals.saveActivityFeed(activityFeed, data.localDirectory);
    }

    const userImages = await internals.getUserImages(data.accessToken, data.brand);
    await internals.saveUserImages(userImages, data.localDirectory);

    // Get and save doorbell media
    if (config.get('download:saveDoorbellMedia') && (data.includeVideos || data.includeImages)) {
      await internals.saveMediaFromActivityFeed(
        activityFeed,
        data.localDirectory,
        data.userID,
        data.includeVideos,
        data.includeImages
      );
    }

    // Compress
    const compressFilePath = await internals.compressUserFiles(data.localDirectory);

    // Upload to S3
    const s3result = await internals.uploadToS3(compressFilePath, data.userID);

    // Get a signed URL
    const url = await s3Util.getSignedUrl(S3_BUCKET, s3result.Key, config.get('download:signedUrlExpirationInSeconds'));

    // Update the database
    await model.usersDownloads.update(data.userDownload._id, {
      url,
      s3key: s3result.Key,
      status: constants.UserDownloadStatuses.AVAILABLE,
    });

    // TODO: Use Cloudfront

    // Send email to user
    const user = await model.users.findByID(data.userID);
    const emails = await util.getUserEmails(data.userID);
    const templateName =
      data.brand === constants.Brands.YALE ? 'user-download-processed-yale.html' : 'user-download-processed.html';
    const firstName = _.get(user, 'FirstName');
    const emailData = {
      from: fromEmail,
      subject: babelfish.t('downloadProcessed', { user, brand: data.brand }),
      event: 'user_download_processed',
      template: {
        name: templateName,
        data: {
          name: entities.unescape(firstName),
          brandName,
          brand: data.brand,
        },
      },
    };
    await util.sendEmailToUser(data.userID, emails, emailData);

    logger.info({
      file: __filename,
      event: 'processDownloadJob',
      data: {
        userID: data.userID,
        brand: data.brand,
        brandName,
        firstName,
      },
    });
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'processDownloadJobError',
      error: err,
      data: {
        userID: data.userID,
      },
    });

    await model.usersDownloads.update(data.userDownload._id, {
      status: constants.UserDownloadStatuses.FAILED,
    });

    throw err;
  } finally {
    statsdClient.gaugeDelta('downloads.processing', -1);
    await internals.deleteLocalDirectory(data.localDirectory);
  }
};

/**
 * Download an object from S3.
 *
 * @param {string} bucket - S3 bucket.
 * @param {string} key - Key of the file within the S3 bucket.
 * @param {string} directory - Save the object to this directory.
 * @returns {Promise.<undefined|Error>} Resolves when the object has been downloaded.
 */
internals.downloadFromS3 = (bucket, key, directory) => {
  return new Promise((resolve, reject) => {
    const destPath = `${directory}/${path.basename(key)}`;
    const params = {
      Bucket: bucket,
      Key: key,
    };
    const s3Stream = s3Util.getObject(params).createReadStream();
    const fileStream = fs.createWriteStream(destPath);
    s3Stream.on('error', reject);
    fileStream.on('error', reject);
    fileStream.on('close', () => {
      resolve();
    });
    s3Stream.pipe(fileStream);
  });
};

/**
 * Create a new job queue for processing user download requests.
 *
 * @returns {Object} The job queue.
 */
async function createDownloadQueue() {
  const redisConfig = config.get('redis');

  let bullConfig = {
    redis: {
      host: _.get(redisConfig, 'connection.host'),
      port: _.get(redisConfig, 'connection.port'),
    },
  };
  if (!bullConfig.redis.host || !bullConfig.redis.port) {
    if (typeof redisConfig.connection !== 'string') {
      throw new Error(`bad configuration values for "redis": ${redisConfig}`);
    }
    const [host, port, extraCrap] = redisConfig.connection.split(':');
    if (!host || !port || extraCrap) {
      throw new Error(`improper format for "redis" ${redisConfig.connection}`);
    }
    bullConfig.redis.host = host;
    bullConfig.redis.port = port;
  }

  const downloadQueue = new Bull('download queue', bullConfig);

  const concurrencyCache = await augustCache.cache.get(config.get('cacheKeys:concurrentUserDownloadJobs'));
  const concurrencyConfig = config.get('download:concurrentUserDownloadJobs');
  const concurrency = +concurrencyCache || concurrencyConfig;

  downloadQueue.process(concurrency, async job => {
    await internals.processDownloadJob(job.data);
  });

  // A Job is waiting to be processed as soon as a worker is idling.
  downloadQueue.on('waiting', async jobID => {
    const jobCounts = await updateDownloadQueueStatsd(downloadQueue);

    logger.info({
      file: __filename,
      event: 'jobQueueWaiting',
      data: {
        jobID,
        jobCountsStr: JSON.stringify(jobCounts),
      },
    });
  });

  // A job has started.
  downloadQueue.on('active', async job => {
    const jobCounts = await updateDownloadQueueStatsd(downloadQueue);

    logger.info({
      file: __filename,
      event: 'jobQueueActive',
      data: {
        jobID: job.id,
        jobCountsStr: JSON.stringify(jobCounts),
      },
    });
  });

  // An error occured.
  downloadQueue.on('error', async err => {
    const jobCounts = await updateDownloadQueueStatsd(downloadQueue);

    logger.error({
      file: __filename,
      event: 'jobQueueError',
      error: err,
      data: {
        jobCountsStr: JSON.stringify(jobCounts),
      },
    });
  });

  // A job failed.
  downloadQueue.on('failed', async (job, err) => {
    const jobCounts = await updateDownloadQueueStatsd(downloadQueue);

    const downloadID = _.get(job, 'data.userDownload._id');
    if (downloadID) {
      try {
        await model.usersDownloads.update(downloadID, {
          status: constants.UserDownloadStatuses.FAILED,
        });
      } catch (err) {
        logger.error({
          file: __filename,
          event: 'jobQueueFailedDatabaseError',
          error: err,
          data: {
            jobID: job.id,
            jobCountsStr: JSON.stringify(jobCounts),
          },
        });
      }
    }

    logger.error({
      file: __filename,
      event: 'jobQueueFailedError',
      error: err,
      data: {
        jobID: job.id,
        jobCountsStr: JSON.stringify(jobCounts),
      },
    });
  });

  // A job has been marked as stalled.
  downloadQueue.on('stalled', async job => {
    const jobCounts = await updateDownloadQueueStatsd(downloadQueue);

    logger.error({
      file: __filename,
      event: 'jobQueueStalledError',
      error: new Error('job stalled'),
      data: {
        jobID: job.id,
        jobCountsStr: JSON.stringify(jobCounts),
      },
    });
  });

  // A job successfully completed.
  downloadQueue.on('completed', async job => {
    const jobCounts = await updateDownloadQueueStatsd(downloadQueue);

    logger.info({
      file: __filename,
      event: 'jobQueueCompleted',
      data: {
        jobID: job.id,
        jobCountsStr: JSON.stringify(jobCounts),
      },
    });
  });

  downloadQueue.on('cleaned', async (jobs, type) => {
    logger.info({
      file: __filename,
      event: 'jobQueueCleaned',
      data: {
        jobCounts: jobs.length,
        type,
      },
    });
  });

  logger.info({
    file: __filename,
    event: 'createDownloadQueue',
    data: {
      bullConfig,
      concurrentUserDownloadJobs: concurrency,
    },
  });

  return downloadQueue;
}

/**
 * Send data to statsd about the state of the download queue (e.g. active jobs, waiting jobs, failed jobs)
 *
 * @async
 * @param {Object} downloadQueue - The bull queue
 * @returns {Promise<Object>} Returns the number of active and waiting jobs.
 */
async function updateDownloadQueueStatsd(downloadQueue) {
  // Clean the old failed jobs from queue so that the statsD only reflects the recent jobs
  downloadQueue.clean(config.get('download:failedExpirationInMS'), 'failed');
  const jobCounts = await downloadQueue.getJobCounts();

  statsdClient.gauge('downloads.queue.waiting', jobCounts.waiting);
  statsdClient.gauge('downloads.queue.active', jobCounts.active);
  statsdClient.gauge('downloads.queue.completed', jobCounts.completed);
  statsdClient.gauge('downloads.queue.failed', jobCounts.failed);
  statsdClient.gauge('downloads.queue.delayed', jobCounts.delayed);

  return jobCounts;
}
