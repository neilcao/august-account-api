const extract = require('august-extract');
const logger = require('august-logger');
const statsdClient = require('august-statsd').statsdClient;

/**
 * Confirm the identifier in the req.body and spilt it to
 * identifierType and identifierValue
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 *
 * @return {void}
 */
exports.validateIdentifier = (req, res, next) => {
  try {
    const identifier = extract.external(req, 'body.identifier');
    const [identifierType, identifierValue] = identifier.split(':');

    req.set('identifier', identifier);
    req.set('identifierType', identifierType);
    req.set('identifierValue', identifierValue);

    return next();
  } catch (error) {
    logger.error({
      file: __filename,
      event: 'validateIdentifierError',
      error: error,
    });
    statsdClient.increment('validateIdentifier.failure');

    return next(error);
  }
};

/**
 * Confirm the identifier in the req.body and spilt it to
 * identifierType and identifierValue
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 *
 * @return {void}
 */
exports.validateIdentifierTypeAndValue = (req, res, next) => {
  try {
    const identifierValue = extract.external(req, 'body.identifier');
    const identifierType = extract.external(req, 'body.identifierType');

    req.set('identifierType', identifierType);
    req.set('identifierValue', identifierValue);

    return next();
  } catch (error) {
    logger.error({
      file: __filename,
      event: 'validateIdentifierTypeAndValueError',
      error: error,
    });
    statsdClient.increment('validateIdentifierTypeAndValue.failure');

    return next(error);
  }
};
