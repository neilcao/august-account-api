'use strict';

const errors = require('restify-errors');

const logger = require('august-logger');
const AugustRatelimiter = require('august-ratelimiter');
const statsd = require('august-statsd').statsdClient;

const config = require('../config/index.js');

/**
 * Process a log from the frontend.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.processLog = async (req, res, next) => {
  const userID = req.get('userID');
  const type = req.body.type;
  const component = req.body.component;
  const event = req.body.event;
  const data = req.body.data || {};

  try {
    const limiter = await AugustRatelimiter.check(
      `account_api_log_count:${userID}`,
      config.get('limits:logging:limit'),
      config.get('limits:logging:timeframeInMS') / 1000
    );

    if (limiter.limiting) {
      const err = new errors.TooManyRequestsError('rate limited');

      logger.warn({
        file: __filename,
        event: 'processLogRateLimited',
        message: err.message,
        data: {
          userID,
          type,
          event,
          component,
        },
      });
      statsd.increment('log.failure');

      res.header('Retry-After', config.get('limits:logging:timeframeInMS') / 1000);

      return next(err);
    }

    const options = {
      file: __filename,
      event: 'processLog',
      data: {
        userID,
        component,
        event,
        ...data,
      },
    };
    if (type === 'error') {
      options.error = new Error(req.body.error);
    }

    logger[type](options);

    statsd.increment('log.success');
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'processLogError',
      error: err,
      data: {
        userID,
        type,
        event,
        component,
      },
    });
    statsd.increment('log.failure');

    return next(err);
  }
};
