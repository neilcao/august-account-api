'use strict';

const logger = require('august-logger');
const model = require('august-model');

/**
 * Gets a list of lock IDs for which callingUser is the super user
 * and adds them req.params.superUserLocks
 *
 * @param {restify/Request} req - request
 * @param {restify/Response} res - response
 * @param {Function} next - next
 * @return {undefined}
 */
exports.getSuperUserLocks = async function getSuperUserLocks(req, res, next) {
  let userID = req.get('userID');

  try {
    const superUserLockIDs = await model.lockSuperUsers.findAllLocksByUserID(userID);
    req.set('superUserLocks', superUserLockIDs);

    return next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getSuperUserLocksError',
      error: err,
      data: {
        userID: userID,
      },
    });

    return next(err);
  }
};
