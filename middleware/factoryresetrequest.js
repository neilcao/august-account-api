'use strict';

const restifyErrors = require('restify-errors');

const _ = require('lodash');

const logger = require('august-logger');
const messages = require('august-messages');
const model = require('august-model');
const statsdClient = require('august-statsd').statsdClient;

const acs = require('../acs.js');

/**
 * Gets the factory reset requests for the callingUserID
 * Gets both the requests for which the callingUser is an owner of the lock
 * and the requests for which the callingUser has created a request
 *
 *
 * @param {http.IncomingMessage} req - request object
 * @param {http.ServerResponse} res - response object
 * @param {Function} next - next callback
 * @return {undefined}
 */
module.exports.getAllFactoryResetRequests = async function getAllFactoryResetRequests(req, res, next) {
  const callingUserID = req.get('userID');
  const lockSuperUsers = req.get('superUserLocks');
  let factoryResetRequests;

  try {
    const locks = await model.locks.findByLockIDList(lockSuperUsers, { serialNumber: 1, LockName: 1 });
    const lockNameMap = locks.reduce(function(acc, lock) {
      acc[lock.serialNumber] = lock.LockName;

      return acc;
    }, {});

    const lockSerials = locks.map(lock => lock.serialNumber);

    const lockOwnerFactoryResetRequests = await model.factoryResetRequest.findAllBySerialNumber(lockSerials);
    lockOwnerFactoryResetRequests.map(request => _.set(request, 'LockName', lockNameMap[request.serialNumber]));

    const callingUserFactoryResetRequests = await model.factoryResetRequest.findAllByUserID(callingUserID);

    factoryResetRequests = lockOwnerFactoryResetRequests.concat(callingUserFactoryResetRequests);
    factoryResetRequests.sort(function(a, b) {
      return b.createdAt - a.createdAt;
    });
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getAllFactoryResetRequestsError',
      error: err,
      data: {
        callingUserID,
      },
    });

    return next(err);
  }

  res.send({ factoryResetRequests });
  logger.info({
    file: __filename,
    event: 'getAllFactoryResetRequestsSuccess',
    data: {
      callingUserID,
      totalFactoryResetRequests: factoryResetRequests.length,
    },
  });

  return next();
};

/**
 * Processes an approval/denial of a factory reset request.
 *
 * @param {restify/Request} req -
 * @param {restify/Response} res -
 * @param {Function} next -
 * @return {Promise.<undefined>} -
 */
module.exports.process = async function process(req, res, next) {
  const brand = req.get('brand');
  let callingUserID;
  let serialNumber;
  let lockID;
  let approved;
  let requestID;

  try {
    callingUserID = req.get('userID');
    lockID = req.get('lockID');
    serialNumber = req.get('serialNumber');

    approved = _.get(req, 'body.approved');
    requestID = _.get(req, 'body.requestID');

    if (_.isUndefined(approved) || _.isUndefined(requestID) || !_.isBoolean(approved)) {
      throw new restifyErrors.BadRequestError('Invalid body');
    }
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'processFactoryResetRequestParamsError',
      error: err,
      data: {
        callingUserID,
        lockID,
        serialNumber,
        requestID,
      },
    });

    return next(err);
  }

  try {
    const [factoryResetRequest, lock] = await Promise.all([
      model.factoryResetRequest.findByID(requestID),
      model.locks.findByLockID(lockID, { LockID: 1, appBrand: 1 }),
    ]);
    if (!factoryResetRequest) {
      throw new restifyErrors.ResourceNotFoundError(`Unable to find factory reset request ${requestID}`);
    }

    if (approved) {
      await acs.factoryResetLock(req.get('acsAccessToken'), brand, lockID);

      const data = {
        resetRequestID: requestID,
        requestingUserID: factoryResetRequest.requestingUserID,
        updatingUserID: callingUserID,
        serialNumber,
        lockID,
        lock,
      };
      const context = await messages.publish('event.resetrequest.approved', data);
      logger.info({
        file: __filename,
        event: 'factoryResetRequestProcessApproved',
        data: {
          messageKey: context.key,
          messageHistory: context.history,
          callingUserID,
          serialNumber,
          lockID,
          requestID,
        },
      });

      return next();
    }

    // Factory reset request denied.
    const data = {
      resetRequestID: requestID,
      requestingUserID: factoryResetRequest.requestingUserID,
      updatingUserID: callingUserID,
      serialNumber,
      lockID,
    };
    const context = await messages.publish('event.resetrequest.denied', data);

    statsdClient.increment('acs.factory-reset-request-denied.success');
    logger.info({
      file: __filename,
      event: 'factoryResetRequestProcessDenied',
      data: {
        messageKey: context.key,
        messageHistory: context.history,
        callingUserID,
        serialNumber,
        lockID,
        requestID,
      },
    });

    return next();
  } catch (err) {
    statsdClient.increment('acs.factory-reset-request.process.failure');
    logger.error({
      file: __filename,
      event: 'factoryResetRequestProcessError',
      error: err,
      data: {
        callingUserID,
        serialNumber,
        lockID,
        requestID,
      },
    });

    return next(err);
  }
};
