'use strict';

const _ = require('lodash');

const constants = require('august-constants');
const AccessToken = require('august-access-token');
const babelfish = require('august-babelfish');
const entities = require('august-html-entities');
const keys = require('august-keys').Keys();
const logger = require('august-logger');
const model = require('august-model');
const statsdClient = require('august-statsd').statsdClient;

const acs = require('../acs.js');
const config = require('../config');
const jsUtil = require('august-js-utility');
const session = require('./session.js');
const util = require('../util.js');

let _encryptionPassword;

const getEncryptionPassword = () => {
  if (!_encryptionPassword) {
    _encryptionPassword = keys.getServiceKeys('august-account-api').encryptionPassword;
  }

  return _encryptionPassword;
};

module.exports.test = {
  getEncryptionPassword,
};

/**
 * Get user info.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.getUser = async (req, res, next) => {
  try {
    const userID = req.get('userID');
    const acsAccessToken = req.get('acsAccessToken');
    const brand = req.get('brand');
    const [
      userResult,
      appsResult,
      locksResult,
      doorbellsResult,
      emailIdentifiers,
      phoneIdentifiers,
    ] = await Promise.all([
      acs.getUser(acsAccessToken, brand),
      acs.getUserApps(acsAccessToken, brand),
      acs.getUserLocks(acsAccessToken, brand),
      acs.getUserDoorbells(acsAccessToken, brand),
      model.userIdentifiers.findAllByUserIDAndType(userID, 'email'),
      model.userIdentifiers.findAllByUserIDAndType(userID, 'phone'),
    ]);
    const emails = emailIdentifiers.map(item => model.userIdentifiers.getValue(item._value));
    const phones = phoneIdentifiers.map(item => model.userIdentifiers.getValue(item._value));
    const lastOwnerLocks = await getLastOwnerLocks(_.get(locksResult, 'data', {}), userID);
    const lastOwnerDoorbells = await getLastOwnerDoorbells(_.get(doorbellsResult, 'data', {}), userID);
    const info = _.get(userResult, 'data', {});
    ['FirstName', 'LastName'].forEach(key => (info[key] = entities.unescape(info[key])));

    logger.info({
      file: __filename,
      event: 'getUser',
      data: {
        userID: req.get('userID'),
      },
    });
    statsdClient.increment('user.get.success');

    const bodyToSend = {
      info,
      apps: _.get(appsResult, 'data', []).map(item => entities.unescape(item.name)),
      emails,
      phones,
      lastOwnerLocks,
      lastOwnerDoorbells,
    };

    res.send(bodyToSend);
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'getUserError',
      error: err,
      data: {
        userID: req.get('userID'),
      },
    });
    statsdClient.increment('user.get.failure');

    return next(err);
  }
};

/**
 * Update user info.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.putUser = async (req, res, next) => {
  try {
    const acsResult = await acs.putUser(req.get('acsAccessToken'), req.body, req.get('brand'));
    const tokenEncoded = acsResult.headers[config.get('acsAccessTokenHeader')];
    const returnedToken = new AccessToken();
    returnedToken.parseJWT(tokenEncoded);

    let accountManagementToken;
    if (returnedToken.UserID) {
      accountManagementToken = returnedToken.UserID;
    } else if (returnedToken.email.length) {
      accountManagementToken = returnedToken.email[0].split(':')[1];
    }

    const newTokenExpiration = new Date(Date.now() + config.get('tokenExpirationTimeInMS'));
    session.setTokenExpiration(returnedToken, newTokenExpiration);

    const refreshToken = null;
    await model.usersTokens.upsert(
      accountManagementToken,
      `${config.get('userTokenPrefix')}:${returnedToken.toJWT()}`,
      newTokenExpiration.getTime(),
      refreshToken,
      newTokenExpiration
    );

    logger.info({
      file: __filename,
      event: 'putUser',
      data: {
        userID: req.get('userID'),
      },
    });
    statsdClient.increment('user.put.success');

    res.send(jsUtil.crypto.encrypt(accountManagementToken, getEncryptionPassword()));
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'putUserError',
      error: err,
      data: {
        userID: req.get('userID'),
      },
    });
    statsdClient.increment('user.put.failure');

    return next(err);
  }
};

/**
 * Delete the user's account.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.deleteUser = async (req, res, next) => {
  const userID = req.get('userID');
  const brand = req.query.brand || constants.Brands.AUGUST;

  try {
    const user = await model.users.findByID(userID);
    const emails = await util.getUserEmails(userID);
    const emailData = {
      from: config.get('email'),
      subject: babelfish.t('deleteUserEmailSubject', { user, brand }),
      event: 'user_deleted',
      template: {
        name: 'user-deleted.html',
        data: {
          name: entities.unescape(user.FirstName),
        },
      },
    };

    await acs.deleteUser(req.get('acsAccessToken'), brand);
    await util.sendEmailToUser(userID, emails, emailData);

    logger.info({
      file: __filename,
      event: 'deleteUser',
      data: {
        userID,
      },
    });
    statsdClient.increment('user.delete.success');

    res.send();
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'deleteUserError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('user.delete.failure');

    return next(err);
  }
};

/**
 * Log the user out of their mobile apps.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.appLogout = async (req, res, next) => {
  const userID = req.get('userID');
  const accessToken = req.get('acsAccessToken');
  const brand = req.get('brand');

  try {
    const result = await acs.getUser(accessToken, brand);
    const user = _.get(result, 'data', {});
    await acs.userAppLogout(accessToken, brand, user);

    logger.info({
      file: __filename,
      event: 'appLogout',
      data: {
        userID,
      },
    });
    statsdClient.increment('user.app-logout.success');

    res.send();
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'appLogoutError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('user.app-logout.failure');

    return next(err);
  }
};

/**
 * Returns true if the user is the last owner on the lock.
 *
 * @param {string} userID -
 * @param {string} lockID -
 * @returns {boolean} Returns true or false.
 */
async function isUserLastLockOwner(userID, lockID) {
  const lockSuperUserIDs = await model.lockSuperUsers.findAllUsersByLockID(lockID);

  return lockSuperUserIDs.length === 1 && lockSuperUserIDs[0] === userID;
}

/**
 * Returns true if the user is the last owner on the doorbell.
 *
 * @param {string} userID -
 * @param {string} doorbellID -
 * @returns {boolean} Returns true or false.
 */
async function isUserLastDoorbellOwner(userID, doorbellID) {
  const doorbellSuperUserIDs = await model.doorbellUsers.findDoorbellUsers(doorbellID);

  return doorbellSuperUserIDs.length === 1 && doorbellSuperUserIDs[0] === userID;
}

/**
 * Filter the list of locks to only those where the user is the last owner.
 *
 * @async
 * @param {Object} locksData -
 * @param {string} userID -
 * @returns {Array} The list of locks.
 */
async function getLastOwnerLocks(locksData, userID) {
  const lockIDs = Object.keys(locksData);
  const locks = Object.values(locksData);
  const lastOwnerLocks = [];

  for (let i = 0; i < lockIDs.length; i++) {
    let lockID = lockIDs[i];
    let lock = locks[i];

    if (await isUserLastLockOwner(userID, lockID)) {
      lastOwnerLocks.push({
        name: entities.unescape(lock.LockName),
        house: entities.unescape(lock.HouseName),
      });
    }
  }

  return lastOwnerLocks;
}

/**
 * Filter the list of doorbells to only those where the user is the last owner.
 *
 * @async
 * @param {Object} doorbellsData -
 * @param {string} userID -
 * @returns {Array} The list of doorbells.
 */
async function getLastOwnerDoorbells(doorbellsData, userID) {
  const doorbellIDs = Object.keys(doorbellsData);
  const doorbells = Object.values(doorbellsData);
  const lastOwnerDoorbells = [];

  for (let i = 0; i < doorbellIDs.length; i++) {
    let doorbellID = doorbellIDs[i];
    let doorbell = doorbells[i];

    if (await isUserLastDoorbellOwner(userID, doorbellID)) {
      lastOwnerDoorbells.push({
        name: entities.unescape(doorbell.name),
        house: entities.unescape(doorbell.HouseName),
      });
    }
  }

  return lastOwnerDoorbells;
}
