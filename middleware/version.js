'use strict';

const packageFile = require('../package.json');

/**
 * Get info about the server's version.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.getVersion = async (req, res, next) => {
  res.send({
    env: process.env.NODE_ENV,
    version: packageFile.version,
  });
  next();
};
