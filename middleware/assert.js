'use strict';

const restifyErrors = require('restify-errors');

const extract = require('august-extract');
const logger = require('august-logger');
const model = require('august-model');

/**
 * Confirms that the calling user is a manager of the lock.  If not, it returns
 * a not authorized error.  Both user and lock must be validated before this is called.
 *
 * @param {restify/Request} req -
 * @param {restify/Response} res -
 * @param {Function} next -
 * @return {undefined}
 */
exports.assertLockSuperUser = async function assertLockSuperUser(req, res, next) {
  let serialNumber;
  let callingUserID;
  let lockID;

  try {
    serialNumber = extract.external(req, 'body.serialNumber');
    callingUserID = req.get('userID');
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'assertLockSuperUserParamError',
      error: err,
    });

    return next(err);
  }

  try {
    lockID = ((await model.locks.findBySerialNumber(serialNumber, { LockID: 1 })) || {}).LockID;
    const lockSuperUser = await model.lockSuperUsers.findByLockIDAndUserID(lockID, callingUserID);
    if (!lockSuperUser) {
      throw new restifyErrors.NotAuthorizedError(`user ${callingUserID} is not a superuser of lock ${lockID}`);
    }

    req.set('lockID', lockID);
    req.set('serialNumber', serialNumber);

    return next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'assertLockSuperUserError',
      error: err,
      data: {
        lockID,
        callingUserID,
      },
    });

    next(err);
  }
};
