'use strict';

const _ = require('lodash');
const errors = require('restify-errors');

const AccessToken = require('august-access-token');
const AugustRatelimiter = require('august-ratelimiter');
const constants = require('august-constants');
const extract = require('august-extract');
const jsUtil = require('august-js-utility');
const keys = require('august-keys').Keys();
const logger = require('august-logger');
const model = require('august-model');

const acs = require('../acs.js');
const augustCache = require('../cache.js');
const config = require('../config');
const statsdClient = require('august-statsd').statsdClient;

const accessTokenHeader = config.get('accessTokenHeader');
const acsAccessTokenHeader = config.get('acsAccessTokenHeader');

let _encryptionPassword;

const getEncryptionPassword = () => {
  if (!_encryptionPassword) {
    _encryptionPassword = keys.getServiceKeys('august-account-api').encryptionPassword;
  }

  return _encryptionPassword;
};

module.exports.test = {
  getEncryptionPassword,
};

const userTokenPrefix = config.get('userTokenPrefix');

/**
 * Attempt a email/phone and password login on rest api.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.login = async (req, res, next) => {
  let identifier;
  let identifierType;
  let identifierValue;
  let password;
  let brand;

  try {
    identifier = extract.external(req, 'body.identifier');
    [identifierType, identifierValue] = identifier.split(':');
    password = extract.external(req, 'body.password');
    brand = _.get(req, `headers.${config.get('brandingHeader')}`, constants.Brands.AUGUST);
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'loginParamError',
      error: err,
    });
    statsdClient.increment('login.failure');

    return next(err);
  }

  try {
    const limiter = await AugustRatelimiter.check(
      `account_api_login_attempts:${identifierValue}`,
      config.get('limits:login:limit'),
      config.get('limits:login:timeframeInMS') / 1000
    );

    if (limiter.limiting) {
      const err = new errors.TooManyRequestsError('rate limited');

      logger.warn({
        file: __filename,
        event: 'sessionRateLimitedError',
        message: err.message,
        data: {
          identifierType,
          identifierValue,
        },
      });
      statsdClient.increment('login.failure');

      res.header('Retry-After', config.get('limits:login:timeframeInMS') / 1000);

      return next(err);
    }

    const loginResult = await acs.login(identifier, password, brand);

    const tokenEncoded = _.get(loginResult, `headers["${acsAccessTokenHeader}"]`);
    const token = new AccessToken();
    token.parseJWT(tokenEncoded);

    // TODO: Remove this check when we're ready to go live.
    if (config.get('useLoginWhitelist')) {
      const allowedUserIDs = await augustCache.cache.lrange(config.get('cacheKeys:loginWhitelist'), 0, -1);
      const identifiers = await model.userIdentifiers.findAllByUserIDAndType(token.UserID, 'email');
      const userEmails = identifiers.map(item => model.userIdentifiers.getValue(item._value));
      const allowedEmailRegex = /@bishopfoxemail\.com|@august\.com$/i;
      const emailAllowed = userEmails.some(item => allowedEmailRegex.test(item));

      if (!allowedUserIDs.includes(token.UserID) && !emailAllowed) {
        const err = new errors.NotAuthorizedError(`User ID ${token.UserID} is not on the login whitelist`);

        logger.error({
          file: __filename,
          event: 'loginWhitelistError',
          error: err,
          data: {
            identifierType,
            identifierValue,
            userID: token.UserID,
          },
        });
        statsdClient.increment('login.failure');

        return next(err);
      }
    }

    validateToken(token, { minFactors: 1 });

    const newTokenExpiration = new Date(Date.now() + config.get('tokenExpirationTimeInMS'));
    setTokenExpiration(token, newTokenExpiration.toISOString());

    const refreshToken = null;

    await model.usersTokens.upsert(
      token.UserID,
      `${userTokenPrefix}:${token.toJWT()}`,
      newTokenExpiration.getTime(),
      refreshToken,
      newTokenExpiration
    );

    await acs.sendVerificationCode(identifierType, identifierValue, token.toJWT(), brand);

    logger.info({
      file: __filename,
      event: 'login',
      data: {
        identifierType,
        identifierValue,
        userID: token.UserID,
      },
    });
    statsdClient.increment('login.success');

    const tokenToSend = jsUtil.crypto.encrypt(token.UserID, getEncryptionPassword());

    res.send(tokenToSend);
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'loginError',
      error: err,
      data: {
        identifierType,
        identifierValue,
      },
    });
    statsdClient.increment('login.failure');

    return next(err);
  }
};

/**
 * Logout the user.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.logout = async (req, res, next) => {
  let userID;
  let userIDEncrypted;

  try {
    userIDEncrypted = extract.external(req, `headers["${config.get('accessTokenHeader')}"]`);
    userID = jsUtil.crypto.decrypt(userIDEncrypted, getEncryptionPassword());
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'logoutParamError',
      error: err,
    });
    statsdClient.increment('logout.failure');

    return next(err);
  }

  try {
    await model.usersTokens.removeAccountManagementTokenByUserID(userID);

    logger.info({
      file: __filename,
      event: 'logout',
      data: {
        userID,
      },
    });
    statsdClient.increment('logout.success');

    res.send();
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'logoutError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('logout.failure');

    return next(err);
  }
};

/**
 * Extend a user's session.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.extendSession = async (req, res, next) => {
  let userID;
  let userIDEncrypted;

  try {
    userIDEncrypted = extract.external(req, `headers["${config.get('accessTokenHeader')}"]`);
    userID = jsUtil.crypto.decrypt(userIDEncrypted, getEncryptionPassword());
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'extendSessionParamError',
      error: err,
    });
    statsdClient.increment('extend-session.failure');

    return next(err);
  }

  try {
    const userToken = await model.usersTokens.findByUserIDAndValueType(userID, userTokenPrefix);
    if (!userToken) {
      const err = new errors.InvalidCredentialsError('invalid account management token');

      logger.error({
        file: __filename,
        event: 'extendSessionCredentialsError',
        error: err,
        data: {
          userID,
          userTokenPrefix,
        },
      });
      statsdClient.increment('extend-session.failure');

      return next(err);
    }

    const newTokenExpiration = new Date(Date.now() + config.get('tokenExpirationTimeInMS'));
    const tokenEncoded = _.get(userToken, 'token', '').split(':')[1];
    const token = new AccessToken();
    token.parseJWT(tokenEncoded);

    setTokenExpiration(token, newTokenExpiration.toISOString());

    const refreshToken = null;
    await model.usersTokens.upsert(
      token.UserID,
      `${userTokenPrefix}:${token.toJWT()}`,
      newTokenExpiration.getTime(),
      refreshToken,
      newTokenExpiration
    );

    logger.info({
      file: __filename,
      event: 'extendSession',
      data: {
        userID,
        newTokenExpiration,
      },
    });
    statsdClient.increment('extend-session.success');

    res.send();
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'extendSessionError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('extend-session.failure');

    return next(err);
  }
};

/**
 * Validate a user's verification code.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.validateVerificationCode = async (req, res, next) => {
  let userID;
  let userIDEncrypted;
  let identifierType;
  let identifierValue;
  let code;
  let brand;

  try {
    userIDEncrypted = _.get(req, `headers["${accessTokenHeader}"]`);
    if (userIDEncrypted) {
      userID = jsUtil.crypto.decrypt(userIDEncrypted, getEncryptionPassword());
    }
    identifierType = extract.external(req, 'params.identifier');
    identifierValue = extract.external(req, `body.["${identifierType}"]`);
    code = extract.external(req, `body.code`);
    brand = _.get(req, `headers.${config.get('brandingHeader')}`, constants.Brands.AUGUST);
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'validateParamError',
      error: err,
    });
    statsdClient.increment('validate-code.failure');

    return next(err);
  }

  try {
    let acsAccessToken;
    const userToken = await model.usersTokens.findByUserIDAndValueType(userID, userTokenPrefix);
    if (userToken) {
      acsAccessToken = userToken.token.split(':')[1];
    } else {
      acsAccessToken = new AccessToken().toJWT();
    }

    const validateResult = await acs.validateVerificationCode(
      identifierType,
      identifierValue,
      code,
      acsAccessToken,
      brand
    );

    const tokenEncoded = _.get(validateResult, `headers["${acsAccessTokenHeader}"]`);
    const token = new AccessToken();
    token.parseJWT(tokenEncoded);

    validateToken(token, { minFactors: 1, skipPassword: true, skipUser: true });

    const newTokenExpiration = new Date(Date.now() + config.get('tokenExpirationTimeInMS'));
    setTokenExpiration(token, newTokenExpiration.toISOString());

    let accountManagementToken;
    if (token.UserID) {
      accountManagementToken = token.UserID;
    } else if (token.email.length) {
      accountManagementToken = token.email[0].split(':')[1];
    } else if (token.phone.length) {
      accountManagementToken = token.phone[0].split(':')[1];
    }

    const refreshToken = null;
    await model.usersTokens.upsert(
      accountManagementToken,
      `${userTokenPrefix}:${token.toJWT()}`,
      newTokenExpiration.getTime(),
      refreshToken,
      newTokenExpiration
    );

    logger.info({
      file: __filename,
      event: 'validate',
      data: {
        identifierType,
        identifierValue,
        accountManagementToken,
        userID: token.UserID,
      },
    });
    statsdClient.increment('validate-code.success');

    res.send(jsUtil.crypto.encrypt(accountManagementToken, getEncryptionPassword()));
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'validateError',
      error: err,
      data: {
        identifierType,
        identifierValue,
      },
    });
    statsdClient.increment('validate-code.failure');

    return next(err);
  }
};

/**
 * Send a verification code to a phone or email.
 * This is rate limited.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.sendVerificationCode = async (req, res, next) => {
  let identifier;
  let identifierType;
  let identifierValue;
  let brand;

  try {
    identifier = extract.external(req, 'body.identifier');
    [identifierType, identifierValue] = identifier.split(':');
    brand = _.get(req, `headers.${config.get('brandingHeader')}`, constants.Brands.AUGUST);
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'sendVerificationCodeParamError',
      error: err,
    });
    statsdClient.increment('send-code.failure');

    return next(err);
  }

  try {
    const limiter = await AugustRatelimiter.check(
      `account_api_verification_codes:${identifierValue}`,
      config.get('limits:codes:limit'),
      config.get('limits:codes:timeframeInMS') / 1000
    );

    if (limiter.limiting) {
      const err = new errors.TooManyRequestsError('rate limited');

      logger.warn({
        file: __filename,
        event: 'sendVerificationCodeRateLimitedError',
        message: err.message,
        data: {
          identifierType,
          identifierValue,
        },
      });
      statsdClient.increment('send-code.failure');

      res.header('Retry-After', config.get('limits:codes:timeframeInMS') / 1000);

      return next(err);
    }

    const acsAccessToken = new AccessToken().toJWT();
    await acs.sendVerificationCode(identifierType, identifierValue, acsAccessToken, brand);

    logger.info({
      file: __filename,
      event: 'sendVerificationCode',
      data: {
        identifierType,
        identifierValue,
      },
    });
    statsdClient.increment('send-code.success');

    res.send();
    next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'sendVerificationCodeError',
      error: err,
      data: {
        identifierType,
        identifierValue,
      },
    });
    statsdClient.increment('send-code.failure');

    return next(err);
  }
};

/**
 * Ensure that clients are passing a valid account management token.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {undefined}
 */
module.exports.authHandler = async (req, res, next) => {
  let userID;
  let userIDEncrypted;

  try {
    userIDEncrypted = extract.external(req, `headers["${config.get('accessTokenHeader')}"]`, 'InvalidCredentialsError');
    userID = jsUtil.crypto.decrypt(userIDEncrypted, getEncryptionPassword());
  } catch (err) {
    logger.error({
      event: 'authHandlerParamError',
      error: err,
    });
    statsdClient.increment('authenticate.failure');

    return next(err);
  }

  try {
    if (userID) {
      const userToken = await model.usersTokens.findByUserIDAndValueType(userID, userTokenPrefix);
      if (userToken) {
        const tokenEncoded = userToken.token.split(':')[1];
        const token = new AccessToken();
        token.parseJWT(tokenEncoded);

        validateToken(token, { minFactors: 2, skipPassword: true, skipUser: true });

        const brand = _.get(req, `headers.${config.get('brandingHeader')}`, constants.Brands.AUGUST);
        req.set('brand', brand);
        req.set('acsAccessToken', tokenEncoded);
        req.set('userID', userID);

        logger.info({
          file: __filename,
          event: 'authHandler',
          data: {
            userID,
          },
        });

        statsdClient.increment('authenticate.success');

        return next();
      }
    }
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'authHandlerError',
      error: err,
      data: {
        userID,
      },
    });
    statsdClient.increment('authenticate.failure');

    return next(err);
  }

  const err = new errors.InvalidCredentialsError('invalid account management token');

  logger.error({
    file: __filename,
    event: 'authHandlerCredentialsError',
    error: err,
    data: {
      userID,
    },
  });
  statsdClient.increment('authenticate.failure');

  return next(err);
};

/**
 * Make sure an acs access token is valid.
 *
 * @param {Object} token - An instance of august-access-token.
 * @param {Object} [options] -
 * @returns {undefined}
 * @throws {Error} Throws an error if the access token isn't valid.
 */
function validateToken(token, options = {}) {
  const genericError = 'Something went wrong.';
  let actualError;

  // We only care if password, email or phone is verified.
  // We don't care about verified install IDs.
  token._token.vInstallId = false;
  token._token.hasInstallId = false;

  if (!token.isValid()) {
    actualError = 'invalid access token';
  }

  if (!token.isCurrent()) {
    actualError = 'expired access token';
  }

  if (options.skipPassword !== true && !token.vPassword) {
    actualError = 'invalid identifier or password';
  }

  if (options.skipUser !== true && !token.UserID) {
    actualError = 'access token not associated with a user';
  }

  if (options.minFactors && token.numFactors() < options.minFactors) {
    actualError = `token has ${token.numFactors()} factors when it should have at least ${options.minFactors}`;
  }

  if (actualError) {
    logger.error({
      file: __filename,
      event: 'validateTokenError',
      error: new Error(actualError),
      data: {
        options,
      },
    });

    throw new errors.InvalidCredentialsError(genericError);
  }
}

/**
 * Set the expiration date of an acs access token.
 *
 * @param {Object} token - An instance of august-access-token.
 * @param {string} expiresAt - ISO string.
 * @returns {Object} The modified token.
 */
function setTokenExpiration(token, expiresAt) {
  token.expiresAt = expiresAt;
  token._token.exp = Math.floor(new Date(token.expiresAt).valueOf() / 1000);
}
module.exports.setTokenExpiration = setTokenExpiration;

/**
 * Validate a user's verification code.
 *
 * @async
 * @param {restify/Request} req - request object
 * @param {restify/Response} res - response object
 * @param {Function} next -
 * @returns {Promise<void>} -
 */
module.exports.validateFromLinkByToken = async (req, res, next) => {
  let code;
  let brand;

  let identifier;
  let identifierType;
  let identifierValue;
  let token;
  let encodedToken;

  try {
    ({ identifier, token, code, brand } = _.get(req, 'body', {}));
    [identifierType, identifierValue] = identifier.split(':');
    const newAccessToken = new AccessToken();
    newAccessToken.parseJWT(token);
    if (!newAccessToken.isValid()) {
      throw new errors.InvalidCredentialsError('invalid verification token.');
    }
    newAccessToken._token.apiKey = keys.getServiceKeys('august-account-api').acsApiKey;
    encodedToken = newAccessToken.toJWT();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'validateFromLinkByTokenGetParamError',
      error: err,
    });
    statsdClient.increment('validate-from-link.failure');

    return next(err);
  }

  try {
    const validateResult = await acs.validateVerificationCode(
      identifierType,
      identifierValue,
      code,
      encodedToken,
      brand,
      true
    );

    res.sendToBody = validateResult;

    logger.info({
      file: __filename,
      event: 'validateFromLinkSuccess',
      data: {
        identifier,
        brand,
      },
    });
    statsdClient.increment('validate-from-link.success');

    return next();
  } catch (err) {
    logger.error({
      file: __filename,
      event: 'validateFromLinkError',
      error: err,
      data: {
        identifier,
        brand,
        message: _.get(err, 'response.data.message'),
      },
    });
    statsdClient.increment('validate-from-link.failure');

    const errRes = err.response;
    if (errRes) {
      // transfer the acs status
      res.send(errRes.status, errRes.data);

      return next();
    }

    return next(err);
  }
};
