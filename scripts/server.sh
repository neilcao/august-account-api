#!/usr/bin/env bash

#
# starts the app
#

set -e
[ -z "$DEBUG" ] || set -x

cd "$(dirname "$0")/.."
APP_ROOT="$(pwd)"

# set NODE_ENV=local if not defined
test -z "$NODE_ENV" && NODE_ENV='local'

clear


echo "===> Starting server ..."
if [ $NODE_ENV = 'local' ]
then
    nodemon -i test server.js
else
    node server.js
fi




