'use strict';

// always load New Relic before any other modules
if (process.env.NEW_RELIC) {
  require('newrelic');
}

const Promise = require('bluebird').Promise;
const restify = require('restify');
const serverHealth = require('server-health');
const corsMiddleware = require('restify-cors-middleware');

const augustKeys = require('august-keys');
const AccessToken = require('august-access-token');
const AugustRatelimiter = require('august-ratelimiter');
const babelfish = require('august-babelfish');
const configLoader = require('august-config-loader');
const messages = require('august-messages');
const model = require('august-model');
const logger = require('august-logger');
const statsd = require('august-statsd');
const { AugustAPI } = require('august-restify-api');
const timers = require('august-timer');

const cacheModule = require('./cache');
const config = require('./config');
const packageInfo = require('./package.json');
const s3Util = require('./s3');
const downloadsMiddleware = require('./middleware/download');
const redactNameFromDeletedUsers = require('./redactNameFromDeletedUsers');

const port = config.get('port');

/**
 * init server
 * @return {Promise} Promise
 */
async function init() {
  await logger.init(config.get('logger'));
  await config.init();
  const augustKeysOptions = {
    requiredKeys: [
      'august.secret',
      'august-account-api.s3',
      'august-account-api.acsApiKey',
      'august-account-api.encryptionPassword',
    ],
  };
  await augustKeys.init(configLoader, augustKeysOptions);

  const cacheOptions = config.get('redis');
  const statdsOptions = config.get('statsdConfig');
  const modelOptions = {
    connections: config.get('db:connections'),
    inMemoryCache: {
      enabled: true,
      refreshPeriodMs: 600000, // 10 minute reload
    },
  };
  const messagesOptions = {
    ...config.get('messages'),
    statsdClient: statsd.factory({ prefix: 'msg' }),
    loggerInstance: logger,
  };

  const babelfishOptions = {
    translationsPath: packageInfo.name,
  };

  const [cache] = await Promise.all([
    cacheModule.init(cacheOptions),
    model.init(modelOptions),
    statsd.init(statdsOptions),
    messages.init(messagesOptions),
    babelfish.init(babelfishOptions),
    AccessToken.init(augustKeys.Keys().august.secret),
    // fs.mkdirp(config.get('download:localDirectoryBasePath')),
  ]);

  // SER-5461
  timers.init(cacheModule.cache);
  const redactNameFromDeletedUsersConfigs = config.get('redactNameFromDeletedUsers');
  if (redactNameFromDeletedUsersConfigs.enabled) {
    redactNameFromDeletedUsers(
      redactNameFromDeletedUsersConfigs.intervalMs,
      redactNameFromDeletedUsersConfigs.key,
      redactNameFromDeletedUsersConfigs.options
    );
  }

  // needs cache to be initialized
  await downloadsMiddleware.initDownloadQueue();

  messages.error(function(err, data) {
    logger.error({
      file: __filename,
      event: 'messagesError',
      error: err,
      data: {
        messageData: data,
        messageKey: this.key,
        messageHistory: this.history,
      },
    });

    this.nack(false); // doesn't requeue
  });

  AugustRatelimiter.init(cache);
  s3Util.init();

  await config.onChange();

  logger.info({
    file: __filename,
    event: 'init',
    data: {
      cacheOptions,
      statdsOptions,
      modelOptions,
      messagesOptions: config.get('messages'),
      babelfishOptions,
    },
  });
}

/**
 * Starts health check server
 * @return {undefined}
 */
function startHealthCheckServer() {
  return new Promise(function startHealthCheck(resolve, reject) {
    const healthPort = config.get('healthPort');
    const healthServer = restify.createServer();
    serverHealth.addConnectionCheck('Redis', cacheModule.cache.isConnected.bind(cacheModule.cache));
    serverHealth.exposeHealthEndpoint(healthServer);

    healthServer.on('error', err => {
      reject(err);
    });

    healthServer.listen(healthPort, function() {
      logger.info({
        file: __filename,
        event: 'healthCheckServerStartListening',
        message: `Listening on port ${healthPort}`,
        data: {
          healthPort,
        },
      });

      resolve(healthServer);
    });
  });
}

/**
 * @returns {undefined}
 */
async function createServer() {
  const serverOptions = {
    name: packageInfo.name,
    version: packageInfo.version,
    strictNext: true,
  };

  const server = restify.createServer(serverOptions);
  // http://restify.com/#cors
  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
  const options = {
    origins: config.get('cors:origins'),
    allowHeaders: [config.get('accessTokenHeader'), config.get('brandingHeader')],
  };
  const cors = corsMiddleware(options);
  server.pre(cors.preflight);
  server.use(cors.actual);

  server.use(restify.plugins.acceptParser(server.acceptable));

  // Do not require before as session requires august-access-token to be initialized
  const sessionMiddleware = require('./middleware/session.js');

  const api = new AugustAPI(server, {
    instrument: true,
    // TODO: no hardcode
    version: '1.0.0',
    authHandler: sessionMiddleware.authHandler,
    queryParser: restify.plugins.queryParser(),
    bodyParser: restify.plugins.bodyParser(),
    conditionalHandler: restify.plugins.conditionalHandler,
  });

  require('./controllers/download.js')(api);
  require('./controllers/log.js')(api);
  require('./controllers/session.js')(api);
  require('./controllers/user.js')(api);
  require('./controllers/version.js')(api);
  require('./controllers/factoryresetrequest')(api);

  api.build();

  await api.listen(port);

  logger.info({
    file: __filename,
    event: 'serverStartReady',
    message: `Listening on port ${port}`,
    data: {
      port,
      name: server.name,
      url: server.url,
    },
  });
}

module.exports = (async function() {
  try {
    await init();
    await startHealthCheckServer();
    await createServer();
  } catch (error) {
    logger.error({
      file: __filename,
      event: 'serverStartError',
      printStack: true,
      error,
    });
  }
})();
