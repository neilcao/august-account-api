'use strict';

const AWS = require('aws-sdk');
const fs = require('fs');
const Promise = require('bluebird').Promise;

const augustKeys = require('august-keys').Keys();

const config = require('./config');

let s3;

module.exports = {
  init: () => {
    const s3Keys = augustKeys.getServiceKeys('august-account-api').s3;
    s3 = new AWS.S3({
      credentials: new AWS.Credentials({
        accessKeyId: s3Keys.access,
        secretAccessKey: s3Keys.secret,
      }),
      region: config.get('s3:region'),
    });
  },

  /**
   * Upload a file to S3.
   *
   * @param {string} bucket - The S3 bucket.
   * @param {string} key - The S3 key.
   * @param {string} filePath - Path of the file to upload.
   * @returns {Promise<Object|Error>} - The result of the AWS SDK call.
   */
  upload: (bucket, key, filePath) => {
    const fileStream = fs.createReadStream(filePath);

    const uploadParams = {
      Bucket: bucket,
      Key: key,
      Body: fileStream,
    };

    const promise = new Promise((resolve, reject) => {
      fileStream.on('error', err => {
        reject(err);
      });

      s3.upload(uploadParams, (err, result) => {
        if (err) {
          reject(err);
        }

        resolve(result);
      });
    });

    return promise.finally(() => {
      fileStream.destroy();
    });
  },

  /**
   * Delete an S3 object.
   *
   * @param {string} bucket - The S3 bucket.
   * @param {string} key - The S3 key.
   * @returns {Promise<Object|Error>} - The result of the AWS SDK call.
   */
  deleteObject: (bucket, key) => {
    return new Promise(function (resolve, reject) {
      const params = {
        Bucket: bucket,
        Key: key,
      };

      s3.deleteObject(params).send(function (err, data) {
        if (err) {
          return reject(err);
        }

        resolve(data);
      });
    });
  },

  /**
   * List all objects in a bucket.
   *
   * @param {Object} params - The params to pass to S3.
   * @see https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#listObjectsV2-property
   * @returns {Promise<Object|Error>} The list of objects.
   */
  listObjects: params => {
    return s3.listObjectsV2(params).promise();
  },

  /**
   * Get an object.
   *
   * @param {Object} params - The params to pass to S3.
   * @see https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#getObject-property
   * @returns {Promise<Object|Error>} The object.
   */
  getObject: params => s3.getObject(params),

  /**
   * Get a signed URL for an S3 object.
   *
   * @param {string} bucket - The S3 bucket.
   * @param {string} key - The S3 key.
   * @param {number} expirationInSeconds - How long the signed URL is valid for.
   * @returns {Promise<string|Error>} - The signed URL.
   */
  getSignedUrl: (bucket, key, expirationInSeconds) => {
    return new Promise((resolve, reject) => {
      const options = {
        Bucket: bucket,
        Key: key,
        Expires: expirationInSeconds,
      };

      s3.getSignedUrl('getObject', options, (err, url) => {
        if (err) {
          return reject(err);
        }

        resolve(url);
      });
    });
  },
};
