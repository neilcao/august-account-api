'use strict';

const augustCache = require('august-cache');
const logger = require('august-logger');

let cache;

module.exports = {
  get cache() {
    if (!cache) {
      const error = new Error('Cache not initialized');

      logger.error({
        file: __filename,
        event: 'getCacheError',
        error,
      });

      throw error;
    }

    return cache;
  },
};

module.exports.init = async options => {
  if (cache) {
    const error = new Error('Cache already Initialized');

    logger.error({
      file: __filename,
      event: 'initCacheError',
      error,
    });

    throw error;
  }

  cache = await augustCache.init(options);

  logger.info({
    file: __filename,
    event: 'augustCacheInitialized',
    data: {
      options,
    },
  });

  return cache;
};
